"""CS 220, Data Abstraction, Spring 2016.

This module contains a class definition for a Polynomial.

Partner 0: _NAME_HERE_
Partner 1: _NAME_HERE_
NOTE: The code in Partner 0's repository will be downloaded and graded.

Pair Programming Log:
  Partner 0 Driver: ____, Navigator: ____
  Partner 1 Driver: ____, Navigator: ____

Overall Contribution Estimate:
  Partner 0: ____
  Partner 1: ____

Documentation: _DETAILED_DOCUMENTATION_STATEMENT_HERE_.
"""


class Polynomial( object ):
    """Represents a polynomial in the form "4x^3+5x^2-3x^1+9x^0".

    The polynomial is stored using a linked-list of Term objects where
    each Term object contains an integer coefficient, an integer
    exponent, and a reference to the next Term in the polynomial.
    """

    class Term( object ):
        """A single term in a polynomial (i.e., a node in the list)."""
        def __init__( self, coefficient=0, exponent=0, next_term=None ):
            """Creates a new term."""
            self.coefficient = coefficient
            self.exponent = exponent
            self.next = next_term

    def __init__( self, s="" ):
        """Creates a new polynomial from an input string in the form "4x^3+5x^2-3x^1+9x^0".

        :param str s: String from which to parse a Polynomial.
        """
        # This must be the only attribute in your Polynomial class.
        self._head = None
        """:type: Polynomial.Term"""

    def __str__( self ):
        """Returns a string representation of the polynomial in the form "4x^3+5x^2-3x^1+9x^0".

        All coefficients and exponents are present, a lower-case 'x' is used as the
        variable, and the carat symbol '^' is used to designate the exponent.
        """
        return ""

    def __add__( self, other ):
        """Adds this polynomial to the given polynomial producing a new polynomial object;
        the original two polynomials remain unchanged.

        :param Polynomial other: Polynomial to be added to this polynomial.
        :return: A new polynomial containing the result.
        :rtype: Polynomial
        """
        return None

    def __sub__( self, other ):
        """Subtracts the given polynomial from this polynomial producing a new polynomial object;
        the original two polynomials remain unchanged.

        :param Polynomial other: Polynomial to be subtracted from this polynomial.
        :return: A new polynomial containing the result.
        :rtype: Polynomial
        """
        return None

    def __mul__( self, other ):
        """Multiplies this polynomial and the given polynomial producing a new polynomial object;
        the original two polynomials remain unchanged.

        :param Polynomial other: Polynomial to be multiplied with this polynomial.
        :return: A new polynomial containing the result.
        :rtype: Polynomial
        """
        return None


if __name__ == "__main__":
    # A quick bit of code to test basic creation of Polynomials,
    # but most testing should still be done with unit tests.

    # Test a few basic polynomials.
    poly_strings = [ "4x^3+5x^2-3x^1+9x^0",   # Basic polynomial.
                     "-4x^3+5x^2-3x^1+9x^0",  # Same polynomial with a leading minus sign.
                     "4x^3+0x^2-3x^1+9x^0",   # Polynomial with a zero coefficient (which should not be stored).
                     "456x^123+55x^12-13x^1+999999999x^0",  # Multiple digit coefficients and exponents.
                     "4x^2+2x^2-5x^1+2x^1+8x^0-4x^0" ]      # Multiple terms with the same exponent.
    for s in poly_strings:
        p = Polynomial( s )
        print( "s = {}".format( s ), "p = {}".format( p ), sep="\n", end="\n\n" )

    # Test a few invalid polynomials.
    poly_strings = [ "4x^3+5q5x^2-3x^1+9x^0",  # Non-numeric coefficient.
                     "4x^3+5x^2q3-3x^1+9x^0",  # Non-numeric exponent.
                     "4x^3+5y^2-3x^1+9x^0" ]   # Missing 'x' in a term.
    for s in poly_strings:
        try:
            p = Polynomial( s )
            print( "s = {}".format( s ), "p = {}".format( p ), sep="\n" )
        except ValueError as e:
            print( "s = {}: {}".format( s, e ) )
        except Exception as e:
            print( "s = {}: Unexpected {} exception {}.".format( type( e ), e ) )
        else:
            print( "ERROR: Expected exception not raised." )
        finally:
            print()  # Blank line between tests.

    # Test a basic addition, subtraction, and multiplication. Expected results are:
    # Addition:       4x^3+1x^2-3x^1+1x^0
    # Subtraction:    2x^3+5x^2-5x^1+9x^0
    # Multiplication: 3x^6-3x^5-7x^4+4x^3-26x^2+21x^1-20x^0
    a = Polynomial( "3x^3+3x^2-4x^1+5x^0" )
    b = Polynomial( "1x^3-2x^2+1x^1-4x^0" )
    print( "  {}\n+ {}\n=====================\n  {}\n".format( a, b, a + b ) )
    print( "  {}\n- {}\n=====================\n  {}\n".format( a, b, a - b ) )
    print( "  {}\n* {}\n=======================================\n  {}\n".format( a, b, a * b ) )
