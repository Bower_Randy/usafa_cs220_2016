"""CS 220, Data Abstraction, Spring 2016, Dr. Bower.

This module contains a basic template for writing unit tests.

Documentation: _YOUR_DETAILED_DOCUMENTATION_STATEMENT_HERE_.
"""

import unittest
import sys


class UnitTests( unittest.TestCase ):
    """For more information about Python's unittest module, see
    https://docs.python.org/3/library/unittest.html
    """

    @classmethod
    def setUpClass( cls ):
        """A class method called before any tests in an individual class run."""
        pass

    @classmethod
    def tearDownClass( cls ):
        """A class method called after all tests in an individual class run."""
        pass

    def setUp( self ):
        """This method is called immediately before calling each test method."""
        pass

    def tearDown( self ):
        """This method is called immediately after each test method completes."""
        pass

    def test_true( self ):
        """A basic test."""
        self.assertTrue( 42 < 73 )

    def test_false( self ):
        """A basic test."""
        self.assertFalse( 42 > 73 )

    def test_equal( self ):
        """A basic test."""
        self.assertEqual( 21 * 2, 14 * 3 )


if __name__ == '__main__':
    sys.argv.append( "-v" )  # Add the verbose command line flag.
    unittest.main()
