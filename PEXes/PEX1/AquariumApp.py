"""CS 220, Data Abstraction, Spring 2016, Dr. Bower.

A virtual aquarium application using a GUI created with Qt Designer.

Documentation: The original concept for this programming exercise is
from former faculty member and department chair, Dr. Dino Schweitzer.
This Python version of the programming exercise is completely original.
"""

import sys
from itertools import combinations
from time import sleep
from threading import Thread, Event
from PyQt4 import QtGui
from PEXes.PEX1.AquariumGui import Ui_aquarium
from PEXes.PEX1.Creatures import *


class Aquarium( object ):
    """Application class to instantiate an AquariumGui."""
    # The following are class attributes describing the dimensions of the aquarium.
    #     Note the attribute docstrings following the assignment statements:
    #     https://www.python.org/dev/peps/pep-0257/#what-is-a-docstring

    WIDTH = 1200  # Set to 900 for laptop screen ... reset to 1200 to submit!!!
    """The width of the aquarium, left-to-right on the screen."""
    HEIGHT = WIDTH * 2 // 3
    """The height of the aquarium, top-to-bottom on the screen."""
    DEPTH = HEIGHT // 2
    """The *virtual* depth of the aquarium, front-to-back."""

    def __init__( self ):
        """Initialize and show the gui."""
        # Create the main window in which the gui will display.
        self.main_window = QtGui.QWidget()

        # Create an instance of the gui and set it up in the main window.
        self.gui = Ui_aquarium()
        self.gui.setupUi( self.main_window )

        # Get the 1200x800 aquarium image to use as the background
        # and scale it to the size of the aquarium, if necessary.
        background = QtGui.QImage( "./images/Aquarium.png" )
        if Aquarium.WIDTH != 1200:
            background = background.scaledToWidth( Aquarium.WIDTH )
            self.main_window.resize( background.width(), background.height() )
            self.main_window.setFixedSize( background.width(), background.height() )
        # Set the background of the main window to the aquarium image.
        palette = QtGui.QPalette()
        palette.setBrush( QtGui.QPalette.Window, QtGui.QBrush( background ) )
        self.main_window.setPalette( palette )

        # Catch the paint event so the creatures can be drawn.
        self.gui.drawing_widget.paintEvent = self.paint_event
        # Catch the window close event so the animation thread can be stopped.
        self.main_window.closeEvent = self.close_event
        # This event object will be used to stop the animation.
        self.stop_event = Event()
        # Show the main window containing our gui.
        self.main_window.show()

        # Create a list of creatures.
        how_many = 3
        self.creatures = []
        self.creatures += [ Angelfish() for _ in range( how_many ) ]
        self.creatures += [ Clownfish() for _ in range( how_many ) ]
        self.creatures += [ Seahorse() for _ in range( how_many ) ]
        self.creatures += [ Crab() for _ in range( how_many ) ]
        self.creatures += [ Lobster() for _ in range( how_many ) ]
        self.creatures += [ Snail() for _ in range( how_many ) ]
        self.creatures += [ Bubble() for _ in range( how_many * 6 ) ]

        # A few statements to comment/un-comment for debugging, if necessary.
        # print( "\n".join( [ str( c ) for c in self.creatures ] ), flush=True )
        # self.creatures.sort()
        # print( "\n".join( [ str( c ) for c in self.creatures ] ), flush=True )

    def paint_event( self, q_paint_event ):
        """Called automatically when the drawing widget needs to repaint.

        :param QtGui.QPaintEvent q_paint_event: The event object from PyQt.
        """
        # Get a QPainter object that can paint on the drawing widget
        # and then have each creature use the painter to draw itself.
        painter = QtGui.QPainter( self.gui.drawing_widget )
        for creature in self.creatures:
            creature.draw( painter )

    def run( self ):
        """This function is to be launched in a separate thread to run the animation."""
        # The stop event will be set when the user closes the window.
        while not self.stop_event.is_set():
            # Every creature has the opportunity to move during each frame of the animation.
            for creature in self.creatures:
                creature.move()

            # Sort the creatures after each move to show depth, back-to-front, when drawn.
            # Note: The data will be nearly sorted most of the time with few creatures changing
            # positions. The sort used by Python is quite efficient in this case, so no worries:
            #     http://en.wikipedia.org/wiki/Timsort
            # Also, the objects being sorted must implement the __lt__() method to be sortable.
            self.creatures.sort()

            # Use the combinations method from the nifty itertools module to see if any pair of
            # creatures have met each other. Both creatures must have met for either to react.
            for ( a, b ) in combinations( self.creatures, 2 ):
                if a.met( b ) and b.met( a ):
                    a.react()
                    b.react()

            # Update the drawing widget to show this frame of the animation and
            # sleep 1/30th of a second for a 30 frames per second animation.
            self.gui.drawing_widget.update()
            sleep( 0.0333 )

    def close_event( self, event ):
        """Called automatically when the user closes the application window.

        :param PyQt.QtGui.QCloseEvent event: The event object from PyQt.
        """
        # Set the event to stop the animation thread.
        self.stop_event.set()
        # Accept the event so the window will shut down.
        event.accept()


def main():
    """Main program to launch the application."""
    # Create a QApplication to handle event processing for our gui.
    app = QtGui.QApplication( sys.argv )

    # Create an instance of our application.
    aquarium = Aquarium()

    # Start the animation in a new thread.
    t = Thread( target=aquarium.run )
    t.start()

    # Start the application executing, exiting when it returns (i.e., the window is closed).
    sys.exit( app.exec_() )  # Note the underscore at the end of exec_().


# Execute main() if the program is launched from this file.
if __name__ == "__main__":
    main()
