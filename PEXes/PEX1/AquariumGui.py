# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'C:/Users/Randall.Bower/Documents/Courses/CS220/Code/PEXes/PEX1_Aquarium/AquariumGui.ui'
#
# Created: Tue Dec 30 17:21:01 2014
#      by: PyQt4 UI code generator 4.11.2
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_aquarium(object):
    def setupUi(self, aquarium):
        aquarium.setObjectName(_fromUtf8("aquarium"))
        aquarium.resize(1200, 800)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(aquarium.sizePolicy().hasHeightForWidth())
        aquarium.setSizePolicy(sizePolicy)
        aquarium.setMinimumSize(QtCore.QSize(1200, 800))
        aquarium.setMaximumSize(QtCore.QSize(1200, 800))
        self.aquarium_layout = QtGui.QHBoxLayout(aquarium)
        self.aquarium_layout.setSpacing(0)
        self.aquarium_layout.setMargin(0)
        self.aquarium_layout.setObjectName(_fromUtf8("aquarium_layout"))
        self.drawing_layout = QtGui.QHBoxLayout()
        self.drawing_layout.setSpacing(0)
        self.drawing_layout.setObjectName(_fromUtf8("drawing_layout"))
        self.drawing_widget = QtGui.QWidget(aquarium)
        self.drawing_widget.setObjectName(_fromUtf8("drawing_widget"))
        self.drawing_layout.addWidget(self.drawing_widget)
        self.aquarium_layout.addLayout(self.drawing_layout)

        self.retranslateUi(aquarium)
        QtCore.QMetaObject.connectSlotsByName(aquarium)

    def retranslateUi(self, aquarium):
        aquarium.setWindowTitle(_translate("aquarium", "Aquarium", None))

