"""CS 220, Data Abstraction, Spring 2016, _YOUR_NAME_HERE_.

This module contains class definitions for the various aquatic creatures.

Documentation: _YOUR_DETAILED_DOCUMENTATION_STATEMENT_HERE_.
"""

from random import random
from PyQt4 import QtCore, QtGui
from PEXes.PEX1.AquariumApp import Aquarium


class Creature( object ):
    """Parent class for all aquatic creatures."""

    def __init__( self ):
        """Initializes a new creature, setting default values for common attributes."""
        # Private attributes for the image and its dimensions.
        self._image = QtGui.QImage( "./images/{}.png".format( self.__class__.__name__ ) )
        self._w = self._image.width()  # Width of the creature/image.
        self._h = self._image.height()  # Height of the creature/image.
        self._d = self._h / 2  # Depth is front-to-back; i.e., "thickness".
        self._rect = QtCore.QRect()  # Rectangle used to draw the image.

        # Initial attributes of the creature; subclasses may impose further restrictions.
        self._x = random() * ( Aquarium.WIDTH - self._w )
        self._y = random() * ( Aquarium.HEIGHT - self._h )
        self._z = random() * ( Aquarium.DEPTH - self._d )
        self._speed = 0.5 + random() * 1.5
        # Randomly decide the direction the creature is moving/facing.
        if random() < 0.5:
            self._speed *= -1
            self._image = self._image.mirrored( horizontal=True, vertical=False )

    def __str__( self ):
        """Returns a string representation of the creature."""
        return "{} is at ({},{},{}), dimensions=({},{},{}), speed={}".format(
            self.__class__.__name__, self._x, self._y, self._z, self._w, self._h, self._d, self._speed )

    def __lt__( self, other ):
        """Compares this Creature to another using < operator; used for sorting.

        **Do Not Modify This Method! All subclasses must inherit, as is.**

        :param Creature other: Creature object to compare to this creature object.
        :return: True if this Creature object's z position is less than the other; False otherwise.
        :rtype: bool
        """
        return self._z < other._z

    def draw( self, painter ):
        """Draws the creature using its current position, scale, and direction.

        **Do Not Modify This Method! All subclasses must inherit, as is.**

        :param QtGui.QPainter painter: QPainter to be used to draw the image.
        """
        # Scale the size based on distance from the front of the aquarium.
        scale = 0.5 + self._z / Aquarium.DEPTH / 2.0
        # Move and size the rectangle so the creature draws correctly.
        self._rect.setRect( self._x, self._y, self._w * scale, self._h * scale )
        # Use the painter to draw the creature's image in the rectangle.
        painter.drawImage( self._rect, self._image )

    def met( self, other ):
        """Determines if the creature has met another creature.

        :param Creature other: The other creature.
        :return: True if the creature has met the other creature; False otherwise.
        :rtype: bool
        """
        return False

    def move( self ):
        """All creatures must define their own movement."""
        pass

    def react( self ):
        """All creatures must define their own reaction to meeting another creature."""
        pass


class Angelfish( Creature ):
    """Swims horizontally across the upper 3/4 of the aquarium,
    always moving at a slight angle either upward or downward.
    Reverses direction when reacting to another creature.
    """
    pass


class Bubble( Creature ):
    """Moves vertically from the bottom to the top of the aquarium.
    Ignores all creatures.
    """
    pass


class Clownfish( Creature ):
    """Swims straight across the upper 3/4 of the aquarium.
    Swims at an angle for a short time, downward if moving left-to-right
    and upward if moving right-to-left when reacting to another creature.
    """
    pass


class Crab( Creature ):
    """Crawls forward and backward in the bottom 1/4 of the aquarium,
    pausing at random times for a short amount of time.
    Scurries away quickly, moving horizontally at three times their
    normal speed for a short period when reacting to another creature.
    """
    pass


class Lobster( Creature ):
    """Crawls forward and backward in the bottom 1/4 of the aquarium.
    With a probability of 0.5, it also moves horizontally the same
    distance it moved forward or backward. The direction it moves
    horizontally (left or right) is determined by the direction the
    lobster's image is facing such that it appears to be moving
    forward or backward, not sliding laterally. Note that after
    being created, the lobster's image never reverses.
    Pauses for a short period when reacting to another creature.
    """
    pass


class Seahorse( Creature ):
    """Swims horizontally across the upper 3/4 of the aquarium, also
    moving vertically in an up-and-down, zig-zag type motion.
    Ignores all creatures.
    """
    pass


class Snail( Creature ):
    """Crawls forward and backward in the bottom 1/4 of the aquarium.
    Ignores all creatures.
    """
    pass
