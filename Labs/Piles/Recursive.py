"""CS 220, Data Abstraction, Spring 2016, Dr. Bower.

This module contains recursive linked list implementations of the Pile ADT.

The Pile data structure contains operations for insertion, removal,
membership test, emptiness test, and size calculation.

The crux of the Pile data structure is the removal operation always removes
and returns the smallest item. Thus, items placed into the container must
be comparable (i.e., they have implemented the __lt__ method).

Documentation: None required for non-graded lab exercises.
"""

from Labs.Piles.Linked import LinkedPile, LinkedOrderedPile


class RecursivePile( LinkedPile ):
    """Implements a Pile ADT using a linked data structure and recursion."""

    def __init__( self, s=None ):
        """Creates an empty pile.

        :param str s: String of space-delimited integer values to be added to the pile.
        """
        super().__init__()

        # TODO - Lab 11: Implement the method as described in the lab document.
        pass  # Remove this line (and this comment) when writing your own code.

    def __str__( self ):
        """Returns a string of all items in the pile."""
        # TODO - Lab 12: Implement the method as described in the lab document.
        return super().__str__()  # Remove this line (and this comment) when writing your own code.

    def get( self ):
        """Removes and returns the smallest item in the pile.

        This operation is O( __ ).

        :return: The smallest item in this pile.
        """
        # TODO - Lab 12: Implement the method as described in the lab document.
        return super().get()  # Remove this line (and this comment) when writing your own code.

    def contains( self, item ):
        """Determines if the given item is in the pile.

        This operation is O( __ ).

        :param item: The item for which the pile is to be searched.
        :return: True if the item is in the pile; False otherwise.
        :rtype: bool
        """
        # TODO - Lab 12: Implement the method as described in the lab document.
        return super().contains( item )  # Remove this line (and this comment) when writing your own code.

    def size( self ):
        """Determines the size of the pile.

        This operation is O( __ )

        :return: The number of items in the pile.
        :rtype: int
        """
        # TODO - Lab 12: Implement the method as described in the lab document.
        return super().size()  # Remove this line (and this comment) when writing your own code.


class RecursiveOrderedPile( LinkedOrderedPile ):
    """Implements a Pile ADT using an ordered, linked data structure and recursion."""

    def __init__( self, s=None ):
        """Creates an empty pile.

        :param str s: String of space-delimited integer values to be added to the pile.
        """
        super().__init__()

        # TODO - Lab 11: Implement the method as described in the lab document.
        pass  # Remove this line (and this comment) when writing your own code.

    def put( self, item ):
        """Adds the given item to the pile.

        This operation is O( __ ).

        :param item: The item to be added to the pile.
        """
        # TODO - Lab 13: Implement the method as described in the lab document.
        super().put( item )  # Remove this line (and this comment) when writing your own code.

    def merge( self, other ):
        """Merges two piles, creating a new pile without changing either of the original piles.

        :param LinkedPile other: A pile to merge with this pile.
        :return: A new pile containing all items in this pile and the other pile.
        :rtype: LinkedPile
        """
        # TODO - Lab 13: Implement the method as described in the lab document.
        pass  # Remove the pass statement (and this comment) when writing your own code.
