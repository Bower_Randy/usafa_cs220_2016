"""CS 220, Data Abstraction, Spring 2016, Dr. Bower.

This module contains some timed tests comparing unordered
and ordered piles implemented with Python lists.

Documentation: None.
"""

from random import randint, seed
from timeit import timeit

from Labs.Piles.Lists import Pile, OrderedPile

MAX_SIZE = 2 ** 20
HOW_MANY = 2 ** 10
NUM_TESTS = 2 ** 3


# TODO - Lab 5: Read, discuss, and understand the following code.
def main():
    """Main program to run the tests."""

    print( "Running test_0()", end=" ... ", flush=True )
    t = timeit( "test_0()", setup="from __main__ import test_0", number=NUM_TESTS )
    print( "{:.4f}".format( t ) )

    print( "Running test_1()", end=" ... ", flush=True )
    t = timeit( "test_1()", setup="from __main__ import test_1", number=NUM_TESTS )
    print( "{:.4f}".format( t ) )

    print( "Running test_2()", end=" ... ", flush=True )
    t = timeit( "test_2()", setup="from __main__ import test_2", number=NUM_TESTS )
    print( "{:.4f}".format( t ) )

    print( "Running test_3()", end=" ... ", flush=True )
    t = timeit( "test_3()", setup="from __main__ import test_3", number=NUM_TESTS )
    print( "{:.4f}".format( t ) )


def test_0():
    """Test how long it takes to insert items into an unordered pile."""
    # Seed the random number generator to ensure consistent tests.
    seed( 42 )
    # Put a bunch of random values into the pile.
    p = Pile()
    for _ in range( HOW_MANY ):
        p.put( randint( 0, MAX_SIZE ) )


def test_1():
    """Test how long it takes to insert items into an unordered pile."""
    p = Pile()
    # Directly access the pile's list of items to built it quickly.
    p._items = list( range( HOW_MANY ) )
    # Now get all the items from the pile.
    for _ in range( HOW_MANY ):
        p.get()


def test_2():
    """Test how long it takes to insert items into an ordered pile."""
    # Seed the random number generator to ensure consistent tests.
    seed( 42 )
    # Put a bunch of random values into the pile.
    p = OrderedPile()
    for _ in range( HOW_MANY ):
        p.put( randint( 0, MAX_SIZE ) )


def test_3():
    """Test how long it takes to insert items into an ordered pile."""
    p = OrderedPile()
    # Directly access the pile's list of items to built it quickly.
    p._items = list( range( HOW_MANY ) )
    # Now get all the items from the pile.
    for _ in range( HOW_MANY ):
        p.get()


# Call main() if this file is being executed rather than imported.
if __name__ == "__main__":
    main()  # https://docs.python.org/3/library/__main__.html
