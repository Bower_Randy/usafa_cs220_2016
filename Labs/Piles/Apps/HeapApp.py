"""CS 220, Data Abstraction, Spring 2016, Dr. Bower.

This file contains a GUI application to visualize the Heap class.

Documentation: None.
"""

from threading import Thread, Event
from time import sleep
from random import randint
from Labs.Piles.Heap import Heap
from Labs.Piles.Apps.HeapGui import Ui_HeapGui
from PyQt4 import QtCore, QtGui
import sys


def main():
    """Launch a GUI created with Qt Designer."""
    # Create a QApplication to handle event processing.
    qt_app = QtGui.QApplication( sys.argv )

    # Create an instance of the app and show the main window.
    my_app = HeapApp()
    my_app.main_window.show()

    # Execute the QApplication, exiting when it returns (i.e., the window is closed).
    sys.exit( qt_app.exec_() )  # Note the underscore at the end of exec_().


class HeapApp:
    """Application class to instantiate and control a Heap Demo GUI."""

    def __init__( self ):
        """Initialize and show the gui."""
        # Create the animation thread and the pause/stop events for it (name the thread for debugging).
        self.animation_thread = Thread( target=self.run, name="Animation" )
        self.stop_animation = Event()
        self.run_animation = Event()
        self.run_animation.set()  # Initially set so the animation runs; cleared to pause the animation.

        # The tree thread will be re-created when the Play/Pause button is clicked,
        # but create an empty thread here so PyCharm will know what it is.
        self.heap_thread = Thread()

        # Create the main window in which the gui will display, create
        # an instance of the gui, and set it up in the main window.
        self.main_window = QtGui.QMainWindow()
        self.gui = Ui_HeapGui()
        self.gui.setupUi( self.main_window )
        # Connect the menu buttons to methods.
        self.gui.action_new.triggered.connect( self.new_heap )
        self.gui.action_balanced_data.triggered.connect( lambda: self.fill_heap( 0 ) )
        self.gui.action_ascending_data.triggered.connect( lambda: self.fill_heap( 1 ) )
        self.gui.action_descending_data.triggered.connect( lambda: self.fill_heap( 2 ) )
        self.gui.action_random_data.triggered.connect( lambda: self.fill_heap( 3 ) )
        self.gui.action_save.triggered.connect( self.save )
        self.gui.action_exit.triggered.connect( self.exit )
        self.gui.action_undo.triggered.connect( self.undo )
        self.gui.action_redo.triggered.connect( self.redo )
        self.gui.action_about.triggered.connect( self.about )
        # Connect the return key in the item edit.
        self.gui.item.returnPressed.connect( self.return_pressed )
        # Connect to the play and stop buttons.
        self.gui.play_button.clicked.connect( self.play )
        self.gui.stop_button.clicked.connect( self.stop )
        # Catch the paint event so the data can be drawn.
        self.gui.drawing_widget.paintEvent = self.paint_event
        # Catch the window close event so threads can be stopped.
        self.main_window.closeEvent = self.close_event
        # Put the keyboard focus in the item editor.
        self.gui.item.setFocus()

        # Keep a history of trees for the Undo/Redo functionality.
        self._undo_history = []
        self._redo_history = []

        # This will be reset in new_heap, but putting it here fixes the PyCharm warning.
        self.heap = None
        self.new_heap()
        self.fill_heap( 0 )

    def new_heap( self ):
        """Creates a new, empty heap and clears the history."""
        # Clear the history when creating a new heap.
        self._undo_history = []
        self._redo_history = []
        self.heap = Heap()
        self.gui.drawing_widget.update()

    def fill_heap( self, choice ):
        """Fills the current heap with the type of data indicated by choice.

        :param int choice: Specifies balanced, ascending, descending, or random data.
        """
        # Copy the current tree for the undo/redo functionality.
        self._undo_history.append( self.heap.copy() )
        self._redo_history = []

        data = [ [ 50, 30, 70, 20, 40, 60, 80, 15, 25, 35, 45, 55, 65, 75, 85 ],
                 [ 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 80, 85 ],
                 [ 85, 80, 75, 70, 65, 60, 55, 50, 45, 40, 35, 30, 25, 20, 15 ],
                 [ randint( 10, 99 ) for _ in range( 15 ) ] ]
        for item in data[ choice ]:
            self.heap.put( item )
        self.gui.drawing_widget.update()

    def undo( self ):
        """Returns the heap to its previous state."""
        if len( self._undo_history ) > 0:
            self._redo_history.append( self.heap )
            self.heap = self._undo_history.pop()
            self.gui.drawing_widget.update()
            self.gui.statusbar.clearMessage()

    def redo( self ):
        """Returns the heap to its state prior to an undo operation."""
        if len( self._redo_history ) > 0:
            self._undo_history.append( self.heap )
            self.heap = self._redo_history.pop()
            self.gui.drawing_widget.update()
            self.gui.statusbar.clearMessage()

    def save( self ):
        """Saves the current heap as an image file."""
        pix_map = QtGui.QPixmap.grabWidget( self.gui.drawing_widget )
        file_name = QtGui.QFileDialog.getSaveFileName( self.main_window, filter="*.jpg" )
        if file_name != "":  # file_name == "" when user clicks Cancel on file save dialog.
            pix_map.save( file_name )

    def exit( self ):
        """Exits the application."""
        # Calling close() on the main window automatically generates the
        # closeEvent, which is handled by the close_event() method below.
        self.main_window.close()

    def close_event( self, event ):
        """Called automatically when the user closes the application window.

        :param PyQt.QtGui.QCloseEvent event: The event object from PyQt.
        """
        # Stop the animation and accept the event so the window will shut down.
        self.stop()
        event.accept()

    def about( self ):
        """This method does what its name implies it does."""
        about = "CS 220, Spring 2015, Heap Demo" + \
                "\n\nAuthor: Dr. Bower" + \
                "\n\nDocumentation: None."
        QtGui.QMessageBox.about( self.main_window, "About", about )

    def return_pressed( self ):
        """Called when the user presses return in the item editor; does the operation with no animation."""
        # Copy the current tree for the undo/redo functionality.
        self._undo_history.append( self.heap.copy() )
        self._redo_history = []

        # Only put() and get() operations with a heap;
        # searching just isn't very exciting to animate.
        if self.gui.put_button.isChecked():
            # The user can enter multiple values when pressing return and not animating.
            for item in self.gui.item.text().strip().split():
                try:
                    self.heap.put( int( item ) )
                    self.gui.statusbar.showMessage( "Inserted {} . . .".format( item ) )
                except ( ValueError, TypeError ):
                    # Do an update in case the tree did change before the exception was raised.
                    self.gui.drawing_widget.update()
        else:
            # Any text in the item editor is ignored; replaced with item removed from heap.
            item = self.heap.get()
            self.gui.item.setText( str() )
            self.gui.statusbar.showMessage( "Removed {} . . .".format( item ) )

        # Update the drawing widget to show the updated tree.
        self.gui.drawing_widget.update()

    def play( self ):
        """Called when the user clicks the Play/Pause button."""
        if self.animation_thread.is_alive():
            # If the thread is already running, then play/pause the animation.
            if self.run_animation.is_set():
                self.run_animation.clear()
            else:
                self.run_animation.set()
        else:
            # Copy the current tree for the undo/redo functionality.
            self._undo_history.append( self.heap.copy() )
            self._redo_history = []

            # Only put() and get() operations with a heap;
            # searching just isn't very exciting to animate.
            if self.gui.put_button.isChecked():
                # Get the item from the GUI, using a try/except in case the user types something silly.
                try:
                    item = int( self.gui.item.text() )
                except ( ValueError, TypeError ):
                    item = 0
                    self.gui.item.setText( "" )
                    return

                # Create the heap thread, but don't start it yet (name the thread for debugging).
                self.heap_thread = Thread( target=lambda: self.heap.put( item ), name="Put" )
                self.gui.statusbar.showMessage( "Inserting {} . . .".format( item ) )
            else:
                self.heap_thread = Thread( target=lambda: self.heap.get(), name="Get" )
                # Have to cheat here and access _items to update the status bar ... sorry.
                item = self.heap._items[ 0 ]  # The item to be removed is always in location 0.
                self.gui.statusbar.showMessage( "Removing {} . . .".format( item ) )

            # Clear the heap's event to be sure the animation thread has a chance to draw the initial data.
            self.heap.permission_to_continue.clear()
            # Clear the animation's stop event so it will be able to run.
            self.stop_animation.clear()
            # Set the animation's run event so it will be able to run.
            self.run_animation.set()
            # Start the heap thread first because the animation thread only runs when the tree thread is alive.
            self.heap_thread.start()
            # Finally, start the animation thread.
            self.animation_thread.start()

    def stop( self ):
        """Called when the user clicks the Stop button."""
        # Set the stop animation event so the animation loop will terminate.
        self.stop_animation.set()
        # Set the run animation event in case it was paused so the loop will terminate due to the stop event.
        self.run_animation.set()
        # Set the tree's continue event to make sure the tree thread finishes.
        self.heap.permission_to_continue.set()

    def run( self ):
        """This function is to be launched in a separate thread to run the animation."""
        # The stop event will be set by the stop button or when the user closes the window.
        while not self.stop_animation.is_set() and self.heap_thread.is_alive():
            # Clear the tree's continue event so it will wait while the tree is displayed.
            self.heap.permission_to_continue.clear()
            # Update the drawing widget to show this frame of the animation.
            self.gui.drawing_widget.update()
            # This isn't a continuous animation, so sleep long enough to see what's happening.
            sleep( 0.5 )
            # Wait if the run event has been cleared (i.e., the animation has been paused).
            self.run_animation.wait()
            # Set the tree's continue event so it will continue.
            self.heap.permission_to_continue.set()

            # NOTE: The sleep in the above loop is *between* the clear() and set() of the
            # tree's continue event. Thus, once the tree demo pauses and the animation
            # starts drawing, the animation will draw an un-changing snapshot of the tree.
            # Further, since the set at the end of one iteration of the animation loop is
            # immediately followed by the clear the beginning of the next iteration of the
            # animation loop, the tree should be drawn most every time a pause is called.

        # Wait briefly for the tree thread to finish (necessary if the stop button was pressed).
        while self.heap_thread.is_alive():
            sleep( 0.1 )

        # Do one last update to make sure everything is pretty.
        self.gui.drawing_widget.update()

        # Create a new animation thread so it's ready to start nex time the Play button is clicked.
        self.animation_thread = Thread( target=self.run, name="Animation" )

    def paint_event( self, q_paint_event ):
        """Called automatically whenever the drawing widget needs to repaint.

        :param PyQt.QtGui.QPaintEvent q_paint_event: The event object from PyQt (not used).
        """
        # Get a QPainter object that can paint on the drawing widget.
        painter = QtGui.QPainter( self.gui.drawing_widget )
        painter.setBrush( QtCore.Qt.white )  # The brush determines the fill color.
        painter.setPen( QtCore.Qt.black )    # The pen determines the outline color.

        # Width and height of the drawing widget.
        w = self.gui.drawing_widget.width()
        h = self.gui.drawing_widget.height()
        # # Diameter of a tree node; nodes will overlap if the tree is too wide.
        d = 32
        # Delta-y is the change in y-coordinate between levels of the heap; the height of the
        # widget divided by how many nodes/branches need drawn (minus margin at top/bottom).
        # It get smaller as the heap gets higher; nodes overlap if the heap is too tall.
        dy = d * 2
        if self.heap.height() > 0:
            dy = min( d * 2, ( h - d * 3 ) // self.heap.height() )

        # Use the painter to draw the binary tree, starting with the tree contents across the top.
        painter.drawText( 0, 0, w, d, QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter, str( self.heap ) )
        # Draw the lines first, between the centers of the nodes.
        self.paint_lines( 0, painter, d, d // 2, w - d // 2, d * 2, dy, w // 2, d * 2 )
        # The nodes will now cover part of the lines, making3 everything pretty.
        self.paint_nodes( 0, painter, d, d // 2, w - d // 2, d * 2, dy )

    # Note: The methods below access protected members of the Heap class.
    # This is done to keep the for student use to keep the Heap class as
    # separate from the GUI application as possible. In a non-learning environment,
    # this would be written with the paint methods in the Heap class.

    def paint_lines( self, current_index, painter, d, left_x, right_x, y, dy, parent_x, parent_y ):
        """Draws the lines connecting binary heap/tree nodes.

        :param int current_index: The current index in the heap/tree being drawn.
        :param QtGui.QPainter painter: The painter to do the drawing.
        :param int d: The diameter of each tree node.
        :param int left_x: The left edge of the drawing area for this sub-tree.
        :param int right_x: The right edge of the drawing area for this sub-tree.
        :param int y: The y-coordinate of the current node in the drawing.
        :param int dy: The amount to move vertically with each level of the tree.
        :param int parent_x: The x-coordinate of the current node's parent.
        :param int parent_y: The y-coordinate of the current node's parent.
        """
        if 0 <= current_index < self.heap.size():
            x = ( left_x + right_x ) // 2
            painter.drawLine( x, y, parent_x, parent_y )
            self.paint_lines( self.heap._left_child( current_index ), painter, d, left_x, x, y + dy, dy, x, y )
            self.paint_lines( self.heap._right_child( current_index ), painter, d, x, right_x, y + dy, dy, x, y )

    def paint_nodes( self, current_index, painter, d, left_x, right_x, y, dy ):
        """Draws the binary heap/tree nodes.

        :param int current_index: The current index in the heap/tree being drawn.
        :param QtGui.QPainter painter: The painter to do the drawing.
        :param int d: The diameter of each tree node.
        :param int left_x: The left edge of the drawing area for this sub-tree.
        :param int right_x: The right edge of the drawing area for this sub-tree.
        :param int y: The y-coordinate of the current node in the drawing.
        :param int dy: The amount to move vertically with each level of the tree.
        """
        if 0 <= current_index < self.heap.size():
            x = ( left_x + right_x ) // 2

            if current_index == self.heap.highlight_index:
                painter.setBrush( QtCore.Qt.green )  # The brush determines the fill color.
                painter.drawEllipse( x - d // 2, y - d // 2, d, d )
                painter.setBrush( QtCore.Qt.white )  # The brush determines the fill color.
            else:
                painter.drawEllipse( x - d // 2, y - d // 2, d, d )

            painter.drawText( x - d // 2, y - d // 2, d, d,
                              QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter,
                              str( self.heap._items[ current_index ] ) )

            self.paint_nodes( self.heap._left_child( current_index ), painter, d, left_x, x, y + dy, dy )
            self.paint_nodes( self.heap._right_child( current_index ), painter, d, x, right_x, y + dy, dy )


# Call main() if this file is being executed rather than imported.
if __name__ == "__main__":
    main()  # https://docs.python.org/3/library/__main__.html
