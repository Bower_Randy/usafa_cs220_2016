"""CS 220, Data Abstraction, Spring 2016, Dr. Bower.

This file contains a GUI application to visualize the Binary Tree classes.

Documentation: None.
"""

from threading import Thread, Event
from time import sleep
from random import randint
from Labs.Piles.Trees import BinaryTree, BinarySearchTree, AVLTree
from Labs.Piles.Apps.ForestGui import Ui_ForestGui
from PyQt4 import QtCore, QtGui
import sys


def main():
    """Launch a GUI created with Qt Designer."""
    # Create a QApplication to handle event processing.
    qt_app = QtGui.QApplication( sys.argv )

    # Create an instance of the app and show the main window.
    my_app = ForestApp()
    my_app.main_window.show()

    # Execute the QApplication, exiting when it returns (i.e., the window is closed).
    sys.exit( qt_app.exec_() )  # Note the underscore at the end of exec_().


class ForestApp:
    """Application class to instantiate and control a Sort Demo GUI."""

    def __init__( self ):
        """Initialize and show the gui."""
        # Create the animation thread and the pause/stop events for it (name the thread for debugging).
        self.animation_thread = Thread( target=self.run, name="Animation" )
        self.stop_animation = Event()
        self.run_animation = Event()
        self.run_animation.set()  # Initially set so the animation runs; cleared to pause the animation.

        # The tree thread will be re-created when the Play/Pause button is clicked,
        # but create an empty thread here so PyCharm will know what it is.
        self.tree_thread = Thread()

        # Create the main window in which the gui will display, create
        # an instance of the gui, and set it up in the main window.
        self.main_window = QtGui.QMainWindow()
        self.gui = Ui_ForestGui()
        self.gui.setupUi( self.main_window )
        # Connect the menu buttons to methods.
        self.gui.action_binary_tree.triggered.connect( lambda: self.new_tree( BinaryTree() ) )
        self.gui.action_binary_search_tree.triggered.connect( lambda: self.new_tree( BinarySearchTree() ) )
        self.gui.action_avl_tree.triggered.connect( lambda: self.new_tree( AVLTree() ) )
        self.gui.action_balanced_data.triggered.connect( lambda: self.fill_tree( 0 ) )
        self.gui.action_ascending_data.triggered.connect( lambda: self.fill_tree( 1 ) )
        self.gui.action_descending_data.triggered.connect( lambda: self.fill_tree( 2 ) )
        self.gui.action_random_data.triggered.connect( lambda: self.fill_tree( 3 ) )
        self.gui.action_save.triggered.connect( self.save )
        self.gui.action_exit.triggered.connect( self.exit )
        self.gui.action_undo.triggered.connect( self.undo )
        self.gui.action_redo.triggered.connect( self.redo )
        self.gui.action_about.triggered.connect( self.about )
        # Connect the return key in the item edit.
        self.gui.item.returnPressed.connect( self.return_pressed )
        # Connect to the play and stop buttons.
        self.gui.play_button.clicked.connect( self.play )
        self.gui.stop_button.clicked.connect( self.stop )
        # Catch the paint event so the data can be drawn.
        self.gui.drawing_widget.paintEvent = self.paint_event
        # Catch the window close event so threads can be stopped.
        self.main_window.closeEvent = self.close_event
        # Put the keyboard focus in the item editor.
        self.gui.item.setFocus()

        # Keep a history of trees for the Undo/Redo functionality.
        self._undo_history = []
        self._redo_history = []

        # Start with a plain Binary Tree (a somewhat arbitrary choice) and fill it with balanced data.
        self.tree = None  # This will be reset in new_tree, but putting it here fixes the PyCharm warning.
        self.new_tree( BinaryTree() )
        self.fill_tree( 0 )

    def new_tree( self, tree ):
        """This method does what its name implies it does.

        :param BinaryTrees.BinaryTree tree: The new tree; may be a BinaryTree, BinarySearchTree, or AVLTree.
        """
        # Clear the history when creating a new tree.
        self._undo_history = []
        self._redo_history = []
        # A new, empty tree is passed as a parameter. Save it and show it's type in the title bar.
        self.tree = tree
        self.main_window.setWindowTitle( "Forest Demo - {}".format( type( tree ).__name__ ) )
        self.gui.drawing_widget.update()

    def fill_tree( self, choice ):
        """This method does what its name implies it does.

        :param int choice: Specifies balanced, ascending, descending, or random data.
        """
        # Copy the current tree for the undo/redo functionality.
        self._undo_history.append( self.tree.copy() )
        self._redo_history = []

        data = [ [ 50, 30, 70, 20, 40, 60, 80, 15, 25, 35, 45, 55, 65, 75, 85 ],
                 [ 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 80, 85 ],
                 [ 85, 80, 75, 70, 65, 60, 55, 50, 45, 40, 35, 30, 25, 20, 15 ],
                 [ randint( 10, 99 ) for _ in range( 15 ) ] ]
        for item in data[ choice ]:
            self.tree.put( item )
        self.gui.drawing_widget.update()

    def undo( self ):
        """This method does what its name implies it does."""
        if len( self._undo_history ) > 0:
            self._redo_history.append( self.tree )
            self.tree = self._undo_history.pop()
            self.gui.drawing_widget.update()
            self.gui.statusbar.clearMessage()

    def redo( self ):
        """This method does what its name implies it does."""
        if len( self._redo_history ) > 0:
            self._undo_history.append( self.tree )
            self.tree = self._redo_history.pop()
            self.gui.drawing_widget.update()
            self.gui.statusbar.clearMessage()

    def save( self ):
        """This method does what its name implies it does."""
        # pix_map = QtGui.QPixmap.grabWindow( self.gui.drawing_widget.winId() )
        pix_map = QtGui.QPixmap.grabWidget( self.gui.drawing_widget )
        file_name = QtGui.QFileDialog.getSaveFileName( self.main_window, filter="*.jpg" )
        if file_name != "":  # file_name == "" when user clicks Cancel on file save dialog.
            pix_map.save( file_name )

    def exit( self ):
        """This method does what its name implies it does."""
        # Calling close() on the main window automatically generates the
        # closeEvent, which is handled by the close_event() method below.
        self.main_window.close()

    def close_event( self, event ):
        """Called automatically when the user closes the application window.

        :param PyQt.QtGui.QCloseEvent event: The event object from PyQt.
        """
        # Stop the animation and accept the event so the window will shut down.
        self.stop()
        event.accept()

    def about( self ):
        """This method does what its name implies it does."""
        about = "CS 220, Spring 2015, Binary Tree Demo" + \
                "\n\nAuthor: Dr. Bower" + \
                "\n\nDocumentation: None."
        QtGui.QMessageBox.about( self.main_window, "About", about )

    def return_pressed( self ):
        """Called when the user presses return in the item editor; does the operation with no animation."""
        # Copy the current tree for the undo/redo functionality.
        self._undo_history.append( self.tree.copy() )
        self._redo_history = []

        # The user can enter multiple values when pressing return and not animating.
        for item in self.gui.item.text().strip().split():
            try:
                if self.gui.insert_button.isChecked():
                    self.tree.put( int( item ) )
                    self.gui.statusbar.showMessage( "Inserting {} . . .".format( item ) )
                elif self.gui.remove_button.isChecked():
                    self.tree.remove( int( item ) )
                    self.gui.statusbar.showMessage( "Removing {} . . .".format( item ) )
                else:
                    # print( self.tree.pre_order() )
                    # print( self.tree.in_order() )
                    # print( self.tree.post_order() )
                    result = "successful" if self.tree.contains( int( item ) ) else "unsuccessful"
                    self.gui.statusbar.showMessage( "Search for {} was {}.".format( item, result ) )
            except ( ValueError, TypeError ):
                # Do an update in case the tree did change before the exception was raised.
                self.gui.drawing_widget.update()

        # Update the drawing widget to show the updated tree.
        self.gui.drawing_widget.update()

    def play( self ):
        """Called when the user clicks the Play/Pause button."""
        if self.animation_thread.is_alive():
            # If the thread is already running, then play/pause the animation.
            if self.run_animation.is_set():
                self.run_animation.clear()
            else:
                self.run_animation.set()
        else:
            # Get the item from the GUI, using a try/except in case the user types something silly.
            try:
                item = int( self.gui.item.text() )
            except ( ValueError, TypeError ):
                item = 0
                self.gui.item.setText( "" )
                return

            # Copy the current tree for the undo/redo functionality.
            self._undo_history.append( self.tree.copy() )
            self._redo_history = []

            # Create the tree thread, but don't start it yet (name the thread for debugging).
            if self.gui.insert_button.isChecked():
                self.tree_thread = Thread( target=lambda: self.tree.put( item ), name="Insert" )
                self.gui.statusbar.showMessage( "Inserting {} . . .".format( item ) )
            elif self.gui.remove_button.isChecked():
                self.tree_thread = Thread( target=lambda: self.tree.remove( item ), name="Remove" )
                self.gui.statusbar.showMessage( "Removing {} . . .".format( item ) )
            else:
                self.tree_thread = Thread( target=lambda: self.tree.contains( item ), name="Search" )
                self.gui.statusbar.showMessage( "Searching for {} . . .".format( item ) )

            # Clear the tree's event to be sure the animation thread has a chance to draw the initial data.
            self.tree.permission_to_continue.clear()
            # Clear the animation's stop event so it will be able to run.
            self.stop_animation.clear()
            # Set the animation's run event so it will be able to run.
            self.run_animation.set()
            # Start the tree thread first because the animation thread only runs when the tree thread is alive.
            self.tree_thread.start()
            # Finally, start the animation thread.
            self.animation_thread.start()

    def stop( self ):
        """Called when the user clicks the Stop button."""
        # Set the stop animation event so the animation loop will terminate.
        self.stop_animation.set()
        # Set the run animation event in case it was paused so the loop will terminate due to the stop event.
        self.run_animation.set()
        # Set the tree's continue event to make sure the tree thread finishes.
        self.tree.permission_to_continue.set()

    def run( self ):
        """This function is to be launched in a separate thread to run the animation."""
        # The stop event will be set by the stop button or when the user closes the window.
        while not self.stop_animation.is_set() and self.tree_thread.is_alive():
            # Clear the tree's continue event so it will wait while the tree is displayed.
            self.tree.permission_to_continue.clear()
            # Update the drawing widget to show this frame of the animation.
            self.gui.drawing_widget.update()
            # This isn't a continuous animation, so sleep long enough to see what's happening.
            sleep( 0.5 )
            # Wait if the run event has been cleared (i.e., the animation has been paused).
            self.run_animation.wait()
            # Set the tree's continue event so it will continue.
            self.tree.permission_to_continue.set()

            # NOTE: The sleep in the above loop is *between* the clear() and set() of the
            # tree's continue event. Thus, once the tree demo pauses and the animation
            # starts drawing, the animation will draw an un-changing snapshot of the tree.
            # Further, since the set at the end of one iteration of the animation loop is
            # immediately followed by the clear the beginning of the next iteration of the
            # animation loop, the tree should be drawn most every time a pause is called.

        # Wait briefly for the tree thread to finish (necessary if the stop button was pressed).
        while self.tree_thread.is_alive():
            sleep( 0.1 )

        # Do one last update to make sure everything is pretty.
        self.gui.drawing_widget.update()

        # Create a new animation thread so it's ready to start nex time the Play button is clicked.
        self.animation_thread = Thread( target=self.run, name="Animation" )

    def paint_event( self, q_paint_event ):
        """Called automatically whenever the drawing widget needs to repaint.

        :param PyQt.QtGui.QPaintEvent q_paint_event: The event object from PyQt (not used).
        """
        # Get a QPainter object that can paint on the drawing widget.
        painter = QtGui.QPainter( self.gui.drawing_widget )
        painter.setBrush( QtCore.Qt.white )  # The brush determines the fill color.
        painter.setPen( QtCore.Qt.black )    # The pen determines the outline color.

        # Width and height of the drawing widget.
        w = self.gui.drawing_widget.width()
        h = self.gui.drawing_widget.height()
        # Diameter of a tree node; nodes will overlap if the tree is too wide.
        d = 32
        # Delta-y is the change in y-coordinate between levels of the tree; the height of the
        # widget divided by how many nodes/branches need drawn (minus margin at top/bottom).
        # It get smaller as the tree gets higher; nodes overlap if the tree is too tall.
        dy = d * 2
        if self.tree.height() > 0:
            dy = min( d * 2, ( h - d * 3 ) // self.tree.height() )

        # Use the painter to draw the binary tree, starting with the tree contents across the top.
        painter.drawText( 0, 0, w, d, QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter, str( self.tree ) )
        # Draw the lines first, between the centers of the nodes.
        self.paint_lines( self.tree._root, painter, d, d // 2, w - d // 2, d * 2, dy, w // 2, d * 2 )
        # The nodes will now cover part of the lines, making3 everything pretty.
        self.paint_nodes( self.tree._root, painter, d, d // 2, w - d // 2, d * 2, dy )

    def paint_lines( self, current_node, painter, d, left_x, right_x, y, dy, parent_x, parent_y ):
        """Draws the lines connecting binary tree nodes.

        :param BinaryTree.Node current_node: The current node in the tree being drawn.
        :param QtGui.QPainter painter: The painter to do the drawing.
        :param int d: The diameter of each tree node.
        :param int left_x: The left edge of the drawing area for this sub-tree.
        :param int right_x: The right edge of the drawing area for this sub-tree.
        :param int y: The y-coordinate of the current node in the drawing.
        :param int dy: The amount to move vertically with each level of the tree.
        :param int parent_x: The x-coordinate of the current node's parent.
        :param int parent_y: The y-coordinate of the current node's parent.
        """
        if current_node is not None:
            x = ( left_x + right_x ) // 2
            painter.drawLine( x, y, parent_x, parent_y )
            self.paint_lines( current_node.left, painter, d, left_x, x, y + dy, dy, x, y )
            self.paint_lines( current_node.right, painter, d, x, right_x, y + dy, dy, x, y )

    def paint_nodes( self, current_node, painter, d, left_x, right_x, y, dy ):
        """Draws the binary tree nodes.

        :param BinaryTree.Node current_node: The current node in the tree being drawn.
        :param QtGui.QPainter painter: The painter to do the drawing.
        :param int d: The diameter of each tree node.
        :param int left_x: The left edge of the drawing area for this sub-tree.
        :param int right_x: The right edge of the drawing area for this sub-tree.
        :param int y: The y-coordinate of the current node in the drawing.
        :param int dy: The amount to move vertically with each level of the tree.
        """
        if current_node is not None:
            x = ( left_x + right_x ) // 2

            if current_node == self.tree.highlight_node:
                painter.setBrush( QtCore.Qt.green )  # The brush determines the fill color.
                painter.drawEllipse( x - d // 2, y - d // 2, d, d )
                painter.setBrush( QtCore.Qt.white )  # The brush determines the fill color.
            else:
                painter.drawEllipse( x - d // 2, y - d // 2, d, d )

            painter.drawText( x - d // 2, y - d // 2, d, d,
                              QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter, str( current_node.item ) )

            self.paint_nodes( current_node.left, painter, d, left_x, x, y + dy, dy )
            self.paint_nodes( current_node.right, painter, d, x, right_x, y + dy, dy )


# Call main() if this file is being executed rather than imported.
if __name__ == "__main__":
    main()  # https://docs.python.org/3/library/__main__.html
