"""CS 220, Data Abstraction, Spring 2016, Dr. Bower.

This module contains binary tree implementations of the Pile ADT.

The Pile data structure contains operations for insertion, removal,
membership test, emptiness test, and size calculation.

The crux of the Pile data structure is the removal operation always removes
and returns the smallest item. Thus, items placed into the container must
be comparable (i.e., they have implemented the __lt__ method).

Documentation: None required for non-graded lab exercises.
"""

# The threading module is needed in case the trees are being animated by a GUI.
# All references to threading and events can be ignored and the trees work fine.
import threading


# TODO - Lab 21: Read, discuss, and understand the following code.
class BinaryTree( object ):
    """Implements a Pile ADT using a binary tree data structure."""

    class Node( object ):
        """A single node in a binary tree."""

        def __init__( self, item=None ):
            """Creates a node with the given item.

            :param item: The item to be stored in the node.
            """
            self.item = item
            self.left = None   # Binary tree nodes are always created as leaves,
            self.right = None  # so the left and right references are always None.
            self.height = 0

    def __init__( self, s=None ):
        """Creates an empty pile/tree.

        :param str s: String of space-delimited integer values to be added to the pile.
        """
        self._root = None
        """:type: BinaryTree.Node"""

        # Create an event that another thread could use to pause the sort in order to show the data.
        self.permission_to_continue = threading.Event()
        # The event is initially set, so another thread must clear it to pause the sort.
        # NOTE: If another thread clears the event, it must also set it or the tree may freeze!
        self.permission_to_continue.set()
        # When paused, a particular node can be highlighted.
        self.highlight_node = None

        # If the string parameter is given and it is a string with only space-delimited
        # integer values, split the string and put the values in the pile.
        if s is not None and str( s ).replace( ' ', '' ).isdecimal():
            for item in s.split():
                self.put( int( item ) )

    def _pause( self, highlight_node=None ):
        """Pause the sort so another thread could show the data."""
        # When paused, a particular node can be highlighted.
        # Save this as a class attribute so a GUI has access.
        self.highlight_node = highlight_node
        # If the event is never cleared by another thread, the tree cruises along, never waiting.
        # NOTE: If another thread clears the event, it must also set it or the tree may freeze!
        self.permission_to_continue.wait()

    def __str__( self ):
        """Returns a string of all items in the pile."""
        # Use the in-order traversal to create a string of the items in the pile/tree.
        return " ".join( str( item ) for item in self.in_order() )

    def __iter__( self ):
        """Returns an iterator object over the items in the pile/tree."""
        # Get a list with an in-order traversal, then use Python's built-in list iterator.
        # This could be written to be a bit more memory efficient; see challenge exercise.
        return iter( self.in_order() )

    def __eq__( self, other ):
        """Compares this tree to another tree, checking both structure and content.

        :param BinaryTree other: The tree to be compared.
        :return: True if the trees are equal; False otherwise.
        :rtype: bool
        """
        # TODO - Lab 24: Implement the method as described in the lab document.
        return False  # Remove this line (and this comment) when writing your own code.

    def copy( self ):
        """Returns a deep copy of the tree."""
        # TODO - Lab 22: Implement the method as described in the lab document.
        return None  # Remove this line (and this comment) when writing your own code.

    def put( self, item ):
        """Adds the given item to the pile/tree.

        This operation is O( __ ).

        :param item: The item to be added to the pile/tree.
        """
        # Call the recursive helper method with the root of the tree as the current node.
        self._root = self._put( item, self._root )

    def _put( self, item, current_node ):
        # Private, recursive helper for the put method.
        self._pause( current_node )  # Pause and highlight the current node.

        # TODO - Lab 21: Complete the method as described in the lab document.

        # The current_node is either a new node or it's left or right child
        # has been modified, so return it to the waiting recursive call.
        return current_node

    def get( self ):
        """Removes and returns the smallest item in the pile/tree.

        This operation is O( __ ).

        :return: The smallest item in this pile.
        """
        if self.is_empty():
            raise IndexError( "ERROR: Cannot get from empty pile." )
        else:
            # Since the tree is iterable, use Python's built-in min() function.
            item = min( self )
            # Call the recursive helper method to actually remove the item/node.
            self._root = self._remove( item, self._root )
            # Return the item.
            return item

    def remove( self, item ):
        """Removes a specific item from the pile/tree.

        Note: This is not normally part of a pile, but is useful for testing a tree.

        :param item: The item to be removed from the pile/tree.
        """
        self._root = self._remove( item, self._root )

    def _remove( self, item, current_node ):
        # Private, recursive helper for the get/remove methods.
        self._pause( current_node )  # Pause and highlight the current node.

        # TODO - Lab 21/22: Complete the method as described in the lab document.

        # The value of current_node has been changed appropriately or is None, so return it.
        return current_node

    def contains( self, item ):
        """Determines if the given item is in the pile/tree.

        This operation is O( __ ).

        :param item: The item for which the pile is to be searched.
        :return: True if the item is in the pile; False otherwise.
        :rtype: bool
        """
        # Call the recursive helper method passing the root of the tree as the current node.
        return self._contains( item, self._root )

    def _contains( self, item, current_node ):
        # Private, recursive helper for the contains method.
        self._pause( current_node )  # Pause and highlight the current node.

        # TODO - Lab 21: Complete the method as described in the lab document.
        return False  # Remove this line (and this comment) when writing your own code.

    def is_empty( self ):
        """Determines if the pile/tree is empty.

        This operation is O( 1 ).

        :return: True if the pile/tree is empty; False otherwise.
        :rtype: bool
        """
        # Note: Comparison to None is done with the "is" and "is not" operators.
        return self._root is None

    def size( self ):
        """Determines the size of the pile/tree.

        This operation is O( __ )

        :return: The number of items in the pile/tree.
        :rtype: int
        """
        # Call the recursive helper method passing the root of the tree as the current node.
        return self._size( self._root )

    def _size( self, current_node ):
        # Private, recursive helper for the size method.

        # TODO - Lab 21: Complete the method as described in the lab document.
        return 0  # Remove this line (and this comment) when writing your own code.

    def height( self ):
        """Determines the height of the binary tree.

        Note: This is not normally part of a pile, but is useful for testing a tree
        and is also necessary for implementing a balanced binary tree.

        The height of a binary tree is defined as the number of branches between the
        root and the deepest leaf. The height of the root is zero and the height of
        an empty tree is -1.

        :return: The height of the binary tree.
        :rtype: int
        """
        return self._height( self._root )

    def _height( self, current_node ):
        # Private, recursive helper for the height method.

        # TODO - Lab 21: Complete the method as described in the lab document.
        return 0  # Remove this line (and this comment) when writing your own code.

    def in_order( self ):
        """Returns a list of all items in the tree resulting from an
        in-order traversal of the tree.

        :return: An in-order list of items in the tree.
        :rtype: list
        """
        # Call the recursive helper method with the root of the tree as the current node
        return self._in_order( self._root )

    def _in_order( self, current_node ):
        # Private, recursive helper for the in_order method.

        # TODO - Lab 21: Complete the method as described in the lab document.
        return []  # Remove this line (and this comment) when writing your own code.

    def pre_order( self ):
        """Returns a list of all items in the tree resulting from a
        pre-order traversal of the tree.

        :return: An pre-order list of items in the tree.
        :rtype: list
        """
        # Call the recursive helper method with the root of the tree as the current node
        return self._pre_order( self._root )

    def _pre_order( self, current_node ):
        # Private, recursive helper for the pre_order method.

        # TODO - Lab 21: Complete the method as described in the lab document.
        return []  # Remove this line (and this comment) when writing your own code.

    def post_order( self ):
        """Returns a list of all items in the tree resulting from a
        pre-order traversal of the tree.

        :return: An pre-order list of items in the tree.
        :rtype: list
        """
        # Call the recursive helper method with the root of the tree as the current node
        return self._post_order( self._root )

    def _post_order( self, current_node ):
        # Private, recursive helper for the post_order method.

        # TODO - Lab 21: Complete the method as described in the lab document.
        return []  # Remove this line (and this comment) when writing your own code.

    def print( self ):
        """A handy way to print/draw a text version of a binary tree.

        When viewing, touch your left ear to your left shoulder and it looks like a tree.
        """
        self._print( self._root, 0 )

    def _print( self, current_node, depth ):
        # Private, recursive helper for the print method.
        if current_node is not None:
            self._print( current_node.right, depth + 1 )
            print( " " * depth * 4, current_node.item, sep="" )
            self._print( current_node.left, depth + 1 )


class BinarySearchTree( BinaryTree ):
    """Implements a Pile ADT using a binary search tree data structure."""

    # The public put() method can be inherited from BinaryTree since the call to the
    # recursive helper method, passing the root as the current node, is the same.
    # Adding an item to a binary search tree must ensure the property that all values
    # to the left of a given node are smaller and all values to the right are larger.
    def _put( self, item, current_node ):
        # Private, recursive helper for the put method.

        # TODO - Lab 22/23: Implement the method as described in the lab document.
        return super()._put( item, current_node )  # Remove this line when writing your own code.

    # The public get() method could be inherited from BinaryTree and it work work correctly,
    # but knowing the tree adheres to the Binary Search Tree requirements, it can be written
    # much more efficiently than using Python's built-in min() function. Specifically, the
    # smallest item in a Binary Search Tree is the farthest item straight left from the root.
    def get( self ):
        """Removes and returns the smallest item in the pile.

        This operation is O( __ ).

        :return: The smallest item in this pile.
        """
        # TODO - Lab 22/23: Implement the method as described in the lab document.
        return super().get()  # Remove this line when writing your own code.

    # Removing an item from a binary search tree must ensure the property that all values
    # to the left of a given node are smaller and all values to the right are larger.
    def _remove( self, item, current_node ):
        # Private, recursive helper for the remove method.

        # TODO - Lab 22/23: Implement the method as described in the lab document.
        return super()._remove( item, current_node )  # Remove this line when writing your own code.

    # Finding an item in a binary search tree takes advantage of the property that all values
    # to the left of a given node are smaller and all values to the right are larger.
    def _contains( self, item, current_node ):
        # Private, recursive helper for the contains method.

        # TODO - Lab 22/23: Implement the method as described in the lab document.
        return super()._contains( item, current_node )  # Remove this line when writing your own code.


class AVLTree( BinarySearchTree ):
    """Implements a Pile ADT using an AVL tree data structure."""

    def _put( self, item, current_node ):
        # Private, recursive helper for the put method.
        return self._balance( super()._put( item, current_node ) )

    def _remove( self, item, current_node ):
        # Private, recursive helper for the put method.
        return self._balance( super()._remove( item, current_node ) )

    def _balance_factor( self, current_node ):
        # Private method to determine the balance factor of a node.
        if current_node is None:
            return 0
        else:
            return self._height( current_node.left ) - self._height( current_node.right )

    def _balance( self, current_node ):
        # Private, recursive helper for the _put and _remove methods.

        # TODO - Lab 24: Implement the method as described in the lab document.
        return current_node  # Remove this line (and this comment) when writing your own code.

    def _left_left_rotation( self, current_node ):
        # Perform the left rotation and return the new root of this subtree.

        # TODO - Lab 24: Implement the method as described in the lab document.
        return current_node  # Remove this line (and this comment) when writing your own code.

    def _right_right_rotation( self, current_node ):
        # Perform the right rotation and return the new root of this subtree.

        # TODO - Lab 24: Implement the method as described in the lab document.
        return current_node  # Remove this line (and this comment) when writing your own code.

    def _right_left_rotation( self, current_node ):
        # Perform the right-left double rotation and return the new root of this subtree.

        # TODO - Lab 24: Implement the method as described in the lab document.
        return current_node  # Remove this line (and this comment) when writing your own code.

    def _left_right_rotation( self, current_node ):
        # Perform the left-right double rotation and return the new root of this subtree.

        # TODO - Lab 24: Implement the method as described in the lab document.
        return current_node  # Remove this line (and this comment) when writing your own code.
