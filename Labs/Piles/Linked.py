"""CS 220, Data Abstraction, Spring 2016, Dr. Bower.

This module contains linked list implementations of the Pile ADT.

The Pile data structure contains operations for insertion, removal,
membership test, emptiness test, and size calculation.

The crux of the Pile data structure is the removal operation always removes
and returns the smallest item. Thus, items placed into the container must
be comparable (i.e., they have implemented the __lt__ method).

Documentation: None required for non-graded lab exercises.
"""


# TODO - Lab 6: Read, discuss, and understand the following code.
class LinkedPile( object ):
    """Implements a Pile ADT using a linked data structure."""

    class Node( object ):
        """A single node in a linked list."""

        # Note: This class is purposely defined *inside* the LinkedPile
        # class because it is only intended to be used by a linked pile.

        def __init__( self, item=None, next_node=None ):
            """Creates a node with the given item and next node reference.

            :param item: The item to be stored in the node.
            :param next_node: The next node in the list.
            """
            self.item = item
            self.next = next_node

            # Note: The Node class consists of only a constructor that
            # defines the attributes of a single node. The get and set
            # methods defined in the online text are not necessary.

    def __init__( self ):
        """Creates an empty pile."""
        self._head = None
        """:type: LinkedPile.Node"""

    def __str__( self ):
        """Returns a string of all items in the pile."""
        # Start with an empty string and the head of the list.
        result = ""
        curr = self._head
        # Loop through all of the nodes in the list.
        while curr is not None:
            # Add the current item and move to the next item.
            result += str( curr.item ) + " "
            curr = curr.next
        # Strip the trailing space from the result.
        return result.strip()

    def __iter__( self ):
        """Returns an iterator object over the items in the pile."""
        # TODO - Lab 8: Implement the method as described in the lab document.
        return iter( [ int( s ) for s in self.__str__().split() ] )  # Remove this line when writing your own code.
        # The above line is a total hack implementation until we do it right in Lab 8.

    def put( self, item ):
        """Adds the given item to the pile.

        This operation is O( __ ).

        :param item: The item to be added to the pile.
        :return: None
        """
        # New nodes are added to the head of the list.
        self._head = LinkedPile.Node( item, self._head )

    def get( self ):
        """Removes and returns the smallest item in the pile.

        This operation is O( __ ).

        :return: The smallest item in this pile.
        """
        if self.is_empty():
            raise IndexError( "ERROR: Cannot get from empty pile." )
        else:
            # TODO - Lab 6: Implement the method as described in the lab document.
            return self._head.item  # Remove this line (and this comment) when writing your own code.

    def contains( self, item ):
        """Determines if the given item is in the pile.

        This operation is O( __ ).

        :param item: The item for which the pile is to be searched.
        :return: True if the item is in the pile; False otherwise.
        :rtype: bool
        """
        # Start at the head and loop through all nodes in the list.
        curr = self._head
        while curr is not None:
            # Return True and stop as soon as a matching item is found.
            if curr.item == item:
                return True
            # Move along the list.
            curr = curr.next
        # Finished the loop without finding the item.
        return False

    def is_empty( self ):
        """Determines if the pile is empty.

        This operation is O( 1 ).

        :return: True if the pile is empty; False otherwise.
        :rtype: bool
        """
        # Note: Comparison to None is done with the "is" and "is not" operators.
        return self._head is None

    def size( self ):
        """Determines the size of the pile.

        This operation is O( __ )

        :return: The number of items in the pile.
        :rtype: int
        """
        # Loop through and count the nodes in the list.
        length = 0
        curr = self._head
        while curr is not None:
            length += 1
            curr = curr.next
        return length


class LinkedOrderedPile( LinkedPile ):
    """Implements a Pile ADT using an ordered, linked data structure."""

    def put( self, item ):
        """Adds the given item to the pile.

        This operation is O( __ ).

        :param item: The item to be added to the pile.
        :return: None
        """
        # TODO - Lab 7: Implement the method as described in the lab document.
        super().put( item )  # Remove this line (and this comment) when writing your own code.

    def get( self ):
        """Removes and returns the smallest item in the pile.

        This operation is O( __ ).

        :return: The smallest item in this pile.
        """
        # TODO - Lab 7: Implement the method as described in the lab document.
        return super().get()  # Remove this line (and this comment) when writing your own code.

    def contains( self, item ):
        """Determines if the given item is in the pile.

        This operation is O( __ ).

        :param item: The item for which the pile is to be searched.
        :return: True if the item is in the pile; False otherwise.
        :rtype: bool
        """
        # TODO - Lab 7: Implement the method as described in the lab document.
        return super().contains( item )  # Remove this line (and this comment) when writing your own code.
