"""CS 220, Data Abstraction, Spring 2016, Dr. Bower.

This module contains array implementations of the Pile ADT.

The Pile data structure contains operations for insertion, removal,
membership test, emptiness test, and size calculation.

The crux of the Pile data structure is the removal operation always removes
and returns the smallest item. Thus, items placed into the container must
be comparable (i.e., they have implemented the __lt__ method).

Documentation: None required for non-graded lab exercises.
"""


# TODO - Lab 5: Read, discuss, and understand the following code.
class Pile( object ):
    """Implements a Pile ADT using a Python list."""

    def __init__( self ):
        """Creates an empty pile."""
        self._items = []  # Store the items in a Python list.

    def __str__( self ):
        """Returns a string of all items in the pile."""
        return " ".join( [ str( item ) for item in self._items ] )

    def __iter__( self ):
        """Returns an iterator object over the items in the pile."""
        # TODO - Lab 8: Implement the method as described in the lab document.
        return iter( self._items )  # Remove this line (and this comment) when writing your own code.
        # The above line is a bit of a hack implementation until we do it right in Lab 8.

    def put( self, item ):
        """Adds the given item to the pile.

        This operation is O( __ ).

        :param item: The item to be added to the pile.
        :return: None
        """
        self._items.append( item )

    def get( self ):
        """Removes and returns the smallest item in the pile.

        This operation is O( __ ).

        :return: The smallest item in this pile.
        """
        if self.is_empty():
            raise IndexError( "ERROR: Cannot get from empty pile." )
        else:
            smallest = min( self._items )
            self._items.remove( smallest )
            return smallest

    def contains( self, item ):
        """Determines if the given item is in the pile.

        This operation is O( __ ).

        :param item: The item for which the pile is to be searched.
        :return: True if the item is in the pile; False otherwise.
        :rtype: bool
        """
        return item in self._items

    def is_empty( self ):
        """Determines if the pile is empty.

        This operation is O( 1 ).

        :return: True if the pile is empty; False otherwise.
        :rtype: bool
        """
        return not self._items  # A non-empty list tests as True.

    def size( self ):
        """Determines the size of the pile.

        This operation is O( 1 ) ... see https://wiki.python.org/moin/TimeComplexity.

        :return: The number of items in the pile.
        :rtype: int
        """
        return len( self._items )


class OrderedPile( Pile ):
    """Implements a Pile ADT using a Python list, keeping it in order."""

    def put( self, item ):
        """Adds an item to this pile, ensuring the pile remains in order.

        This operation is O( __ ).

        :param item: The item to be added to the pile.
        :return: None
        """
        # TODO - Lab 5: Implement the method as described in the lab document.
        super().put( item )  # Remove this line (and this comment) when writing your own code.

    def get( self ):
        """Removes and returns the smallest item in the pile.

        This operation is O( __ ).

        :return: The smallest item in this pile.
        """
        # TODO - Lab 5: Implement the method as described in the lab document.
        return super().get()  # Remove this line (and this comment) when writing your own code.

    def contains( self, item ):
        """Determines if the given item is in the pile.

        This operation is O( __ ).

        :param item: The item for which the pile is to be searched.
        :return: True if the item is in the pile; False otherwise.
        :rtype: bool
        """
        # TODO - Lab 5: Implement the method as described in the lab document.
        return super().contains( item )  # Remove this line (and this comment) when writing your own code.
