"""CS 220, Data Abstraction, Spring 2016, Dr. Bower.

This module contains a pile of Pile unit tests.

Documentation: None.
"""

import random
import unittest
import sys

from Labs.Piles.Lists import Pile as Pile
# from Labs.Piles.Lists import OrderedPile as Pile
# from Labs.Piles.Linked import LinkedPile as Pile
# from Labs.Piles.Linked import LinkedOrderedPile as Pile
# from Labs.Piles.Recursive import RecursivePile as Pile
# from Labs.Piles.Recursive import RecursiveOrderedPile as Pile
# from Labs.Piles.Trees import BinaryTree as Pile
# from Labs.Piles.Trees import BinarySearchTree as Pile
# from Labs.Piles.Trees import AVLTree as Pile
# from Labs.Piles.Heap import Heap as Pile


# TODO - Lab 5: Read, discuss, and understand the following code.
class UnitTests( unittest.TestCase ):
    """For more information about Python's unittest module, see
    https://docs.python.org/3/library/unittest.html
    """

    @classmethod
    def setUpClass( cls ):
        """A class method called before any tests in an individual class run."""
        print( "Testing the {} implementation of Pile.".format( Pile().__class__.__name__ ), flush=True )

    @classmethod
    def tearDownClass( cls ):
        """A class method called after all tests in an individual class run."""
        pass

    def setUp( self ):
        """This method is called immediately before calling each test method."""
        self.pile = Pile()

    def tearDown( self ):
        """This method is called immediately after each test method completes."""
        pass

    def test_0( self ):
        """Test a brand new, empty pile."""
        self.assertTrue( self.pile.is_empty() )
        self.assertEqual( 0, self.pile.size() )

    def test_1( self ):
        """Test putting and getting one item."""
        # Put something in and make sure everything is ok.
        self.pile.put( 42 )
        self.assertFalse( self.pile.is_empty() )
        self.assertEqual( 1, self.pile.size() )

        # Get the item and make sure everything is ok.
        self.assertEqual( 42, self.pile.get() )
        self.assertTrue( self.pile.is_empty() )
        self.assertEqual( 0, self.pile.size() )

    def test_2a( self ):
        """Test putting and getting two items with the first item smaller than the second."""
        # Put two items in and make sure everything is ok.
        self.pile.put( 37 )
        self.pile.put( 42 )
        self.assertFalse( self.pile.is_empty() )
        self.assertEqual( 2, self.pile.size() )

        # Get one item and make sure everything is ok.
        self.assertEqual( 37, self.pile.get() )
        self.assertFalse( self.pile.is_empty() )
        self.assertEqual( 1, self.pile.size() )

        # Get the other item and make sure everything is ok.
        self.assertEqual( 42, self.pile.get() )
        self.assertTrue( self.pile.is_empty() )
        self.assertEqual( 0, self.pile.size() )

    def test_2b( self ):
        """Test putting and getting two items with the second item smaller than the first."""
        # Put two items in and make sure everything is ok.
        self.pile.put( 42 )
        self.pile.put( 37 )
        self.assertFalse( self.pile.is_empty() )
        self.assertEqual( 2, self.pile.size() )

        # Get one item and make sure everything is ok.
        self.assertEqual( 37, self.pile.get() )
        self.assertFalse( self.pile.is_empty() )
        self.assertEqual( 1, self.pile.size() )

        # Get the other item and make sure everything is ok.
        self.assertEqual( 42, self.pile.get() )
        self.assertTrue( self.pile.is_empty() )
        self.assertEqual( 0, self.pile.size() )

    def test_3a( self ):
        """Test putting and getting three items with the items inserted in ascending order."""
        # Put three items in and make sure everything is ok.
        self.pile.put( 37 )
        self.pile.put( 40 )
        self.pile.put( 42 )
        self.assertFalse( self.pile.is_empty() )
        self.assertEqual( 3, self.pile.size() )

        # Get one item and make sure everything is ok.
        self.assertEqual( 37, self.pile.get() )
        self.assertFalse( self.pile.is_empty() )
        self.assertEqual( 2, self.pile.size() )

        # Get another item and make sure everything is ok.
        self.assertEqual( 40, self.pile.get() )
        self.assertFalse( self.pile.is_empty() )
        self.assertEqual( 1, self.pile.size() )

        # Get the last item and make sure everything is ok.
        self.assertEqual( 42, self.pile.get() )
        self.assertTrue( self.pile.is_empty() )
        self.assertEqual( 0, self.pile.size() )

    def test_3b( self ):
        """Test putting and getting three items with the items inserted in descending order."""
        # Put three items in and make sure everything is ok.
        self.pile.put( 42 )
        self.pile.put( 40 )
        self.pile.put( 37 )
        self.assertFalse( self.pile.is_empty() )
        self.assertEqual( 3, self.pile.size() )

        # Get one item and make sure everything is ok.
        self.assertEqual( 37, self.pile.get() )
        self.assertFalse( self.pile.is_empty() )
        self.assertEqual( 2, self.pile.size() )

        # Get another item and make sure everything is ok.
        self.assertEqual( 40, self.pile.get() )
        self.assertFalse( self.pile.is_empty() )
        self.assertEqual( 1, self.pile.size() )

        # Get the last item and make sure everything is ok.
        self.assertEqual( 42, self.pile.get() )
        self.assertTrue( self.pile.is_empty() )
        self.assertEqual( 0, self.pile.size() )

    def test_3c( self ):
        """Test putting and getting three items with the third item in the middle of the first two."""
        # Put three items in and make sure everything is ok.
        self.pile.put( 37 )
        self.pile.put( 42 )
        self.pile.put( 40 )
        self.assertFalse( self.pile.is_empty() )
        self.assertEqual( 3, self.pile.size() )

        # Get one item and make sure everything is ok.
        self.assertEqual( 37, self.pile.get() )
        self.assertFalse( self.pile.is_empty() )
        self.assertEqual( 2, self.pile.size() )

        # Get another item and make sure everything is ok.
        self.assertEqual( 40, self.pile.get() )
        self.assertFalse( self.pile.is_empty() )
        self.assertEqual( 1, self.pile.size() )

        # Get the last item and make sure everything is ok.
        self.assertEqual( 42, self.pile.get() )
        self.assertTrue( self.pile.is_empty() )
        self.assertEqual( 0, self.pile.size() )

    def test_4( self ):
        """Test putting items, getting until the pile is empty, and then putting more items."""
        # Put three items in and make sure everything is ok.
        self.pile.put( 37 )
        self.pile.put( 42 )
        self.pile.put( 40 )
        self.assertFalse( self.pile.is_empty() )
        self.assertEqual( 3, self.pile.size() )

        # Get one item and make sure everything is ok.
        self.assertEqual( 37, self.pile.get() )
        self.assertFalse( self.pile.is_empty() )
        self.assertEqual( 2, self.pile.size() )

        # Get another item and make sure everything is ok.
        self.assertEqual( 40, self.pile.get() )
        self.assertFalse( self.pile.is_empty() )
        self.assertEqual( 1, self.pile.size() )

        # Get the last item and make sure everything is ok.
        self.assertEqual( 42, self.pile.get() )
        self.assertTrue( self.pile.is_empty() )
        self.assertEqual( 0, self.pile.size() )

        # Now repeat the same test to make sure the pile reaching the
        # empty state does not prevent it from continuing to work properly.
        self.pile.put( 37 )
        self.pile.put( 42 )
        self.pile.put( 40 )
        self.assertFalse( self.pile.is_empty() )
        self.assertEqual( 3, self.pile.size() )

        # Get one item and make sure everything is ok.
        self.assertEqual( 37, self.pile.get() )
        self.assertFalse( self.pile.is_empty() )
        self.assertEqual( 2, self.pile.size() )

        # Get another item and make sure everything is ok.
        self.assertEqual( 40, self.pile.get() )
        self.assertFalse( self.pile.is_empty() )
        self.assertEqual( 1, self.pile.size() )

        # Get the last item and make sure everything is ok.
        self.assertEqual( 42, self.pile.get() )
        self.assertTrue( self.pile.is_empty() )
        self.assertEqual( 0, self.pile.size() )

    def test_5( self ):
        """Test putting several items into the pile in various orders."""
        self.pile.put( 42 )  # First item.
        self.pile.put( 37 )  # Smaller than other items.
        self.pile.put( 40 )  # In middle of other items.
        self.pile.put( 29 )  # Smaller than other items.
        self.pile.put( 23 )  # Smaller than other items ... again.
        self.pile.put( 67 )  # Larger than other items.
        self.pile.put( 75 )  # Larger than other items ... again.

        self.assertEqual( 7, self.pile.size() )
        self.assertEqual( 23, self.pile.get() )
        self.assertEqual( 29, self.pile.get() )
        self.assertEqual( 37, self.pile.get() )
        self.assertEqual( 40, self.pile.get() )
        self.assertEqual( 42, self.pile.get() )
        self.assertEqual( 67, self.pile.get() )
        self.assertEqual( 75, self.pile.get() )

    def test_contains( self ):
        """Test the the contains methods."""
        self.pile.put( 42 )  # First item.
        self.pile.put( 37 )  # Smaller than other items.
        self.pile.put( 40 )  # In middle of other items.
        self.pile.put( 29 )  # Smaller than other items.
        self.pile.put( 23 )  # Smaller than other items ... again.
        self.pile.put( 67 )  # Larger than other items.
        self.pile.put( 75 )  # Larger than other items ... again.

        # Check the size of the pile.
        self.assertEqual( 7, self.pile.size() )

        # Make sure all the items are in the pile.
        self.assertTrue( self.pile.contains( 42 ) )
        self.assertTrue( self.pile.contains( 37 ) )
        self.assertTrue( self.pile.contains( 40 ) )
        self.assertTrue( self.pile.contains( 29 ) )
        self.assertTrue( self.pile.contains( 23 ) )
        self.assertTrue( self.pile.contains( 67 ) )
        self.assertTrue( self.pile.contains( 75 ) )

        # Check a few items that are not in the pile.
        self.assertFalse( self.pile.contains( 13 ) )  # Smaller than all items.
        self.assertFalse( self.pile.contains( 99 ) )  # Larger than all items.
        self.assertFalse( self.pile.contains( 41 ) )  # Somewhere in the middle.

    def test_str( self ):
        """Test the __str__() method."""
        self.pile.put( 42 )  # First item.
        self.pile.put( 37 )  # Smaller than other items.
        self.pile.put( 40 )  # In middle of other items.
        self.pile.put( 29 )  # Smaller than other items.
        self.pile.put( 23 )  # Smaller than other items ... again.
        self.pile.put( 67 )  # Larger than other items.
        self.pile.put( 75 )  # Larger than other items ... again.

        pile_string = str( self.pile )
        self.assertTrue( "42" in pile_string )
        self.assertTrue( "37" in pile_string )
        self.assertTrue( "40" in pile_string )
        self.assertTrue( "29" in pile_string )
        self.assertTrue( "23" in pile_string )
        self.assertTrue( "67" in pile_string )
        self.assertTrue( "75" in pile_string )

    def test_str_ordered( self ):
        """Test the __str__() method to see if items are in order.

        Note: This test is only expected to pass with pile implementations that
        use an ordered underlying data structure. This behavior is not mandatory
        for a pile, but it's an interesting test for those data structures that
        are kept in order.
        """
        self.pile.put( 42 )  # First item.
        self.pile.put( 37 )  # Smaller than other items.
        self.pile.put( 40 )  # In middle of other items.
        self.pile.put( 29 )  # Smaller than other items.
        self.pile.put( 23 )  # Smaller than other items ... again.
        self.pile.put( 67 )  # Larger than other items.
        self.pile.put( 75 )  # Larger than other items ... again.

        self.assertEqual( "23 29 37 40 42 67 75", str( self.pile ) )

    def large_random_test( self ):
        """Test a large, randomly shuffled data set."""
        # Create a large list of data and randomly shuffle it.
        data = list( range( 1024 ) )
        for i in range( len( data ) ):
            j = random.randint( 0, len( data ) )
            temp = data[ i ]
            data[ i ] = data[ j ]
            data[ j ] = temp

        # Quick test that the empty pile created in setup is ready to go.
        self.assertTrue( self.pile.is_empty() )
        self.assertEqual( 0, self.pile.size() )

        # Fill the pile with the random data.
        for i in range( len( data ) ):
            # Since the values in the data array are unique, it should not be there yet.
            self.assertFalse( self.pile.contains( data[ i ] ) )
            # Make sure the size is correct.
            self.assertEqual( i, self.pile.size() )
            # Put the random value in the machine.
            self.pile.put( data[ i ] )
            # Make sure the size is correct.
            self.assertEqual( i + 1, self.pile.size() )
            # Make sure it's there.
            self.assertTrue( self.pile.contains( data[ i ] ) )

        # Remove all items.
        for i in range( len( data ) ):
            # Make sure the size is correct.
            self.assertEqual( len( data ) - i, self.pile.size() )
            # Since the data contains all unique values between [1:data.length),
            # the items should come out of the Sorting Machine in that order.
            self.assertEqual( i, self.pile.get() )
            # Make sure the size is correct after the removal.
            self.assertEqual( len( data ) - i - 1, self.pile.size() )

    def test_exception( self ):
        """Test exception."""
        # Should not be able to get from an empty pile.
        with self.assertRaises( IndexError ):
            print( self.pile.get() )

    def test_iterator( self ):
        """Test the iterator."""
        # Create a list of items and put them in the pile.
        items = [ 42, 37, 40, 29, 23, 67, 75 ]
        for item in items:
            self.pile.put( item )
        self.assertEqual( 7, self.pile.size() )

        # Now iterate over every item in the pile.
        for item in self.pile:
            # Make sure the item produced by the iterator was originally put in the pile.
            self.assertTrue( item in items )
            # Make sure item produced by the iterator is actually in the pile.
            self.assertTrue( self.pile.contains( item ) )
            # Implementing the iterator allows use of the "in" operator.
            self.assertTrue( item in self.pile )

        # Now test explicitly using the iter() and next() functions.
        it = iter( self.pile )
        # Call next()
        for item in items:
            # Get the items using the next() function and ensure
            # they are in both the original items and the pile.
            item = next( it )
            self.assertTrue( item in items )
            self.assertTrue( item in self.pile )
            self.assertTrue( self.pile.contains( item ) )

        # Finally, make sure the iterator is empty after the above loop.
        with self.assertRaises( StopIteration ):
            next( it )


# Call main() if this file is being executed rather than imported.
if __name__ == '__main__':
    sys.argv.append( "-v" )  # Add the verbose command line flag.
    unittest.main()  # https://docs.python.org/3/library/__main__.html
