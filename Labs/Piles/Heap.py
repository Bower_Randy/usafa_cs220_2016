"""CS 220, Data Abstraction, Spring 2016, Dr. Bower.

This module contains a binary heap implementation of the Pile ADT.

The Pile data structure contains operations for insertion, removal,
membership test, emptiness test, and size calculation.

The crux of the Pile data structure is the removal operation always removes
and returns the smallest item. Thus, items placed into the container must
be comparable (i.e., they have implemented the __lt__ method).

Documentation: None required for non-graded lab exercises.
"""

# The threading module is needed in case the trees are being animated by a GUI.
# All references to threading and events can be ignored and the trees work fine.
import threading
from math import log2


class Heap( object ):
    """Implements a Pile ADT using a binary heap data structure."""

    def __init__( self, s=None ):
        """Creates an empty pile/heap.

        :param str s: String of space-delimited integer values to be added to the pile.
        """
        self._items = []

        # Create an event that another thread could use to pause and show the heap.
        self.permission_to_continue = threading.Event()
        # The event is initially set, so another thread must clear it to pause.
        # NOTE: If another thread clears the event, it must also set it or the heap may freeze!
        self.permission_to_continue.set()
        # When paused, a particular index/node can be highlighted.
        self.highlight_index = -1

        # If the string parameter is given and it is a string with only space-delimited
        # integer values, split the string and put the values in the pile.
        if s is not None and str( s ).replace( ' ', '' ).isdecimal():
            for item in s.split():
                self.put( int( item ) )

    def _pause( self, highlight_index=-1 ):
        """Pause so another thread could show the heap."""
        # When paused, a particular index/node can be highlighted.
        # Save this as a class attribute so a GUI has access.
        self.highlight_index = highlight_index
        # If the event is never cleared by another thread, the heap cruises along, never waiting.
        # NOTE: If another thread clears the event, it must also set it or the heap may freeze!
        self.permission_to_continue.wait()

    def __str__( self ):
        """Returns a string of all items in the pile/heap."""
        # Use the in-order traversal to create a string of the items in the pile/heap.
        return " ".join( str( item ) for item in self._items )

    def __iter__( self ):
        """Returns an iterator object over the items in the pile/heap."""
        # Use Python's built-in list iterator with the list of items in the heap.
        return iter( self._items )

    def __eq__( self, other ):
        """Compares this heap to another heap, checking only content.

        :param BinaryHeap other: The heap to be compared.
        :return: True if the heaps are equal; False otherwise.
        :rtype: bool
        """
        return sorted( self._items ) == sorted( other._items )

    def copy( self ):
        """Returns a copy of the heap."""
        heap = Heap()
        heap._items = list( self._items )  # Makes a copy of the list.
        return heap

    def put( self, item ):
        """Adds the given item to the pile/heap.

        This operation is O( __ ).

        :param item: The item to be added to the pile/heap.
        """
        # Append the item to the list of items and then push it up to maintain heap-ness.
        self._items.append( item )
        # Call the recursive helper with the index of the newly added item.
        self._push_up( len( self._items ) - 1 )
        # Set highlight_index so the last node isn't left highlighted.
        self.highlight_index = -1

    def _push_up( self, index ):
        # Private, recursive helper for the put method.

        # TODO - Lab 27: Implement the method as described in the lab document.
        pass  # Remove the pass statement (and this comment) when writing your own code.

    def get( self ):
        """Removes and returns the smallest item in the pile/heap.

        This operation is O( __ ).

        :return: The smallest item in this pile/heap.
        """
        if self.is_empty():
            raise IndexError( "ERROR: Cannot get from empty pile." )
        else:
            # Pause to show the item being removed.
            self._pause( 0 )
            # Pause to show the item being moved to the root, before it's pushed down.
            self._pause( len( self._items ) - 1 )

            # The smallest item in the heap is the first in the list of items.
            item = self._items[ 0 ]
            # Copy the last item in the list to the first.
            self._items[ 0 ] = self._items[ -1 ]
            # Actually remove the last item from the list.
            self._items.pop()
            # Push the first item down to maintain heap-ness.
            self._push_down( 0 )
            # Set highlight_index so the last node isn't left highlighted.
            self.highlight_index = -1
            # Return the item.
            return item

    def _push_down( self, index ):
        # Private, recursive helper for the get method.

        # TODO - Lab 27: Implement the method as described in the lab document.
        pass  # Remove the pass statement (and this comment) when writing your own code.

    def contains( self, item ):
        """Determines if the given item is in the pile/heap.

        This operation is O( __ ).

        :param item: The item for which the pile is to be searched.
        :return: True if the item is in the pile; False otherwise.
        :rtype: bool
        """
        # Since there is no left-right ordering of items/branches, as in
        # a binary search tree the entire list of items must be searched.
        return item in self._items

    def is_empty( self ):
        """Determines if the pile/heap is empty.

        This operation is O( 1 ).

        :return: True if the pile/heap is empty; False otherwise.
        :rtype: bool
        """
        return not self._items  # A non-empty list tests as True.

    def size( self ):
        """Determines the size of the pile/heap.

        This operation is O( __ )

        :return: The number of items in the pile/heap.
        :rtype: int
        """
        return len( self._items )

    def height( self ):
        """Determines the height of the heap/tree.

        :return: The height of the heap.
        :rtype: int
        """
        return -1 if self.is_empty() else int( log2( self.size() ) )

    # Q: Why are the following three methods marked as "static"?
    # A: They only need the value of the parameter to do their work and are NOT dependent on
    #    the current state of the object, so they can be "static" which means all instances
    #    of this class would use/share these methods (i.e., there is no need for each instance
    #    to have a separate copy of such methods ... there can be only one).

    @staticmethod
    def _parent( index ):
        # Returns the index of the parent of the item at the given index.
        return ( index - 1 ) // 2  # Integer division!

    @staticmethod
    def _left_child( index ):
        # Returns the index of the left child of the item at the given index.
        return index * 2 + 1

    @staticmethod
    def _right_child( index ):
        # Returns the index of the right child of the item at the given index.
        return index * 2 + 2
