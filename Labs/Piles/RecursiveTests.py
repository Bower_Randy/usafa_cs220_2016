"""CS 220, Data Abstraction, Spring 2016, Dr. Bower.

This module contains a unit tests of the merge method
in the recursive, linked, ordered pile.

Documentation: None.
"""

import unittest
import sys

from Labs.Piles.Recursive import RecursivePile, RecursiveOrderedPile


# TODO - Lab 13: Read, discuss, and understand the following code.
class UnitTests( unittest.TestCase ):
    """For more information about Python's unittest module, see
    https://docs.python.org/3/library/unittest.html
    """

    def test_parse_1( self ):
        """Test putting several items into the pile using the parse method."""
        pile = RecursivePile( "42 37 40 29 23 67 75" )
        self.assertFalse( pile.is_empty() )
        self.assertEqual( 7, pile.size() )
        for item in [ 23, 29, 37, 40, 42, 67, 75 ]:
            self.assertEqual( item, pile.get() )
        self.assertTrue( pile.is_empty() )

    def test_parse_2( self ):
        """Test putting several items into the pile using the parse method."""
        pile = RecursiveOrderedPile( "42 37 40 29 23 67 75" )
        self.assertFalse( pile.is_empty() )
        self.assertEqual( 7, pile.size() )
        for item in [ 23, 29, 37, 40, 42, 67, 75 ]:
            self.assertEqual( item, pile.get() )
        self.assertTrue( pile.is_empty() )

    def test_merge_1( self ):
        """Test merging one list with all odd numbers and one list with all even
        numbers such that the merge takes an item from each list "zipper" style.
        """
        # Create a couple lists to merge.
        odd = RecursiveOrderedPile( "1 3 5 7 9" )
        even = RecursiveOrderedPile( "2 4 6 8" )
        # Test the merge both ways.
        self.assertEqual( "1 2 3 4 5 6 7 8 9", str( odd.merge( even ) ) )
        self.assertEqual( "1 2 3 4 5 6 7 8 9", str( even.merge( odd ) ) )
        # Make sure the merge operation did not change either of the original lists.
        self.assertEqual( "1 3 5 7 9", str( odd ) )
        self.assertEqual( "2 4 6 8", str( even ) )

    def test_merge_2( self ):
        """Test merging two lists such that all of one list comes before all of the other."""
        # Create a couple lists to merge.
        small = RecursiveOrderedPile( "1 2 3 4 5" )
        large = RecursiveOrderedPile( "6 7 8 9" )
        # Test the merge both ways.
        self.assertEqual( "1 2 3 4 5 6 7 8 9", str( small.merge( large ) ) )
        self.assertEqual( "1 2 3 4 5 6 7 8 9", str( large.merge( small ) ) )
        # Make sure the merge operation did not change either of the original lists.
        self.assertEqual( "1 2 3 4 5", str( small ) )
        self.assertEqual( "6 7 8 9", str( large ) )

    def test_merge_3( self ):
        """Test merging two lists such that one list contains items on the ends
        and the second list contains items in the middle.
        """
        # Create a couple lists to merge.
        ends = RecursiveOrderedPile( "1 2 3 8 9" )
        middle = RecursiveOrderedPile( "4 5 6 7" )
        # Test the merge both ways.
        self.assertEqual( "1 2 3 4 5 6 7 8 9", str( ends.merge( middle ) ) )
        self.assertEqual( "1 2 3 4 5 6 7 8 9", str( middle.merge( ends ) ) )
        # Make sure the merge operation did not change either of the original lists.
        self.assertEqual( "1 2 3 8 9", str( ends ) )
        self.assertEqual( "4 5 6 7", str( middle ) )

    def test_merge_4( self ):
        """Test merging two lists when one of the lists is empty."""
        # Create a couple lists to merge.
        full = RecursiveOrderedPile( "1 2 3 4 5 6 7 8 9" )
        empty = RecursiveOrderedPile()
        # Test the merge both ways.
        self.assertEqual( "1 2 3 4 5 6 7 8 9", str( full.merge( empty ) ) )
        self.assertEqual( "1 2 3 4 5 6 7 8 9", str( empty.merge( full ) ) )
        # Make sure the merge operation did not change either of the original lists.
        self.assertEqual( "1 2 3 4 5 6 7 8 9", str( full ) )
        self.assertEqual( "", str( empty ) )


# Call main() if this file is being executed rather than imported.
if __name__ == '__main__':
    sys.argv.append( "-v" )  # Add the verbose command line flag.
    unittest.main()  # https://docs.python.org/3/library/__main__.html
