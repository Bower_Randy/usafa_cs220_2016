"""CS 220, Data Abstraction, Spring 2016, Dr. Bower.

This module contains a timed comparison of binary tree implementations.

Documentation: None.
"""

from random import sample
from timeit import timeit
from Labs.Piles.Trees import *

# How many times to run each test.
HOW_MANY_TESTS = 2 ** 4

# Create a list of data to use as the items in the trees.
HOW_MANY_ITEMS = 2 ** 8
DATA = sample( range( HOW_MANY_ITEMS * 2 ), HOW_MANY_ITEMS )


# TODO - Lab 24: Read, discuss, and understand the following code.
def main():
    """Main program to run the tests."""

    t0 = timeit( "test_binary_tree()", setup="from __main__ import test_binary_tree", number=HOW_MANY_TESTS )
    print( " t0 = {:.2f}".format( t0 ) )

    t1 = timeit( "test_bst()", setup="from __main__ import test_bst", number=HOW_MANY_TESTS )
    print( " t1 = {:.2f}".format( t1 ) )

    # t2 = timeit( "test_avl()", setup="from __main__ import test_avl", number=HOW_MANY_TESTS )
    # print( " t2 = {:.2f}".format( t2 ) )

    # Note: As implemented, the AVL Tree is correct, but may not show the expected
    # performance increase over the Binary Search Tree. This is almost certainly due
    # to the inefficient, repeated calculation of the balance factor of each node.
    # Challenge: Include the height and/or balance factor of a node as an attribute
    # of the Node class and only re-calculate the value when necessary.


def test_tree( tree ):
    """Tests the put(), contains(), and get() methods of the given binary tree.

    Note: The given tree may be a plain binary tree, a binary search tree, or an AVL tree.

    :param BinaryTrees.BinaryTree tree: The tree to be tested.
    """
    # Show progress.
    print( ".", end="", flush=True )

    # Put all the data items in the tree. To see worst-case performance
    # of a Binary Search Tree, change DATA to sorted( DATA ).
    # Note: Do not do data.sort(), which actually changes the DATA list.
    for item in DATA:
        tree.put( item )

    # Search for items, half of them should be there, half should not.
    # The range of items is twice the number actually in the data.
    count = 0
    for item in range( HOW_MANY_ITEMS * 2 ):
        if tree.contains( item ):
            count += 1
    if count != HOW_MANY_ITEMS:
        print( "ERROR: tree.contains() expected {} matches but found {}.".format( HOW_MANY_ITEMS, count ), flush=True )

    # Remove all items, ensuring the smallest item is removed each time.
    previous = tree.get()
    for _ in range( HOW_MANY_ITEMS - 1 ):
        current = tree.get()
        if current < previous:
            print( "ERROR: tree.get() returned {} after {}.".format( current, previous ), flush=True )
        previous = current

    if not tree.is_empty():
        print( "ERROR: tree is not empty after getting {} items.".format( HOW_MANY_ITEMS ), flush=True )


def test_binary_tree():
    """Tests a plain binary tree."""
    test_tree( BinaryTree() )


def test_bst():
    """Tests a binary search tree."""
    test_tree( BinarySearchTree() )


def test_avl():
    """Tests an AVL tree."""
    test_tree( AVLTree() )


if __name__ == "__main__":
    main()
