"""CS 220, Data Abstraction, Spring 2016, Dr. Bower.

This module contains tests for AVL Trees, which in turn test
methods inherited from Binary Tree and Binary Search Tree.

Documentation: None.
"""

import unittest
import sys

from Labs.Piles.Trees import AVLTree


# TODO - Lab 24: Read, discuss, and understand the following code.
class UnitTests( unittest.TestCase ):
    """For more information about Python's unittest module, see
    https://docs.python.org/3/library/unittest.html
    """

    # Creates My Favorite Tree (MFT) for comparison in various tests;
    # done here since this tree never changes and one copy can be shared.
    #           42
    #       25      75
    #     13  37  67  88
    # Note: The tree is created as a plain Binary Tree, with careful
    # insertion order producing the exact tree pictured above.
    MFT = AVLTree( "42 25 75 13 67 37 88" )

    def setUp( self ):
        """This method is called immediately before calling each test method."""
        # Creates a perfectly balanced tree with 15 nodes, 50 as the root, and
        # all multiples of 5 between 15 and 85, with no rotations required:
        #                   50
        #           30              70
        #       20      40      60      80
        #     15  25  35  45  55  65  75  85
        self.avl_tree = AVLTree( "50 30 70 20 40 60 80 15 25 35 45 55 65 75 85" )
        # Create an empty AVl tree to be used in various tests.
        self.empty_avl_tree = AVLTree()

    def test_mft( self ):
        """Test the plain binary tree built for comparison in other tests."""
        # Note: These tests cover methods not tested in UnitTests.py, i.e.,
        # put(), contains(), and get(), which in turn tests remove().
        # The tree:
        #           42
        #       25      75
        #     13  37  67  88
        self.assertTrue( UnitTests.MFT.size() == 7 )
        self.assertTrue( UnitTests.MFT.height() == 2 )
        self.assertTrue( "42 25 12 37 67 75 88", str( UnitTests.MFT ) )
        self.assertEquals( "42 25 13 37 75 67 88",
                           " ".join( str( item ) for item in UnitTests.MFT.pre_order() ) )
        self.assertEquals( "13 37 25 67 88 75 42",
                           " ".join( str( item ) for item in UnitTests.MFT.post_order() ) )
        # If the __eq__ method is correct, the three should be equal to itself!
        self.assertTrue( UnitTests.MFT == UnitTests.MFT )
        # If the copy() method is correct, a copy should equal the original.
        self.assertTrue( UnitTests.MFT == UnitTests.MFT.copy() )

    def test_avl_left_left( self ):
        """Inserting values in descending order causes left-left rotations."""
        tree = self.empty_avl_tree
        # Rotations happen upon insertion of 67, 37, 25, and 13.
        for item in [ 88, 75, 67, 42, 37, 25, 13 ]:
            tree.put( item )
        self.assertTrue( tree == UnitTests.MFT )

    def test_avl_right_right( self ):
        """Inserting values in ascending order causes right-right rotations."""
        tree = self.empty_avl_tree
        # Rotations happen upon insertion of 37, 67, 75, and 88.
        for item in [ 13, 25, 37, 42, 67, 75, 88 ]:
            tree.put( item )
        self.assertTrue( tree == UnitTests.MFT )

    def test_avl_left_right( self ):
        """Inserting values in this order cases a left-right rotation on 37."""
        tree = self.empty_avl_tree
        # Rotations happen upon insertion of 37.
        for item in [ 75, 88, 25, 13, 42, 37, 67 ]:
            tree.put( item )
        self.assertTrue( tree == UnitTests.MFT )

    def test_avl_right_left( self ):
        """Inserting values in this order cases a right-left rotation on 67."""
        tree = self.empty_avl_tree
        # Rotations happen upon insertion of 67.
        for item in [ 25, 13, 75, 42, 88, 67, 37 ]:
            tree.put( item )
        self.assertTrue( tree == UnitTests.MFT )

    def test_avl_setup_tree( self ):
        """Make sure the tree created in the setup method is correct."""
        self.assertEquals( "15 20 25 30 35 40 45 50 55 60 65 70 75 80 85",
                           str( self.avl_tree ) )  # The __str__ method uses the in_order traversal.
        self.assertEquals( "50 30 20 15 25 40 35 45 70 60 55 65 80 75 85",
                           " ".join( str( item ) for item in self.avl_tree.pre_order() ) )
        self.assertEquals( "15 25 20 35 45 40 30 55 65 60 75 85 80 70 50",
                           " ".join( str( item ) for item in self.avl_tree.post_order() ) )

    """These following removal tests are done by removing leaves, but the inherited binary tree
    remove method swaps an internal node to be removed with a leaf value, then removes the leaf.
    Thus, any balancing that must be done only happens after a leaf value is removed.
    """

    def test_avl_remove_leaves( self ):
        """Removes all leaves one at a time, making sure nothing rotates when it shouldn't.
                          50
                  30              70
              20      40      60      80
            15  25  35  45  55  65  75  85
        """
        self.avl_tree.remove( 37 )
        self.assertEquals( "15 20 25 30 35 40 45 50 55 60 65 70 75 80 85", str( self.avl_tree ) )
        self.avl_tree.remove( 15 )
        self.assertEquals( "20 25 30 35 40 45 50 55 60 65 70 75 80 85", str( self.avl_tree ) )
        self.avl_tree.remove( 25 )
        self.assertEquals( "20 30 35 40 45 50 55 60 65 70 75 80 85", str( self.avl_tree ) )
        self.avl_tree.remove( 35 )
        self.assertEquals( "20 30 40 45 50 55 60 65 70 75 80 85", str( self.avl_tree ) )
        self.avl_tree.remove( 45 )
        self.assertEquals( "20 30 40 50 55 60 65 70 75 80 85", str( self.avl_tree ) )
        self.avl_tree.remove( 55 )
        self.assertEquals( "20 30 40 50 60 65 70 75 80 85", str( self.avl_tree ) )
        self.avl_tree.remove( 65 )
        self.assertEquals( "20 30 40 50 60 70 75 80 85", str( self.avl_tree ) )
        self.avl_tree.remove( 75 )
        self.assertEquals( "20 30 40 50 60 70 80 85", str( self.avl_tree ) )
        self.avl_tree.remove( 85 )
        self.assertEquals( "20 30 40 50 60 70 80", str( self.avl_tree ) )
        self.avl_tree.remove( 20 )
        self.assertEquals( "30 40 50 60 70 80", str( self.avl_tree ) )
        self.avl_tree.remove( 40 )
        self.assertEquals( "30 50 60 70 80", str( self.avl_tree ) )
        self.avl_tree.remove( 60 )
        self.assertEquals( "30 50 70 80", str( self.avl_tree ) )
        self.avl_tree.remove( 80 )
        self.assertEquals( "30 50 70", str( self.avl_tree ) )
        self.avl_tree.remove( 70 )
        self.assertEquals( "30 50", str( self.avl_tree ) )
        self.avl_tree.remove( 30 )
        self.assertEquals( "50", str( self.avl_tree ) )
        self.avl_tree.remove( 50 )
        self.assertEquals( "", str( self.avl_tree ) )

    def test_avl_remove_root( self ):
        """Repeatedly removes the root until entire tree is gone, ensuring
        rotations happen as necessary to keep the tree balanced.
                          50
                  30              70
              20      40      60      80
            15  25  35  45  55  65  75  85
        """
        self.assertEquals( "15 20 25 30 35 40 45 50 55 60 65 70 75 80 85", str( self.avl_tree ) )
        self.assertEquals( "50 30 20 15 25 40 35 45 70 60 55 65 80 75 85",
                           " ".join( str( item ) for item in self.avl_tree.pre_order() ) )
        self.avl_tree.remove( 50 )
        #               45
        #       30              70
        #   20      40      60      80
        # 15  25  35      55  65  75  85
        self.assertEquals( "15 20 25 30 35 40 45 55 60 65 70 75 80 85", str( self.avl_tree ) )
        self.assertEquals( "45 30 20 15 25 40 35 70 60 55 65 80 75 85",
                           " ".join( str( item ) for item in self.avl_tree.pre_order() ) )

        self.avl_tree.remove( 45 )
        #               40
        #       30              70
        #   20      35      60      80
        # 15  25          55  65  75  85
        self.assertEquals( "15 20 25 30 35 40 55 60 65 70 75 80 85", str( self.avl_tree ) )
        self.assertEquals( "40 30 20 15 25 35 70 60 55 65 80 75 85",
                           " ".join( str( item ) for item in self.avl_tree.pre_order() ) )

        self.avl_tree.remove( 40 )
        #               35
        #       20              70
        #   15      30      60      80
        #         25      55  65  75  85
        self.assertEquals( "15 20 25 30 35 55 60 65 70 75 80 85", str( self.avl_tree ) )
        self.assertEquals( "35 20 15 30 25 70 60 55 65 80 75 85",
                           " ".join( str( item ) for item in self.avl_tree.pre_order() ) )

        self.avl_tree.remove( 35 )
        #               30
        #       20              70
        #   15      25      60      80
        #                 55  65  75  85
        self.assertEquals( "15 20 25 30 55 60 65 70 75 80 85", str( self.avl_tree ) )
        self.assertEquals( "30 20 15 25 70 60 55 65 80 75 85",
                           " ".join( str( item ) for item in self.avl_tree.pre_order() ) )

        self.avl_tree.remove( 30 )
        #               25
        #       20              70
        #   15              60      80
        #                 55  65  75  85
        self.assertEquals( "15 20 25 55 60 65 70 75 80 85", str( self.avl_tree ) )
        self.assertEquals( "25 20 15 70 60 55 65 80 75 85",
                           " ".join( str( item ) for item in self.avl_tree.pre_order() ) )

        self.avl_tree.remove( 25 )
        #               70
        #       20              80
        #   15      60      75      85
        #         55  65
        self.assertEquals( "15 20 55 60 65 70 75 80 85", str( self.avl_tree ) )
        self.assertEquals( "70 20 15 60 55 65 80 75 85",
                           " ".join( str( item ) for item in self.avl_tree.pre_order() ) )

        self.avl_tree.remove( 70 )
        #               65
        #       20              80
        #   15      60      75      85
        #         55
        self.assertEquals( "15 20 55 60 65 75 80 85", str( self.avl_tree ) )
        self.assertEquals( "65 20 15 60 55 80 75 85",
                           " ".join( str( item ) for item in self.avl_tree.pre_order() ) )

        self.avl_tree.remove( 65 )
        #               60
        #       20              80
        #   15      55      75      85
        self.assertEquals( "15 20 55 60 75 80 85", str( self.avl_tree ) )
        self.assertEquals( "60 20 15 55 80 75 85",
                           " ".join( str( item ) for item in self.avl_tree.pre_order() ) )

        self.avl_tree.remove( 60 )
        #               55
        #       20              80
        #   15              75      85
        self.assertEquals( "15 20 55 75 80 85", str( self.avl_tree ) )
        self.assertEquals( "55 20 15 80 75 85",
                           " ".join( str( item ) for item in self.avl_tree.pre_order() ) )

        self.avl_tree.remove( 55 )
        #               20
        #       15              80
        #                   75      85
        self.assertEquals( "15 20 75 80 85", str( self.avl_tree ) )
        self.assertEquals( "20 15 80 75 85",
                           " ".join( str( item ) for item in self.avl_tree.pre_order() ) )

        self.avl_tree.remove( 20 )
        #               80
        #       15              85
        #           75
        self.assertEquals( "15 75 80 85", str( self.avl_tree ) )
        self.assertEquals( "80 15 75 85",
                           " ".join( str( item ) for item in self.avl_tree.pre_order() ) )

        self.avl_tree.remove( 80 )
        #               75
        #       15              85
        self.assertEquals( "15 75 85", str( self.avl_tree ) )
        self.assertEquals( "75 15 85",
                           " ".join( str( item ) for item in self.avl_tree.pre_order() ) )

        self.avl_tree.remove( 75 )
        #               15
        #                       85
        self.assertEquals( "15 85", str( self.avl_tree ) )
        self.assertEquals( "15 85",
                           " ".join( str( item ) for item in self.avl_tree.pre_order() ) )

        self.avl_tree.remove( 15 )
        #               85
        self.assertEquals( "85", str( self.avl_tree ) )
        self.assertEquals( "85",
                           " ".join( str( item ) for item in self.avl_tree.pre_order() ) )

        self.avl_tree.remove( 85 )
        self.assertEquals( "", str( self.avl_tree ) )
        self.assertEquals( "",
                           " ".join( str( item ) for item in self.avl_tree.pre_order() ) )
        self.assertTrue( self.avl_tree.is_empty() )

    def test_avl_remove_1( self ):
        """Remove the sub-tree rooted at 20.
                          50
                  30              70
              20      40      60      80
            15  25  35  45  55  65  75  85
        Resulting tree:
                          50
                  40              70
              30      45      60      80
                35          55  65  75  85
        """
        self.assertEquals( "15 20 25 30 35 40 45 50 55 60 65 70 75 80 85", str( self.avl_tree ) )
        self.assertEquals( "50 30 20 15 25 40 35 45 70 60 55 65 80 75 85",
                           " ".join( str( item ) for item in self.avl_tree.pre_order() ) )
        self.avl_tree.remove( 15 )
        self.avl_tree.remove( 25 )
        self.avl_tree.remove( 20 )  # This causes a rotation.
        self.assertEquals( "30 35 40 45 50 55 60 65 70 75 80 85", str( self.avl_tree ) )
        self.assertEquals( "50 40 30 35 45 70 60 55 65 80 75 85",
                           " ".join( str( item ) for item in self.avl_tree.pre_order() ) )

    def test_avl_remove_2( self ):
        """Remove the sub-tree rooted at 40.
                          50
                  30              70
              20      40      60      80
            15  25  35  45  55  65  75  85
        Resulting tree:
                          50
                  20              70
              15      30      60      80
                    25      55  65  75  85
        """
        self.assertEquals( "15 20 25 30 35 40 45 50 55 60 65 70 75 80 85", str( self.avl_tree ) )
        self.assertEquals( "50 30 20 15 25 40 35 45 70 60 55 65 80 75 85",
                           " ".join( str( item ) for item in self.avl_tree.pre_order() ) )
        self.avl_tree.remove( 35 )
        self.avl_tree.remove( 40 )
        self.avl_tree.remove( 45 )  # This causes a rotation.
        self.assertEquals( "15 20 25 30 50 55 60 65 70 75 80 85", str( self.avl_tree ) )
        self.assertEquals( "50 20 15 30 25 70 60 55 65 80 75 85",
                           " ".join( str( item ) for item in self.avl_tree.pre_order() ) )

    def test_avl_remove_3( self ):
        """Remove the sub-tree rooted at 60.
                          50
                  30              70
              20      40      60      80
            15  25  35  45  55  65  75  85
        Resulting tree:
                          50
                  30              80
              20      40      70      85
            15  25  35  45      75
        """
        self.assertEquals( "15 20 25 30 35 40 45 50 55 60 65 70 75 80 85", str( self.avl_tree ) )
        self.assertEquals( "50 30 20 15 25 40 35 45 70 60 55 65 80 75 85",
                           " ".join( str( item ) for item in self.avl_tree.pre_order() ) )
        self.avl_tree.remove( 60 )
        self.avl_tree.remove( 65 )
        self.avl_tree.remove( 55 )  # This causes a rotation.
        self.assertEquals( "15 20 25 30 35 40 45 50 70 75 80 85", str( self.avl_tree ) )
        self.assertEquals( "50 30 20 15 25 40 35 45 80 70 75 85",
                           " ".join( str( item ) for item in self.avl_tree.pre_order() ) )

    def test_avl_remove_4( self ):
        """Remove the sub-tree rooted at 80.
                          50
                  30              70
              20      40      60      80
            15  25  35  45  55  65  75  85
        Resulting tree:
                          50
                  30              60
              20      40      55      70
            15  25  35  45          65
        """
        self.assertEquals( "15 20 25 30 35 40 45 50 55 60 65 70 75 80 85", str( self.avl_tree ) )
        self.assertEquals( "50 30 20 15 25 40 35 45 70 60 55 65 80 75 85",
                           " ".join( str( item ) for item in self.avl_tree.pre_order() ) )
        self.avl_tree.remove( 75 )
        self.avl_tree.remove( 85 )
        self.avl_tree.remove( 80 )  # This causes a rotation.
        self.assertEquals( "15 20 25 30 35 40 45 50 55 60 65 70", str( self.avl_tree ) )
        self.assertEquals( "50 30 20 15 25 40 35 45 60 55 70 65",
                           " ".join( str( item ) for item in self.avl_tree.pre_order() ) )

    """Tests 5 through 8 could (should?) be repeated on the side of the tree rooted at 70, but
    these are exactly symmetrical sub-trees below the root and should therefore behave the same.
    """

    def test_avl_remove_5( self ):
        """Add 12 and 18, then remove 65 and the subtree rooted at 80.
                            50
                    30              70
                20      40      60      80
              15  25  35  45  55  65  75  85
             1218
        The first rotation at node 70 creates the following tree:
                            50
                    30              60
                20      40      55      70
              15  25  35  45
             1218
        A second rotation at node 50 results in a balanced tree:
                            30
                    20              50
                15      25      40      60
              12  18         35  45   55  70
        """
        self.avl_tree.put( 12 )
        self.avl_tree.put( 18 )
        self.assertEquals( "12 15 18 20 25 30 35 40 45 50 55 60 65 70 75 80 85", str( self.avl_tree ) )
        self.assertEquals( "50 30 20 15 12 18 25 40 35 45 70 60 55 65 80 75 85",
                           " ".join( str( item ) for item in self.avl_tree.pre_order() ) )
        self.avl_tree.remove( 65 )
        self.avl_tree.remove( 75 )
        self.avl_tree.remove( 85 )
        self.avl_tree.remove( 80 )  # This causes the rotations.
        self.assertEquals( "12 15 18 20 25 30 35 40 45 50 55 60 70", str( self.avl_tree ) )
        self.assertEquals( "30 20 15 12 18 25 50 40 35 45 60 55 70",
                           " ".join( str( item ) for item in self.avl_tree.pre_order() ) )

    def test_avl_remove_6( self ):
        """Add 22 and 28, then remove 65 and the subtree rooted at 80.
                            50
                    30              70
                20      40      60      80
              15  25  35  45  55  65  75  85
                 2228
        The first rotation at node 70 creates the following tree:
                            50
                    30              60
                20      40      55      70
              15  25  35  45
                 2228
        A second rotation at node 50 results in a balanced tree:
                            30
                    20              50
                15      25      40      60
                      22  28  35  45  55  70
        """
        self.avl_tree.put( 22 )
        self.avl_tree.put( 28 )
        self.assertEquals( "15 20 22 25 28 30 35 40 45 50 55 60 65 70 75 80 85", str( self.avl_tree ) )
        self.assertEquals( "50 30 20 15 25 22 28 40 35 45 70 60 55 65 80 75 85",
                           " ".join( str( item ) for item in self.avl_tree.pre_order() ) )
        self.avl_tree.remove( 65 )
        self.avl_tree.remove( 75 )
        self.avl_tree.remove( 85 )
        self.avl_tree.remove( 80 )  # This causes the rotations.
        self.assertEquals( "15 20 22 25 28 30 35 40 45 50 55 60 70", str( self.avl_tree ) )
        self.assertEquals( "30 20 15 25 22 28 50 40 35 45 60 55 70",
                           " ".join( str( item ) for item in self.avl_tree.pre_order() ) )

    def test_avl_remove_7( self ):
        """Add 33 and 37, then remove 75 and the subtree rooted at 60.
                            50
                    30              70
                20      40      60      80
              15  25  35  45  55  65  75  85
                     3337
        The first rotation at node 70 creates the following tree:
                            50
                    30              80
                20      40      70      85
              15  25  35  45
                     3337
        A second rotation at node 30 results in a the following tree:
                            50
                    40              80
                30      45      70      85
              20  35
             15253337
        One final rotation at node 50 results in a balanced tree:
                            40
                    30              50
                20      35      45      80
              15  25  33  37          70  85
        """
        self.avl_tree.put( 33 )
        self.avl_tree.put( 37 )
        self.assertEquals( "15 20 25 30 33 35 37 40 45 50 55 60 65 70 75 80 85", str( self.avl_tree ) )
        self.assertEquals( "50 30 20 15 25 40 35 33 37 45 70 60 55 65 80 75 85",
                           " ".join( str( item ) for item in self.avl_tree.pre_order() ) )
        self.avl_tree.remove( 75 )
        self.avl_tree.remove( 55 )
        self.avl_tree.remove( 60 )
        self.avl_tree.remove( 65 )  # This causes the rotations.
        self.assertEquals( "15 20 25 30 33 35 37 40 45 50 70 80 85", str( self.avl_tree ) )
        self.assertEquals( "40 30 20 15 25 35 33 37 50 45 80 70 85",
                           " ".join( str( item ) for item in self.avl_tree.pre_order() ) )

    def test_avl_remove_8( self ):
        """Add 42 and 48, then remove 75 and the subtree rooted at 60.
                            50
                    30              70
                20      40      60      80
              15  25  35  45  55  65  75  85
                         4248
        The first rotation at node 70 creates the following tree:
                            50
                    30              80
                20      40      70      85
              15  25  35  45
                         4248
        A second rotation at node 30 results in a the following tree:
                            50
                    40              80
                30      45      70      85
              20  35  42  48
             1525
        One final rotation at node 50 results in a balanced tree:
                            40
                    30              50
                20      35      45      80
              15  25          42  48  70  85
        """
        self.avl_tree.put( 42 )
        self.avl_tree.put( 48 )
        self.assertEquals( "15 20 25 30 35 40 42 45 48 50 55 60 65 70 75 80 85", str( self.avl_tree ) )
        self.assertEquals( "50 30 20 15 25 40 35 45 42 48 70 60 55 65 80 75 85",
                           " ".join( str( item ) for item in self.avl_tree.pre_order() ) )
        self.avl_tree.remove( 75 )
        self.avl_tree.remove( 60 )
        self.avl_tree.remove( 65 )
        self.avl_tree.remove( 55 )  # This causes the rotations.
        self.assertEquals( "15 20 25 30 35 40 42 45 48 50 70 80 85", str( self.avl_tree ) )
        self.assertEquals( "40 30 20 15 25 35 50 45 42 48 80 70 85",
                           " ".join( str( item ) for item in self.avl_tree.pre_order() ) )


# Call main() if this file is being executed rather than imported.
if __name__ == '__main__':
    sys.argv.append( "-v" )  # Add the verbose command line flag.
    unittest.main()  # https://docs.python.org/3/library/__main__.html
