# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'C:/Users/Randall.Bower/Documents/Courses/CS220/USAFA_CS220_2016/Labs/Graphs/GraphGui.ui'
#
# Created: Thu Jan 14 08:27:30 2016
#      by: PyQt4 UI code generator 4.11.2
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_GraphGui(object):
    def setupUi(self, GraphGui):
        GraphGui.setObjectName(_fromUtf8("GraphGui"))
        GraphGui.resize(989, 511)
        palette = QtGui.QPalette()
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Base, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Window, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Base, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Window, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Base, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Window, brush)
        GraphGui.setPalette(palette)
        self.centralwidget = QtGui.QWidget(GraphGui)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.centralwidget.sizePolicy().hasHeightForWidth())
        self.centralwidget.setSizePolicy(sizePolicy)
        self.centralwidget.setMinimumSize(QtCore.QSize(0, 0))
        self.centralwidget.setMaximumSize(QtCore.QSize(16777215, 16777215))
        palette = QtGui.QPalette()
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Base, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Window, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Base, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Window, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Base, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Window, brush)
        self.centralwidget.setPalette(palette)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.window_layout = QtGui.QHBoxLayout(self.centralwidget)
        self.window_layout.setObjectName(_fromUtf8("window_layout"))
        self.main_layout = QtGui.QHBoxLayout()
        self.main_layout.setObjectName(_fromUtf8("main_layout"))
        self.drawing_widget = QtGui.QWidget(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(1)
        sizePolicy.setVerticalStretch(1)
        sizePolicy.setHeightForWidth(self.drawing_widget.sizePolicy().hasHeightForWidth())
        self.drawing_widget.setSizePolicy(sizePolicy)
        self.drawing_widget.setMinimumSize(QtCore.QSize(160, 90))
        self.drawing_widget.setMaximumSize(QtCore.QSize(16777215, 16777215))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Courier New"))
        font.setPointSize(14)
        font.setBold(True)
        font.setWeight(75)
        self.drawing_widget.setFont(font)
        self.drawing_widget.setFocusPolicy(QtCore.Qt.StrongFocus)
        self.drawing_widget.setObjectName(_fromUtf8("drawing_widget"))
        self.main_layout.addWidget(self.drawing_widget)
        self.vertical_line = QtGui.QFrame(self.centralwidget)
        self.vertical_line.setFrameShape(QtGui.QFrame.VLine)
        self.vertical_line.setFrameShadow(QtGui.QFrame.Sunken)
        self.vertical_line.setObjectName(_fromUtf8("vertical_line"))
        self.main_layout.addWidget(self.vertical_line)
        self.controls_layout = QtGui.QVBoxLayout()
        self.controls_layout.setSizeConstraint(QtGui.QLayout.SetDefaultConstraint)
        self.controls_layout.setMargin(8)
        self.controls_layout.setObjectName(_fromUtf8("controls_layout"))
        spacerItem = QtGui.QSpacerItem(128, 16777215, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.controls_layout.addItem(spacerItem)
        self.operation_layout = QtGui.QVBoxLayout()
        self.operation_layout.setObjectName(_fromUtf8("operation_layout"))
        self.operation_label = QtGui.QLabel(self.centralwidget)
        self.operation_label.setMaximumSize(QtCore.QSize(128, 16777215))
        font = QtGui.QFont()
        font.setPointSize(12)
        font.setBold(True)
        font.setWeight(75)
        self.operation_label.setFont(font)
        self.operation_label.setObjectName(_fromUtf8("operation_label"))
        self.operation_layout.addWidget(self.operation_label)
        self.bfs_button = QtGui.QRadioButton(self.centralwidget)
        self.bfs_button.setMaximumSize(QtCore.QSize(128, 16777215))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.bfs_button.setFont(font)
        self.bfs_button.setChecked(True)
        self.bfs_button.setObjectName(_fromUtf8("bfs_button"))
        self.operation_group = QtGui.QButtonGroup(GraphGui)
        self.operation_group.setObjectName(_fromUtf8("operation_group"))
        self.operation_group.addButton(self.bfs_button)
        self.operation_layout.addWidget(self.bfs_button)
        self.dfs_button = QtGui.QRadioButton(self.centralwidget)
        self.dfs_button.setMaximumSize(QtCore.QSize(128, 16777215))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.dfs_button.setFont(font)
        self.dfs_button.setObjectName(_fromUtf8("dfs_button"))
        self.operation_group.addButton(self.dfs_button)
        self.operation_layout.addWidget(self.dfs_button)
        self.prim_button = QtGui.QRadioButton(self.centralwidget)
        self.prim_button.setObjectName(_fromUtf8("prim_button"))
        self.operation_group.addButton(self.prim_button)
        self.operation_layout.addWidget(self.prim_button)
        self.dijkstra_button = QtGui.QRadioButton(self.centralwidget)
        self.dijkstra_button.setObjectName(_fromUtf8("dijkstra_button"))
        self.operation_group.addButton(self.dijkstra_button)
        self.operation_layout.addWidget(self.dijkstra_button)
        self.controls_layout.addLayout(self.operation_layout)
        self.line_top = QtGui.QFrame(self.centralwidget)
        self.line_top.setFrameShape(QtGui.QFrame.HLine)
        self.line_top.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_top.setObjectName(_fromUtf8("line_top"))
        self.controls_layout.addWidget(self.line_top)
        spacerItem1 = QtGui.QSpacerItem(20, 12, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Fixed)
        self.controls_layout.addItem(spacerItem1)
        self.start_layout = QtGui.QHBoxLayout()
        self.start_layout.setObjectName(_fromUtf8("start_layout"))
        self.start_label = QtGui.QLabel(self.centralwidget)
        self.start_label.setMaximumSize(QtCore.QSize(60, 16777215))
        font = QtGui.QFont()
        font.setPointSize(12)
        font.setBold(True)
        font.setWeight(75)
        self.start_label.setFont(font)
        self.start_label.setObjectName(_fromUtf8("start_label"))
        self.start_layout.addWidget(self.start_label)
        spacerItem2 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.start_layout.addItem(spacerItem2)
        self.start = QtGui.QLineEdit(self.centralwidget)
        self.start.setMaximumSize(QtCore.QSize(60, 16777215))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.start.setFont(font)
        self.start.setInputMethodHints(QtCore.Qt.ImhDigitsOnly)
        self.start.setText(_fromUtf8(""))
        self.start.setObjectName(_fromUtf8("start"))
        self.start_layout.addWidget(self.start)
        self.controls_layout.addLayout(self.start_layout)
        self.end_layout = QtGui.QHBoxLayout()
        self.end_layout.setObjectName(_fromUtf8("end_layout"))
        self.end_label = QtGui.QLabel(self.centralwidget)
        self.end_label.setMaximumSize(QtCore.QSize(60, 16777215))
        font = QtGui.QFont()
        font.setPointSize(12)
        font.setBold(True)
        font.setWeight(75)
        self.end_label.setFont(font)
        self.end_label.setObjectName(_fromUtf8("end_label"))
        self.end_layout.addWidget(self.end_label)
        spacerItem3 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.end_layout.addItem(spacerItem3)
        self.end = QtGui.QLineEdit(self.centralwidget)
        self.end.setMaximumSize(QtCore.QSize(60, 16777215))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.end.setFont(font)
        self.end.setInputMethodHints(QtCore.Qt.ImhDigitsOnly)
        self.end.setText(_fromUtf8(""))
        self.end.setObjectName(_fromUtf8("end"))
        self.end_layout.addWidget(self.end)
        self.controls_layout.addLayout(self.end_layout)
        spacerItem4 = QtGui.QSpacerItem(20, 12, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Fixed)
        self.controls_layout.addItem(spacerItem4)
        self.line_middle = QtGui.QFrame(self.centralwidget)
        self.line_middle.setFrameShape(QtGui.QFrame.HLine)
        self.line_middle.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_middle.setObjectName(_fromUtf8("line_middle"))
        self.controls_layout.addWidget(self.line_middle)
        self.vertex_size_label = QtGui.QLabel(self.centralwidget)
        font = QtGui.QFont()
        font.setPointSize(12)
        font.setBold(True)
        font.setWeight(75)
        self.vertex_size_label.setFont(font)
        self.vertex_size_label.setObjectName(_fromUtf8("vertex_size_label"))
        self.controls_layout.addWidget(self.vertex_size_label)
        self.vertex_size_slider = QtGui.QSlider(self.centralwidget)
        self.vertex_size_slider.setMinimum(2)
        self.vertex_size_slider.setMaximum(64)
        self.vertex_size_slider.setSingleStep(2)
        self.vertex_size_slider.setProperty("value", 32)
        self.vertex_size_slider.setOrientation(QtCore.Qt.Horizontal)
        self.vertex_size_slider.setTickPosition(QtGui.QSlider.NoTicks)
        self.vertex_size_slider.setObjectName(_fromUtf8("vertex_size_slider"))
        self.controls_layout.addWidget(self.vertex_size_slider)
        self.show_names_checkbox = QtGui.QCheckBox(self.centralwidget)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.show_names_checkbox.setFont(font)
        self.show_names_checkbox.setChecked(True)
        self.show_names_checkbox.setObjectName(_fromUtf8("show_names_checkbox"))
        self.controls_layout.addWidget(self.show_names_checkbox)
        self.show_weights_checkbox = QtGui.QCheckBox(self.centralwidget)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.show_weights_checkbox.setFont(font)
        self.show_weights_checkbox.setChecked(True)
        self.show_weights_checkbox.setObjectName(_fromUtf8("show_weights_checkbox"))
        self.controls_layout.addWidget(self.show_weights_checkbox)
        self.line_bottom = QtGui.QFrame(self.centralwidget)
        self.line_bottom.setFrameShape(QtGui.QFrame.HLine)
        self.line_bottom.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_bottom.setObjectName(_fromUtf8("line_bottom"))
        self.controls_layout.addWidget(self.line_bottom)
        self.play_layout = QtGui.QHBoxLayout()
        self.play_layout.setObjectName(_fromUtf8("play_layout"))
        self.play_button = QtGui.QPushButton(self.centralwidget)
        self.play_button.setMinimumSize(QtCore.QSize(60, 40))
        self.play_button.setText(_fromUtf8(""))
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(_fromUtf8("./images/play.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.play_button.setIcon(icon)
        self.play_button.setIconSize(QtCore.QSize(36, 36))
        self.play_button.setFlat(True)
        self.play_button.setObjectName(_fromUtf8("play_button"))
        self.play_layout.addWidget(self.play_button)
        self.stop_button = QtGui.QPushButton(self.centralwidget)
        self.stop_button.setMinimumSize(QtCore.QSize(60, 40))
        self.stop_button.setText(_fromUtf8(""))
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap(_fromUtf8("./images/stop.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.stop_button.setIcon(icon1)
        self.stop_button.setIconSize(QtCore.QSize(36, 36))
        self.stop_button.setFlat(True)
        self.stop_button.setObjectName(_fromUtf8("stop_button"))
        self.play_layout.addWidget(self.stop_button)
        self.controls_layout.addLayout(self.play_layout)
        spacerItem5 = QtGui.QSpacerItem(128, 16777215, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.controls_layout.addItem(spacerItem5)
        self.main_layout.addLayout(self.controls_layout)
        self.window_layout.addLayout(self.main_layout)
        GraphGui.setCentralWidget(self.centralwidget)
        self.menubar = QtGui.QMenuBar(GraphGui)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 989, 21))
        self.menubar.setObjectName(_fromUtf8("menubar"))
        self.menu_graph = QtGui.QMenu(self.menubar)
        self.menu_graph.setObjectName(_fromUtf8("menu_graph"))
        self.menu_insert = QtGui.QMenu(self.menubar)
        self.menu_insert.setObjectName(_fromUtf8("menu_insert"))
        self.menuHelp = QtGui.QMenu(self.menubar)
        self.menuHelp.setObjectName(_fromUtf8("menuHelp"))
        GraphGui.setMenuBar(self.menubar)
        self.statusbar = QtGui.QStatusBar(GraphGui)
        self.statusbar.setSizeGripEnabled(True)
        self.statusbar.setObjectName(_fromUtf8("statusbar"))
        GraphGui.setStatusBar(self.statusbar)
        self.action_empty_bt = QtGui.QAction(GraphGui)
        self.action_empty_bt.setObjectName(_fromUtf8("action_empty_bt"))
        self.action_balanced_bt = QtGui.QAction(GraphGui)
        self.action_balanced_bt.setObjectName(_fromUtf8("action_balanced_bt"))
        self.action_ascending_bt = QtGui.QAction(GraphGui)
        self.action_ascending_bt.setObjectName(_fromUtf8("action_ascending_bt"))
        self.action_descending_bt = QtGui.QAction(GraphGui)
        self.action_descending_bt.setObjectName(_fromUtf8("action_descending_bt"))
        self.action_empty_bst = QtGui.QAction(GraphGui)
        self.action_empty_bst.setObjectName(_fromUtf8("action_empty_bst"))
        self.action_balanced_bst = QtGui.QAction(GraphGui)
        self.action_balanced_bst.setObjectName(_fromUtf8("action_balanced_bst"))
        self.action_ascending_bst = QtGui.QAction(GraphGui)
        self.action_ascending_bst.setObjectName(_fromUtf8("action_ascending_bst"))
        self.action_descending_bst = QtGui.QAction(GraphGui)
        self.action_descending_bst.setObjectName(_fromUtf8("action_descending_bst"))
        self.action_empty_avl = QtGui.QAction(GraphGui)
        self.action_empty_avl.setObjectName(_fromUtf8("action_empty_avl"))
        self.action_balanced_avl = QtGui.QAction(GraphGui)
        self.action_balanced_avl.setObjectName(_fromUtf8("action_balanced_avl"))
        self.action_ascending_avl = QtGui.QAction(GraphGui)
        self.action_ascending_avl.setObjectName(_fromUtf8("action_ascending_avl"))
        self.action_descending_avl = QtGui.QAction(GraphGui)
        self.action_descending_avl.setObjectName(_fromUtf8("action_descending_avl"))
        self.actionOpen = QtGui.QAction(GraphGui)
        self.actionOpen.setObjectName(_fromUtf8("actionOpen"))
        self.actionSave = QtGui.QAction(GraphGui)
        self.actionSave.setObjectName(_fromUtf8("actionSave"))
        self.actionSave_As = QtGui.QAction(GraphGui)
        self.actionSave_As.setObjectName(_fromUtf8("actionSave_As"))
        self.action_save = QtGui.QAction(GraphGui)
        self.action_save.setObjectName(_fromUtf8("action_save"))
        self.action_exit = QtGui.QAction(GraphGui)
        self.action_exit.setObjectName(_fromUtf8("action_exit"))
        self.action_insert_vertex = QtGui.QAction(GraphGui)
        self.action_insert_vertex.setObjectName(_fromUtf8("action_insert_vertex"))
        self.action_insert_edge = QtGui.QAction(GraphGui)
        self.action_insert_edge.setObjectName(_fromUtf8("action_insert_edge"))
        self.action_about = QtGui.QAction(GraphGui)
        self.action_about.setObjectName(_fromUtf8("action_about"))
        self.action_binary_tree = QtGui.QAction(GraphGui)
        self.action_binary_tree.setObjectName(_fromUtf8("action_binary_tree"))
        self.action_binary_search_tree = QtGui.QAction(GraphGui)
        self.action_binary_search_tree.setObjectName(_fromUtf8("action_binary_search_tree"))
        self.action_avl_tree = QtGui.QAction(GraphGui)
        self.action_avl_tree.setObjectName(_fromUtf8("action_avl_tree"))
        self.action_balanced_data = QtGui.QAction(GraphGui)
        self.action_balanced_data.setObjectName(_fromUtf8("action_balanced_data"))
        self.action_ascending_data = QtGui.QAction(GraphGui)
        self.action_ascending_data.setObjectName(_fromUtf8("action_ascending_data"))
        self.action_descending_data = QtGui.QAction(GraphGui)
        self.action_descending_data.setObjectName(_fromUtf8("action_descending_data"))
        self.action_random_data = QtGui.QAction(GraphGui)
        self.action_random_data.setObjectName(_fromUtf8("action_random_data"))
        self.action_new = QtGui.QAction(GraphGui)
        self.action_new.setObjectName(_fromUtf8("action_new"))
        self.action_node = QtGui.QAction(GraphGui)
        self.action_node.setObjectName(_fromUtf8("action_node"))
        self.action_edge = QtGui.QAction(GraphGui)
        self.action_edge.setObjectName(_fromUtf8("action_edge"))
        self.action_open = QtGui.QAction(GraphGui)
        self.action_open.setObjectName(_fromUtf8("action_open"))
        self.action_save_image = QtGui.QAction(GraphGui)
        self.action_save_image.setObjectName(_fromUtf8("action_save_image"))
        self.menu_graph.addAction(self.action_new)
        self.menu_graph.addAction(self.action_open)
        self.menu_graph.addAction(self.action_save)
        self.menu_graph.addAction(self.action_save_image)
        self.menu_graph.addSeparator()
        self.menu_graph.addAction(self.action_exit)
        self.menu_insert.addAction(self.action_insert_vertex)
        self.menu_insert.addAction(self.action_insert_edge)
        self.menuHelp.addAction(self.action_about)
        self.menubar.addAction(self.menu_graph.menuAction())
        self.menubar.addAction(self.menu_insert.menuAction())
        self.menubar.addAction(self.menuHelp.menuAction())

        self.retranslateUi(GraphGui)
        QtCore.QMetaObject.connectSlotsByName(GraphGui)

    def retranslateUi(self, GraphGui):
        GraphGui.setWindowTitle(_translate("GraphGui", "Graph Demo", None))
        self.operation_label.setText(_translate("GraphGui", "Operation", None))
        self.bfs_button.setText(_translate("GraphGui", "BFS", None))
        self.dfs_button.setText(_translate("GraphGui", "DFS", None))
        self.prim_button.setText(_translate("GraphGui", "Prim", None))
        self.dijkstra_button.setText(_translate("GraphGui", "Dijkstra", None))
        self.start_label.setText(_translate("GraphGui", "Start:", None))
        self.end_label.setText(_translate("GraphGui", "End:", None))
        self.vertex_size_label.setText(_translate("GraphGui", "Vertex Size", None))
        self.show_names_checkbox.setText(_translate("GraphGui", "Show Vertex Names", None))
        self.show_weights_checkbox.setText(_translate("GraphGui", "Show Edge Weights", None))
        self.play_button.setShortcut(_translate("GraphGui", "Space", None))
        self.stop_button.setShortcut(_translate("GraphGui", "Esc", None))
        self.menu_graph.setTitle(_translate("GraphGui", "Graph", None))
        self.menu_insert.setTitle(_translate("GraphGui", "Insert", None))
        self.menuHelp.setTitle(_translate("GraphGui", "Help", None))
        self.action_empty_bt.setText(_translate("GraphGui", "Empty", None))
        self.action_balanced_bt.setText(_translate("GraphGui", "Balanced Data", None))
        self.action_ascending_bt.setText(_translate("GraphGui", "Ascending Data", None))
        self.action_descending_bt.setText(_translate("GraphGui", "Descending Data", None))
        self.action_empty_bst.setText(_translate("GraphGui", "Empty", None))
        self.action_balanced_bst.setText(_translate("GraphGui", "Balanced Data", None))
        self.action_ascending_bst.setText(_translate("GraphGui", "Ascending Data", None))
        self.action_descending_bst.setText(_translate("GraphGui", "Descending Data", None))
        self.action_empty_avl.setText(_translate("GraphGui", "Empty", None))
        self.action_balanced_avl.setText(_translate("GraphGui", "Balanced Data", None))
        self.action_ascending_avl.setText(_translate("GraphGui", "Ascending Data", None))
        self.action_descending_avl.setText(_translate("GraphGui", "Descending Data", None))
        self.actionOpen.setText(_translate("GraphGui", "Open...", None))
        self.actionSave.setText(_translate("GraphGui", "Save", None))
        self.actionSave_As.setText(_translate("GraphGui", "Save As...", None))
        self.action_save.setText(_translate("GraphGui", "Save...", None))
        self.action_exit.setText(_translate("GraphGui", "Exit", None))
        self.action_insert_vertex.setText(_translate("GraphGui", "Vertex...", None))
        self.action_insert_edge.setText(_translate("GraphGui", "Edge...", None))
        self.action_about.setText(_translate("GraphGui", "About", None))
        self.action_binary_tree.setText(_translate("GraphGui", "Binary Tree", None))
        self.action_binary_tree.setShortcut(_translate("GraphGui", "Ctrl+B", None))
        self.action_binary_search_tree.setText(_translate("GraphGui", "Binary Search Tree", None))
        self.action_binary_search_tree.setShortcut(_translate("GraphGui", "Ctrl+T", None))
        self.action_avl_tree.setText(_translate("GraphGui", "AVL Tree", None))
        self.action_avl_tree.setShortcut(_translate("GraphGui", "Ctrl+A", None))
        self.action_balanced_data.setText(_translate("GraphGui", "Balanced Data", None))
        self.action_balanced_data.setShortcut(_translate("GraphGui", "Ctrl+Shift+B", None))
        self.action_ascending_data.setText(_translate("GraphGui", "Ascending Data", None))
        self.action_ascending_data.setShortcut(_translate("GraphGui", "Ctrl+Shift+A", None))
        self.action_descending_data.setText(_translate("GraphGui", "Descending Data", None))
        self.action_descending_data.setShortcut(_translate("GraphGui", "Ctrl+Shift+D", None))
        self.action_random_data.setText(_translate("GraphGui", "Random Data", None))
        self.action_random_data.setShortcut(_translate("GraphGui", "Ctrl+Shift+R", None))
        self.action_new.setText(_translate("GraphGui", "New", None))
        self.action_node.setText(_translate("GraphGui", "Node...", None))
        self.action_edge.setText(_translate("GraphGui", "Edge...", None))
        self.action_open.setText(_translate("GraphGui", "Open...", None))
        self.action_save_image.setText(_translate("GraphGui", "Save Image...", None))

