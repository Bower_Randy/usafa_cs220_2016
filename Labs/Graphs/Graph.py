"""CS 220, Data Abstraction, Spring 2016, Dr. Bower.

This module contains a graph implementation using Python dictionaries.

Documentation: None required for non-graded lab exercises.
"""

# The queue module and the PriorityQueue class are intended for multi-producer,
# multi-consumer queues; i.e., useful for threaded programming. This is of no
# benefit here, but care must be taken to not call get() on an empty priority
# queue as it will block. See https://docs.python.org/3/library/queue.html.
from queue import PriorityQueue
# The threading module is needed in case a graph is being animated by a GUI.
# All references to threading and events can be ignored and the graph works fine.
import threading
# Use itertools.combinations to find paths between all vertex pairs.
from itertools import combinations


# TODO - Lab 29: Read, discuss, and understand the following code.
def main():
    """A test of each algorithm on the sample graph."""
    graph = Graph( "./data/Graph1.txt" )
    print( "Graph:", graph, sep="\n" )
    print( "\nBreadth-first traversal:", graph.traverse( "E" ) )
    print( "\nDepth-first traversal:  ", graph.traverse( "E", -1 ) )
    print( "\nPrim's Minimal Spanning Tree:", graph.prim( "E" ) )
    print( "\nDijkstra's Shortest Path E-F:", graph.dijkstra( "E", "F" ) )
    print()

    # Show the shortest path between all pairs of vertices.
    for v in combinations( sorted( graph.vertices.keys() ), 2 ):
        print( v, "...", graph.dijkstra( v[ 0 ], v[ 1 ] ) )

    # A test of Dijkstra's shortest path algorithm when there is no path.
    print( "\nRemoving edge B-I..." )
    graph.del_edge( "B", "I" )
    print( "\nPrim's Minimal Spanning Tree:", graph.prim( "E" ) )
    print( "\nDijkstra's Shortest Path A-I:", graph.dijkstra( "A", "I" ) )


# TODO - Lab 29: Read, discuss, and understand the following code.
class Vertex( object ):
    """Represents a single vertex in a graph.

    This vertex/graph implementation will use the adjacency list approach with
    each vertex keeping a dictionary of neighboring vertices (i.e., edges).
    A Python dictionary is used to implement a weighted graph where the key
    is the adjacent vertex name and the value is the weight of the edge.
    """

    def __init__( self, name, x=0.0, y=0.0 ):
        """Creates a new vertex with the given label.

        Optional arguments are an (x,y) coordinate to be used to draw a vertex/graph.
        Note: The values of the (x,y) coordinates are in the range [0.0-1.0] and will
        be used to scale the graph to a bounding rectangle when drawn.

        :param str name: The name of the vertex; will label the vertex when drawing the graph.
        :param float x: The x-coordinate of the vertex; only used when drawing the graph.
        :param float y: The y-coordinate of the vertex; only used when drawing the graph.
        """
        # The comments after each attribute are written to utilize Pycharm's type-hinting:
        # https://www.jetbrains.com/pycharm/help/type-hinting-in-pycharm.html
        self.name = name
        """:type: str"""
        self.x = x
        """:type: float"""
        self.y = y
        """:type: float"""
        self.neighbors = {}
        """:type: dict of (str, float)"""

    def __str__( self ):
        """Returns a string representation of the graph."""
        location = ""
        # Only include detailed location information if x and y are non-default values.
        if self.x != 0.0 or self.y != 0.0:
            location = " is at ({:.2f},{:.2f})".format( self.x, self.y )
        return "{}{}: {}".format( self.name, location, self.neighbors )

    def add_neighbor( self, neighbor, weight=1.0 ):
        """Adds a neighbor to this vertex with the given edge weight.

        :param str neighbor: The name of the neighboring vertex.
        :param float weight: The weight of the edge between the vertices.
        """
        self.neighbors[ neighbor ] = weight

    def get_neighbors( self ):
        """Return a list of vertex names that are adjacent to the vertex.

        :return: A list of vertex names that are adjacent to the vertex.
        :rtype: list of [str]
        """
        return sorted( list( self.neighbors.keys() ) )

    def get_weight( self, neighbor ):
        """Return the weight of the edge between this vertex and the given neighbor.

        :param str neighbor: The name of the adjacent vertex.
        :return: The weight between the vertex and the given neighbor; None if the vertices are not adjacent.
        :rtype: float
        """
        # The second value to the get() method is a default value if the key is not found.
        # https://docs.python.org/3.3/library/stdtypes.html#dict.get
        return self.neighbors.get( neighbor, None )


# TODO - Lab 29: Read, discuss, and understand the following code.
class Graph( object ):
    """Implements a graph using the Vertex class.

    The graph is stored as a dictionary mapping vertex names to Vertex objects
    and each Vertex object keeps track of adjacent vertices (i.e., edges).
    """

    def __init__( self, file_name=None ):
        """Creates an empty graph.

        :param str file_name: Name of a file containing a graph.
        """
        # Empty dictionary of vertices in the graph.
        self.vertices = {}
        """:type : dict of (str, Vertex)"""

        # Create an event that another thread could use to pause and show the heap.
        self.permission_to_continue = threading.Event()
        # The event is initially set, so another thread must clear it to pause.
        # NOTE: If another thread clears the event, it must also set it or the heap may freeze!
        self.permission_to_continue.set()
        # When paused, a particular index/node can be highlighted.
        self.green_vertex = None
        self.red_vertex = None
        self.yellow_vertices = []

        # If the file_name parameter is given, load the graph from the file.
        if file_name is not None:
            self.load_graph( file_name )

    def load_graph( self, file_name ):
        """Loads a graph from the given data file.

        Graph data file format is as follows:
        A 0.25 0.25
        B 0.25 0.75
        C 0.75 0.75
        A B 3.0
        B C 4.0
        A C 5.0

        The above data would draw a right triangle with edge weights forming the
        classic Pythagorean triangle. Note the actual placement of the vertices
        is determined by the size of the area in which the graph is drawn, so
        the edge weight and edge length may not be to scale.

        :param str file_name: The name of the file containing graph information.
        """
        with open( file_name, "r" ) as data_file:
            for line in data_file:
                # Split the line of data into individual pieces.
                data = line.split()
                # The second item on every line determines if the line is a vertex or an edge.
                if data[ 1 ] in self.vertices:
                    # The second item on the input line was a vertex name, so try to add the edge.
                    try:
                        self.add_edge( data[ 0 ], data[ 1 ], float( data[ 2 ] ) )
                    except ValueError:
                        pass  # Just ignore this line if something isn't correct.
                else:
                    # The second item on the line wasn't a vertex name, so try to add a new vertex.
                    try:
                        self.add_vertex( data[ 0 ], float( data[ 1 ] ), float( data[ 2 ] ) )
                    except ValueError:
                        pass  # Just ignore this line if something isn't correct.

    def _pause( self, green=None, red=None, yellow=None ):
        """Pause so another thread could show the graph."""
        # When paused, several nodes can be highlighted; green/start, red/end, and yellow/queued.
        # Save these as a class attributes so a GUI has access.
        self.green_vertex = green
        self.red_vertex = red
        self.yellow_vertices = [] if yellow is None else yellow
        # If the event is never cleared by another thread, the heap cruises along, never waiting.
        # NOTE: If another thread clears the event, it must also set it or the heap may freeze!
        self.permission_to_continue.wait()

    def __str__( self ):
        """Returns a string representation of the graph."""
        return "\n".join( str( self.vertices[ vertex ] ) for vertex in sorted( self.vertices.keys() ) )

    def __contains__( self, vertex_name ):
        """Returns True if the given vertex name is in the graph; False otherwise.

        Implementing this method allows use of the 'in' operator.

        :param str vertex_name: The name of the vertex to search for in the graph.
        :return: True if the given vertex name is in the graph; False otherwise.
        :rtype: bool
        """
        return vertex_name in self.vertices.keys()

    def __iter__( self ):
        """Returns an iterator over the Vertex objects in the graph."""
        return iter( self.vertices.values() )

    def add_vertex( self, name, x=0.0, y=0.0 ):
        """Adds a vertex with the given name (aka, dictionary key) to the graph.

        Note: The (x,y) coordinates are in the range [0.0-1.0] and will
        be used to scale the graph to a bounding rectangle when drawn.

        :param str name: The name (aka, dictionary key) of the vertex.
        :param float x: The x-coordinate of the vertex; used when drawing the graph.
        :param float y: The y-coordinate of the vertex; used when drawing the graph.
        """
        # Vertices are stored in a dictionary with the vertex name serving as the key
        # and a Vertex object stored as the value. No error checking is done, so adding
        # a vertex with a duplicate name replaces the old vertex.
        self.vertices[ name ] = Vertex( name, x, y )

    def add_edge( self, source, target, weight=1.0, directed=False ):
        """Adds an edge between the given source and target vertices to the graph; both
        the source and target vertices must already exist in the vertices dictionary.

        :param str source: The name of the source vertex for the edge.
        :param str target: The name of the target vertex for the edge.
        :param float weight: The weight of the edge.
        :param bool directed: Indicates if the edge is directed.
        """
        # Edges are stored in a dictionary within each Vertex object.
        # Both vertices must be in the graph in order to add the edge.
        if source in self.vertices and target in self.vertices:
            # Add the edge from source to target.
            self.vertices[ source ].add_neighbor( target, weight )
            # If the graph is not directed, also add the edge from target to source.
            if not directed:
                self.vertices[ target ].add_neighbor( source, weight )

    def del_vertex( self, name ):
        """Deletes a vertex with the given name (aka, dictionary key) from the graph.

        :param str name: The name (aka, dictionary key) of the vertex.
        """
        # Default value of None given to pop() method calls below to avoid raising a KeyError.
        # Loop through all vertices in the graph and remove edges to/from the vertex being deleted.
        for vertex in self:
            vertex.neighbors.pop( name, None )
        # Now remove the vertex from the graph.
        self.vertices.pop( name, None )

    def del_edge( self, source, target, directed=False ):
        """Deletes the edge between the given source and target vertices.

        :param str source: The name of the source vertex for the edge.
        :param str target: The name of the target vertex for the edge.
        :param bool directed: Indicates if the edge is directed.
        """
        # Both vertices must be in the graph in order to delete the edge.
        if source in self.vertices and target in self.vertices:
            # Default value of None given to pop() method to avoid raising a KeyError.
            self.vertices[ source ].neighbors.pop( target, None )
            # If the graph is not directed, also delete the edge from target to source.
            if not directed:
                self.vertices[ target ].neighbors.pop( source, None )

    def breadth_first_traversal( self, start_vertex_name ):
        """Performs a breadth-first traversal of the graph, starting at the indicated vertex.

        :param str start_vertex_name: The name of the starting vertex for the traversal.
        :return: A list of vertices in the order they are visited on the traversal.
        :rtype: list of str
        """
        # TODO - Lab 30: Implement the method as described in the lab document.
        return self.random_traversal( start_vertex_name )  # Remove this line when writing your own code.

    def depth_first_traversal( self, start_vertex_name ):
        """Performs a depth-first traversal of the graph, starting at the indicated vertex.

        :param str start_vertex_name: The name of the starting vertex for the traversal.
        :return: A list of vertices in the order they are visited on the traversal.
        :rtype: list of str
        """
        # TODO - Lab 30: Implement the method as described in the lab document.
        return self.random_traversal( start_vertex_name )  # Remove this line when writing your own code.

    def random_traversal( self, start_vertex_name ):
        """Randomly traverses the vertices in the graph, just for something to do.

        NOTE: When distributed to students, this will be given in place of the traverse() method.

        :param str start_vertex_name: The name of the starting vertex for the traversal.
        :return: A list of vertices in the order they are visited on the traversal.
        :rtype: list of str
        """
        # Make sure the start vertex is valid.
        if start_vertex_name not in self.vertices:
            # Arbitrarily pick the vertex that is first alphabetically.
            start_vertex_name = sorted( list( self.vertices.keys() ) )[ 0 ]

        # Make a list of the vertex names so each can be visited, and remove the start vertex.
        # Note the key values in a dictionary are arbitrarily ordered, which is good for this.
        vertex_names = list( self.vertices.keys() )
        vertex_names.remove( start_vertex_name )
        # Arbitrarily pick the next vertex as the end vertex.
        end_vertex_name = vertex_names.pop()
        # Keep track of the visited vertices.
        visited_vertices = []

        # Pausing with a graph allows highlighting a start vertex (green),
        # an end vertex (red), and a list of visited vertices (yellow).
        self._pause( start_vertex_name, end_vertex_name, visited_vertices )

        # Continue until all vertex names have been removed and visited.
        while vertex_names:
            # Remove one vertex name and put it in the visited list.
            visited_vertices.append( vertex_names.pop() )
            # Pause and show the graph with all the fancy colors.
            self._pause( start_vertex_name, end_vertex_name, visited_vertices )

        # return the list of vertices in the order they were visited.
        return [ start_vertex_name ] + visited_vertices + [ end_vertex_name ]

    def dijkstra( self, start_vertex_name, end_vertex_name ):
        """Implements Dijkstra's algorithm to find the shortest path between the given vertices.

        :param str start_vertex_name: The name of the starting vertex.
        :param str end_vertex_name: The name of the ending vertex.
        :return: A list vertex names in the shortest path.
        :rtype: list of str
        """
        # TODO - Lab 33: Implement the method as described in the lab document.
        return None  # Remove this line (and this comment) when writing your own code.

    def prim( self, start_vertex_name ):
        """Implements Prim's algorithm to find the minimal spanning tree from the given start vertex.

        :param str start_vertex_name: The name of the starting vertex.
        :return: A list of edges in the minimal spanning tree.
        :rtype: list of (float, str, str)
        """
        # TODO - Lab 34: Implement the method as described in the lab document.
        return None  # Remove this line (and this comment) when writing your own code.


# Call main() if this file is being executed rather than imported.
if __name__ == "__main__":
    main()  # https://docs.python.org/3/library/__main__.html
