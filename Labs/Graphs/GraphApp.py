"""CS 220, Data Abstraction, Spring 2016, Dr. Bower.

This file contains a GUI application to visualize the Graph class.

Documentation: None.
"""

from os.path import isfile
from threading import Thread, Event
from time import sleep
from Labs.Graphs.Graph import Graph
from Labs.Graphs.GraphGui import Ui_GraphGui
from PyQt4 import QtCore, QtGui
import sys


def main():
    """Launch a GUI created with Qt Designer."""
    # Create a QApplication to handle event processing.
    qt_app = QtGui.QApplication( sys.argv )

    # Create an instance of the app and show the main window.
    my_app = GraphApp()
    my_app.main_window.show()

    # Execute the QApplication, exiting when it returns (i.e., the window is closed).
    sys.exit( qt_app.exec_() )  # Note the underscore at the end of exec_().


class GraphApp:
    """Application class to instantiate and control a Graph Demo GUI."""

    def __init__( self ):
        """Initialize and show the gui."""
        # Create the animation thread and the pause/stop events for it (name the thread for debugging).
        self.animation_thread = Thread( target=self.run, name="Animation" )
        self.stop_animation = Event()
        self.run_animation = Event()
        self.run_animation.set()  # Initially set so the animation runs; cleared to pause the animation.

        # The tree thread will be re-created when the Play/Pause button is clicked,
        # but create an empty thread here so PyCharm will know what it is.
        self.graph_thread = Thread()

        # Create the main window in which the gui will display, create
        # an instance of the gui, and set it up in the main window.
        self.main_window = QtGui.QMainWindow()
        self.gui = Ui_GraphGui()
        self.gui.setupUi( self.main_window )
        # Connect the menu buttons to methods.
        self.gui.action_new.triggered.connect( self.new_graph )
        self.gui.action_open.triggered.connect( self.open_graph )
        self.gui.action_save.triggered.connect( self.save_graph )
        self.gui.action_save_image.triggered.connect( self.save_image )
        self.gui.action_exit.triggered.connect( self.exit )
        self.gui.action_insert_vertex.triggered.connect( self.insert_vertex )
        self.gui.action_insert_edge.triggered.connect( self.insert_edge )
        self.gui.action_about.triggered.connect( self.about )
        # Connect to the various editors and controls.
        self.gui.dijkstra_button.toggled.connect( self.dijkstra_button )
        self.gui.start.textEdited.connect( self.set_start )
        self.gui.end.textEdited.connect( self.set_end )
        self.gui.vertex_size_slider.sliderMoved.connect( self.set_vertex_diameter )
        self.gui.show_names_checkbox.toggled.connect( self.gui.drawing_widget.update )
        self.gui.show_weights_checkbox.toggled.connect( self.gui.drawing_widget.update )
        # Connect to the play and stop buttons.
        self.gui.play_button.clicked.connect( self.play )
        self.gui.stop_button.clicked.connect( self.stop )
        # Catch mouse events to be able to click and drag nodes.
        self.gui.drawing_widget.mouseMoveEvent = self.mouse_move
        self.gui.drawing_widget.mousePressEvent = self.mouse_press
        self.gui.drawing_widget.mouseReleaseEvent = self.mouse_release
        self.gui.drawing_widget.keyPressEvent = self.key_press
        # Catch the paint event so the data can be drawn.
        self.gui.drawing_widget.paintEvent = self.paint_event
        # Catch the window close event so threads can be stopped.
        self.main_window.closeEvent = self.close_event

        # Create a graph to show when the GUI launches ... nobody likes a blank screen.
        self.graph = Graph()
        self.load_graph( "./data/Graph1.txt" )
        # Get the value of the slider to determine vertex size.
        self.vertex_diameter = self.gui.vertex_size_slider.value()
        # Disable the end vertex editor; it is enabled when Dijkstra is selected.
        self.gui.end.setEnabled( False )
        # Put the keyboard focus in the start vertex editor.
        self.gui.start.setFocus()

        # These variables will be used when modifying the graph.
        self.vertex_name = None
        self.edge_weight = None
        self.source_vertex = None
        self.target_vertex = None
        self.selected_edge = None
        self.selected_vertex = None
        self.moving_vertex = None

    def new_graph( self ):
        """Creates a new, empty graph."""
        self.graph = Graph()
        self.gui.drawing_widget.update()

    def open_graph( self ):
        """Prompts the user for a file name and then loads the graph data from the file."""
        file_name = QtGui.QFileDialog.getOpenFileName( self.main_window, filter="*.txt" )
        self.load_graph( file_name )

    def load_graph( self, file_name ):
        """Loads a graph from a text file.

        :param str file_name: The name of the graph data file.
        """
        # Make sure the file name is valid, then load the graph.
        if isfile( file_name ):
            # Create a new graph object from the given file name.
            self.graph = Graph( file_name )
            # Arbitrarily choose the alphabetically first vertex as the start vertex.
            self.graph.green_vertex = sorted( list( self.graph.vertices.keys() ) )[ 0 ]
            self.gui.start.setText( self.graph.green_vertex )
            # Put the name of the last vertex in the editor, but don't highlight it.
            # The end vertex will be drawn in red when the user selects Dijkstra's algorithm.
            self.gui.end.setText( sorted( list( self.graph.vertices.keys() ) )[ -1 ] )
            self.gui.drawing_widget.update()

    def save_graph( self ):
        """Saves the current graph as a text file."""
        file_name = QtGui.QFileDialog.getSaveFileName( self.main_window, filter="*.txt" )
        if file_name != "":  # file_name == "" when user clicks Cancel on file save dialog.
            with open( file_name, "w" ) as data_file:
                # Write each vertex and its coordinates to the text file.
                for vertex in self.graph:
                    print( vertex.name, vertex.x, vertex.y, file=data_file )

                # Write each edge and its weight to the file. Since this application creates
                # un-directed graphs, only write the edge once by comparing vertex names.
                for vertex in self.graph:
                    # Note that get_neighbors() returns a list of vertex names, not Vertex objects.
                    for neighbor in vertex.get_neighbors():
                        if vertex.name < neighbor:
                            print( vertex.name, neighbor, vertex.get_weight( neighbor ), file=data_file )

    def save_image( self ):
        """Saves the current graph as an image file."""
        pix_map = QtGui.QPixmap.grabWidget( self.gui.drawing_widget )
        file_name = QtGui.QFileDialog.getSaveFileName( self.main_window, filter="*.jpg" )
        if file_name != "":  # file_name == "" when user clicks Cancel on file save dialog.
            pix_map.save( file_name )

    def insert_vertex( self ):
        """Puts the GUI in a state to insert a new vertex into the graph."""
        # It's most likely vertices are labelled with single upper-case letters,
        # so use the first unused such letter as the default value to getText().
        alphabet = list( "ABCDEFGHIJKLMNOPQRSTUVWXYZ" )
        for vertex_name in self.graph.vertices.keys():
            alphabet.remove( vertex_name )
        # Get input data from the user for the new vertex's name.
        data = QtGui.QInputDialog.getText( self.main_window, "Input", "Enter node name:", text=alphabet[ 0 ] )
        # getText returns a tuple with the data and True/False to indicate OK vs. Cancel.
        if data[ 1 ]:
            # True in data[ 1 ] indicates the user pressed Enter or clicked OK, so put GUI in state to add the vertex.
            self.vertex_name = data[ 0 ]
            self.main_window.setCursor( QtCore.Qt.CrossCursor )
            self.gui.statusbar.showMessage( "Click to insert vertex {} . . .".format( self.vertex_name ) )

    def insert_edge( self ):
        """Puts the GUI in a state to insert a new edge into the graph."""
        # Get input data from the user for the new vertex's name.
        data = QtGui.QInputDialog.getDouble( self.main_window, "Input", "Enter edge weight:", 1.0 )
        # getText returns a tuple with the data and True/False to indicate OK vs. Cancel.
        if data[ 1 ]:
            # True in data[ 1 ] indicates the user pressed Enter or clicked OK, so put GUI in state to add the edge.
            self.edge_weight = data[ 0 ]
            self.main_window.setCursor( QtCore.Qt.CrossCursor )
            self.graph.green_vertex = None
            self.graph.red_vertex = None
            self.selected_edge = None
            self.gui.drawing_widget.update()
            self.gui.statusbar.showMessage( "Click source vertex . . ." )

    def mouse_press( self, event ):
        """Called automatically when the user presses the mouse button on the drawing widget.

        :param PyQt.QtGui.QMouseEvent event: The event object from PyQt.
        """
        if self.vertex_name is not None:
            # Insert a new node at the selected location.
            self.graph.add_vertex( self.vertex_name,
                                   event.x() / self.gui.drawing_widget.width(),
                                   event.y() / self.gui.drawing_widget.height() )
            self.graph.green_vertex = self.vertex_name
            self.graph.red_vertex = None
            self.vertex_name = None
            self.main_window.setCursor( QtCore.Qt.ArrowCursor )
            self.gui.statusbar.clearMessage()
        elif self.edge_weight is not None:
            # User is inserting an edge.
            if self.source_vertex is None:
                # Get the vertex under the click.
                self.source_vertex = self.get_vertex( event.x(), event.y() )
                if self.source_vertex is None:
                    # User did not click on a vertex, so quit trying to add an edge.
                    self.edge_weight = None
                    self.main_window.setCursor( QtCore.Qt.ArrowCursor )
                    self.gui.statusbar.clearMessage()
                else:
                    # User clicked a vertex, so highlight it.
                    self.graph.green_vertex = self.source_vertex.name
                    self.gui.statusbar.showMessage( "Click target vertex . . ." )
            elif self.target_vertex is None:
                # Get the vertex under the click.
                self.target_vertex = self.get_vertex( event.x(), event.y() )
                if self.target_vertex is None:
                    # User did not click on a vertex, so quit trying to add an edge.
                    self.edge_weight = None
                    self.source_vertex = None
                else:
                    # User clicked a vertex, so add the edge and highlight it.
                    self.graph.add_edge( self.source_vertex.name, self.target_vertex.name, self.edge_weight )
                    self.selected_edge = ( self.source_vertex, self.target_vertex )
                    # Reset these to None so the GUI is ready for the next click.
                    self.edge_weight = None
                    self.source_vertex = None
                    self.target_vertex = None
                self.gui.statusbar.clearMessage()
                self.main_window.setCursor( QtCore.Qt.ArrowCursor )
        else:
            # User clicked somewhere on the drawing widget,
            # but is not in the process of adding anything.
            self.selected_vertex = self.get_vertex( event.x(), event.y() )
            if self.selected_vertex is not None:
                # Click was on a vertex, so set it as the start vertex.
                self.graph.green_vertex = self.selected_vertex.name
                self.gui.start.setText( self.graph.green_vertex )
                # Also allow the user to move/drag the vertex.
                self.moving_vertex = self.selected_vertex
                # If an edge was selected, clear it so only one thing is selected at a time.
                self.selected_edge = None
            else:
                # Click wasn't on a vertex, so clear the start vertex.
                self.graph.green_vertex = None
                self.gui.start.clear()
                # Now see if the click was on an edge. Note: If an edge
                # overlaps a vertex, the edge will NOT get the click.
                self.selected_edge = self.get_edge( event.x(), event.y() )

        # Whatever happened, update the drawing widget to show it.
        self.gui.drawing_widget.update()

    def mouse_move( self, event ):
        """Called automatically when the user moves the mouse button on the drawing widget
        while pressing the mouse button.

        :param PyQt.QtGui.QMouseEvent event: The event object from PyQt.
        """
        if self.moving_vertex is not None:
            self.moving_vertex.x = event.x() / self.gui.drawing_widget.width()
            self.moving_vertex.y = event.y() / self.gui.drawing_widget.height()
            self.gui.drawing_widget.update()

    def mouse_release( self, event ):
        """Called automatically when the user releases the mouse button on the drawing widget.

        :param PyQt.QtGui.QMouseEvent event: The event object from PyQt.
        """
        # In case the user was moving/dragging a vertex, stop moving/dragging it.
        self.moving_vertex = None

    def key_press( self, event ):
        """Called automatically when the user presses a key with focus on the drawing widget.

        :param PyQt.QtGui.QKeyEvent event: The event object from PyQt.
        """
        if event.key() == QtCore.Qt.Key_Delete:
            if self.selected_vertex is not None:
                reply = QtGui.QMessageBox.question( self.main_window, "Question",
                                                    "Delete vertex {}?".format( self.selected_vertex.name ),
                                                    QtGui.QMessageBox.Yes, QtGui.QMessageBox.No )
                if reply == QtGui.QMessageBox.Yes:
                    self.graph.del_vertex( self.selected_vertex.name )
            elif self.selected_edge is not None:
                reply = QtGui.QMessageBox.question( self.main_window, "Question",
                                                    "Delete edge {}-{}?".format( self.selected_edge[ 0 ].name,
                                                                                 self.selected_edge[ 1 ].name),
                                                    QtGui.QMessageBox.Yes, QtGui.QMessageBox.No )
                if reply == QtGui.QMessageBox.Yes:
                    self.graph.del_edge( self.selected_edge[ 0 ].name, self.selected_edge[ 1 ].name )
            self.gui.drawing_widget.update()

    def get_vertex( self, x, y ):
        """Gets the vertex at the given (x,y) coordinate.

        :param int x: The x-coordinate on the drawing widget.
        :param int y: The y-coordinate on the drawing widget.
        :return: The vertex at the given (x,y) coordinate.
        :rtype: Vertex
        """
        # Check all vertices in the graph to see if one as at location (x,y).
        for vertex in self.graph:
            # Get the vertex's coordinate.
            vx = vertex.x * self.gui.drawing_widget.width()
            vy = vertex.y * self.gui.drawing_widget.height()
            # Compare the distance to the vertex diameter.
            if ( ( x - vx ) ** 2 + ( y - vy ) ** 2 ) ** 0.5 < self.vertex_diameter:
                return vertex
        # Click was not on a vertex.
        return None

    def get_edge( self, x, y ):
        """Gets the edge at the given (x,y) coordinate, specified as a tuple of source and target vertices.

        :param int x: The x-coordinate on the drawing widget.
        :param int y: The y-coordinate on the drawing widget.
        :return: The edge at the given (x,y) coordinate.
        :rtype: tuple of (Vertex, Vertex)
        """
        # Check all vertices in the graph to see if an edge from it is at location (x,y).
        for vertex in self.graph:
            # Get the vertex's coordinate.
            x1 = vertex.x * self.gui.drawing_widget.width()
            y1 = vertex.y * self.gui.drawing_widget.height()
            # Go through all the neighbors (by name, not Vertex object) of the vertex to check edges.
            for neighbor in vertex.get_neighbors():
                # Get the coordinate of the neighbor vertex.
                x2 = self.graph.vertices[ neighbor ].x * self.gui.drawing_widget.width()
                y2 = self.graph.vertices[ neighbor ].y * self.gui.drawing_widget.height()
                # Calculate the coordinate of the edge as the midpoint between the vertices.
                ex = ( x1 + x2 ) / 2
                ey = ( y1 + y2 ) / 2
                # See if the click was on the midpoint of this edge.
                if ( ( x - ex ) ** 2 + ( y - ey ) ** 2 ) ** 0.5 < self.vertex_diameter:
                    # Return a tuple of the source and target vertices, in alphabetical order
                    # by name just so it looks pretty when prompting the user to confirm delete.
                    if vertex.name < neighbor:
                        return vertex, self.graph.vertices[ neighbor ]
                    else:
                        return self.graph.vertices[ neighbor ], vertex

        # Click was not on the midpoint of an edge.
        return None

    def exit( self ):
        """Exits the application."""
        # Calling close() on the main window automatically generates the
        # closeEvent, which is handled by the close_event() method below.
        self.main_window.close()

    def close_event( self, event ):
        """Called automatically when the user closes the application window.

        :param PyQt.QtGui.QCloseEvent event: The event object from PyQt.
        """
        # Stop the animation and accept the event so the window will shut down.
        self.stop()
        event.accept()

    def about( self ):
        """This method does what its name implies it does."""
        about = "CS 220, Spring 2015, Graph Demo" + \
                "\n\nAuthor: Dr. Bower" + \
                "\n\nDocumentation: None."
        QtGui.QMessageBox.about( self.main_window, "About", about )

    def dijkstra_button( self ):
        """Called automatically when the Dijkstra button is toggled."""
        self.gui.end.setEnabled( self.gui.dijkstra_button.isChecked() )
        if self.gui.dijkstra_button.isChecked():
            self.set_end( self.gui.end.text() )
        else:
            self.graph.red_vertex = None
            self.gui.drawing_widget.update()

    def set_vertex_diameter( self, diameter ):
        """Called automatically when the vertex size slider is moved.

        :param int diameter: The value of the slider.
        """
        self.vertex_diameter = diameter
        self.gui.drawing_widget.update()

    def set_start( self, text ):
        """Called automatically when the user edits the start vertex.

        :param str text: The text currently in the text editor.
        """
        self.gui.start.setText( text.upper() )
        self.graph.green_vertex = text.upper()
        self.gui.drawing_widget.update()

    def set_end( self, text ):
        """Called automatically when the user edits the end vertex.

        :param str text: The text currently in the text editor.
        """
        self.gui.end.setText( text.upper() )
        self.graph.red_vertex = text.upper()
        self.gui.drawing_widget.update()

    def play( self ):
        """Called when the user clicks the Play/Pause button."""
        if self.animation_thread.is_alive():
            # If the thread is already running, then play/pause the animation.
            if self.run_animation.is_set():
                self.run_animation.clear()
            else:
                self.run_animation.set()
        else:
            # Get the start end end vertices.
            start_vertex = self.gui.start.text()
            end_vertex = self.gui.end.text()

            # Create the thread, but don't start it yet.
            if self.gui.bfs_button.isChecked():
                self.graph_thread = Thread( target=lambda: self.graph.breadth_first_traversal( start_vertex ) )
                self.gui.statusbar.showMessage( "Breadth-first traversal: {}".format(
                    self.graph.breadth_first_traversal( start_vertex ) ) )
            elif self.gui.dfs_button.isChecked():
                self.graph_thread = Thread( target=lambda: self.graph.depth_first_traversal( start_vertex ) )
                self.gui.statusbar.showMessage( "Depth-first traversal: {} . . .".format(
                    self.graph.depth_first_traversal( start_vertex ) ) )
            elif self.gui.prim_button.isChecked():
                self.graph_thread = Thread( target=lambda: self.graph.prim( start_vertex ) )
                self.gui.statusbar.showMessage( "Prim's Minimal Spanning Tree: {}".format(
                    self.graph.prim( start_vertex ) ) )
            else:
                self.graph_thread = Thread( target=lambda: self.graph.dijkstra( start_vertex, end_vertex ) )
                self.gui.statusbar.showMessage( "Dijkstra's shortest path: {}".format(
                    self.graph.dijkstra( start_vertex, end_vertex ) ) )

            # Clear the graph's event to be sure the animation thread has a chance to draw the initial data.
            self.graph.permission_to_continue.clear()
            # Clear the animation's stop event so it will be able to run.
            self.stop_animation.clear()
            # Set the animation's run event so it will be able to run.
            self.run_animation.set()
            # Start the graph thread first because the animation thread only runs when the graph thread is alive.
            self.graph_thread.start()
            # Finally, start the animation thread.
            self.animation_thread.start()
            # Set the mouse cursor to show the animation is in progress.
            self.main_window.setCursor( QtCore.Qt.BusyCursor )

    def stop( self ):
        """Called when the user clicks the Stop button."""
        # Set the stop animation event so the animation loop will terminate.
        self.stop_animation.set()
        # Set the run animation event in case it was paused so the loop will terminate due to the stop event.
        self.run_animation.set()
        # Set the graph's continue event to make sure the graph thread finishes.
        self.graph.permission_to_continue.set()
        # Clear the yellow, but leave the green (start) and red (end) vertices highlighted.
        self.graph.yellow_vertices = []
        self.gui.drawing_widget.update()
        self.gui.statusbar.showMessage( "Ready . . ." )

    def run( self ):
        """This function is to be launched in a separate thread to run the animation."""
        # The stop event will be set by the stop button or when the user closes the window.
        while not self.stop_animation.is_set() and self.graph_thread.is_alive():
            # Clear the graph's continue event so it will wait while the graph is displayed.
            self.graph.permission_to_continue.clear()
            # Update the drawing widget to show this frame of the animation.
            self.gui.drawing_widget.update()
            # This isn't a continuous animation, so sleep long enough to see what's happening.
            sleep( 0.67 )
            # Wait if the run event has been cleared (i.e., the animation has been paused).
            self.run_animation.wait()
            # Set the graph's continue event so it will continue.
            self.graph.permission_to_continue.set()

            # NOTE: The sleep in the above loop is *between* the clear() and set() of the
            # graph's continue event. Thus, once the graph demo pauses and the animation
            # starts drawing, the animation will draw an un-changing snapshot of the graph.
            # Further, since the set at the end of one iteration of the animation loop is
            # immediately followed by the clear the beginning of the next iteration of the
            # animation loop, the graph should be drawn most every time a pause is called.

        # Wait for the graph thread to finish (necessary if the stop button was pressed).
        # while self.graph_thread.is_alive():
        #     sleep( 0.1 )
        if self.graph_thread.is_alive():
            self.graph_thread.join()

        # Do one last update to make sure everything is pretty and reset the mouse pointer.
        self.gui.drawing_widget.update()
        self.main_window.setCursor( QtCore.Qt.ArrowCursor )

        # Create a new animation thread so it's ready to start nex time the Play button is clicked.
        self.animation_thread = Thread( target=self.run, name="Animation" )

    def paint_event( self, q_paint_event ):
        """Called automatically whenever the drawing widget needs to repaint.

        :param PyQt.QtGui.QPaintEvent q_paint_event: The event object from PyQt (not used).
        """
        # Get a QPainter object that can paint on the drawing widget.
        painter = QtGui.QPainter( self.gui.drawing_widget )
        painter.setBrush( QtCore.Qt.white )  # The brush determines the fill color.
        painter.setPen( QtCore.Qt.black )    # The pen determines the outline color.

        # Width and height of the drawing widget.
        w = self.gui.drawing_widget.width()
        h = self.gui.drawing_widget.height()
        # Paint the graph leaving a margin if d//2 around the edges.
        self.paint_graph( painter, self.vertex_diameter // 2, self.vertex_diameter // 2,
                          w - self.vertex_diameter, h - self.vertex_diameter,
                          self.vertex_diameter )

    def paint_graph( self, painter, x, y, w, h, d ):
        """Paints the graph within the specified bounding rectangle.

        :param QtGui.QPainter painter: The painter to do the drawing.
        :param int x: The x-coordinate of the bounding rectangle's upper-left corner.
        :param int y: The y-coordinate of the bounding rectangle's upper-left corner.
        :param int w: The width of the bounding rectangle.
        :param int h: The height of the bounding rectangle.
        :param int d: The diameter of each vertex.
        """
        font_size = max( painter.font().pointSize(), painter.font().pixelSize() )
        # Paint the edges first, between the center of each vertex.
        for source in self.graph:
            for target_name in source.get_neighbors():
                # Get the actual target Vertex object.
                target = self.graph.vertices[ target_name ]
                # Draw the edge, scaling the coordinates to the bounding rectangle.
                x1 = x + source.x * w
                y1 = y + source.y * h
                x2 = x + target.x * w
                y2 = y + target.y * h
                if self.selected_edge is not None and source in self.selected_edge and target in self.selected_edge:
                    painter.setPen( QtCore.Qt.red )
                else:
                    painter.setPen( QtCore.Qt.black )
                painter.drawLine( x1, y1, x2, y2 )

                # Draw the edge weight, if the option is selected.
                if self.gui.show_weights_checkbox.isChecked():
                    # Clear a rectangle at the midpoint of the edge and draw the weight inside it.
                    painter.setPen( QtCore.Qt.white )
                    painter.drawRect( ( x1 + x2 ) // 2 - font_size, ( y1 + y2 ) // 2 - font_size // 2,
                                      font_size * 2, font_size )
                    painter.setPen( QtCore.Qt.black )
                    painter.drawText( ( x1 + x2 ) // 2 - font_size, ( y1 + y2 ) // 2 - font_size // 2,
                                      font_size * 2, font_size,
                                      QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter,
                                      "{:.0f}".format( source.neighbors[ target.name ] ) )

        # Paint the vertices after the edges so things look nice.
        for vertex in self.graph:
            # Set the brush (which determines the fill color) to highlight nodes appropriately.
            if vertex.name == self.graph.green_vertex:
                painter.setBrush( QtCore.Qt.green )
            elif vertex.name == self.graph.red_vertex:
                painter.setBrush( QtCore.Qt.red )
            elif vertex.name in self.graph.yellow_vertices:
                painter.setBrush( QtCore.Qt.yellow )
            else:
                painter.setBrush( QtCore.Qt.white )

            # Draw the vertex, scaling the coordinates to the bounding rectangle.
            painter.drawEllipse( ( x + vertex.x * w ) - d // 2, ( y + vertex.y * h ) - d // 2, d, d )

            # Draw the vertex name, if the option is selected.
            if self.gui.show_names_checkbox.isChecked():
                painter.drawText( ( x + vertex.x * w ) - d // 2, ( y + vertex.y * h ) - d // 2, d, d,
                                  QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter,
                                  str( vertex.name ) )


# Call main() if this file is being executed rather than imported.
if __name__ == "__main__":
    main()  # https://docs.python.org/3/library/__main__.html
