"""CS 220, Data Abstraction, Spring 2016, Dr. Bower.

This module contains unit tests of the Sort Demo algorithms.

Documentation: None.
"""

from Labs.Sorts.SortDemo import SortDemo
import unittest
import sys


# TODO - Lab 18: Read, discuss, and understand the following code.
class UnitTests( unittest.TestCase ):
    """Tests the Sort Demo algorithms."""

    def setUp( self ):
        """This method is called immediately before calling each test method."""
        # Create a new sort demo object for each test.
        self.sort_demo = SortDemo( order="Random", seed=37, size=512 )
        # Reset the sort demo object for each test.
        self.sort_demo.reset()

    def test_bubble( self ):
        """Test the sort demo's bubble sort."""
        self.assertFalse( self.sort_demo.is_sorted() )
        self.sort_demo.bubble()
        self.assertTrue( self.sort_demo.is_sorted() )

    def test_selection( self ):
        """Test the sort demo's selection sort."""
        self.assertFalse( self.sort_demo.is_sorted() )
        self.sort_demo.selection()
        self.assertTrue( self.sort_demo.is_sorted() )

    def test_insertion( self ):
        """Test the sort demo's insertion sort."""
        self.assertFalse( self.sort_demo.is_sorted() )
        self.sort_demo.insertion()
        self.assertTrue( self.sort_demo.is_sorted() )

    def test_shell( self ):
        """Test the sort demo's shell sort."""
        self.assertFalse( self.sort_demo.is_sorted() )
        self.sort_demo.shell()
        self.assertTrue( self.sort_demo.is_sorted() )

    def test_merge( self ):
        """Test the sort demo's merge sort."""
        self.assertFalse( self.sort_demo.is_sorted() )
        self.sort_demo.merge()
        self.assertTrue( self.sort_demo.is_sorted() )

    def test_quick( self ):
        """Test the sort demo's quick sort."""
        self.assertFalse( self.sort_demo.is_sorted() )
        self.sort_demo.quick()
        self.assertTrue( self.sort_demo.is_sorted() )

    def test_radix( self ):
        """Test the sort demo's radix sort."""
        self.assertFalse( self.sort_demo.is_sorted() )
        self.sort_demo.radix()
        self.assertTrue( self.sort_demo.is_sorted() )

    def test_heap( self ):
        """Test the sort demo's heap sort."""
        self.assertFalse( self.sort_demo.is_sorted() )
        self.sort_demo.heap()
        self.assertTrue( self.sort_demo.is_sorted() )

    def test_is_sorted( self ):
        """Test the is_sorted() method in the sort demo class."""
        # Since the is_sorted() method will be used in every test, better make sure it works.

        # Start by hacking the sort demo's data to ensure it is a sorted list and test it.
        self.sort_demo.data = list( range( 512 ) )
        self.assertTrue( self.sort_demo.is_sorted() )

        # Change the first element, test, and put it back.
        self.sort_demo.data[ 0 ] = 42
        self.assertFalse( self.sort_demo.is_sorted() )
        self.sort_demo.data[ 0 ] = 0
        self.assertTrue( self.sort_demo.is_sorted() )

        # Change the second element, test, and put it back.
        self.sort_demo.data[ 1 ] = 42
        self.assertFalse( self.sort_demo.is_sorted() )
        self.sort_demo.data[ 1 ] = 1
        self.assertTrue( self.sort_demo.is_sorted() )

        # Change the last element, test, and put it back.
        self.sort_demo.data[ -1 ] = 42
        self.assertFalse( self.sort_demo.is_sorted() )
        self.sort_demo.data[ -1 ] = 511
        self.assertTrue( self.sort_demo.is_sorted() )

        # Change the penultimate element, test, and put it back.
        self.sort_demo.data[ -2 ] = 42
        self.assertFalse( self.sort_demo.is_sorted() )
        self.sort_demo.data[ -2 ] = 510
        self.assertTrue( self.sort_demo.is_sorted() )

        # Swap a couple arbitrary elements in the middle, test, and swap them back.
        self.sort_demo.data[ 200 ], self.sort_demo.data[ 300 ] = self.sort_demo.data[ 300 ], self.sort_demo.data[ 200 ]
        self.assertFalse( self.sort_demo.is_sorted() )
        self.sort_demo.data[ 200 ], self.sort_demo.data[ 300 ] = self.sort_demo.data[ 300 ], self.sort_demo.data[ 200 ]
        self.assertTrue( self.sort_demo.is_sorted() )


# Call main() if this file is being executed rather than imported.
if __name__ == '__main__':
    sys.argv.append( "-v" )  # Add the verbose command line flag.
    unittest.main()  # https://docs.python.org/3/library/__main__.html
