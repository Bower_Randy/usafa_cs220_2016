"""CS 220, Data Abstraction, Spring 2016, Dr. Bower.

This module contains a timed comparison of the sort demo's algorithms.

Documentation: None.
"""

from timeit import timeit
from Labs.Sorts.SortDemo import SortDemo

# Create the sort demo object here so creating it, and the
# necessary memory allocation, is not part of the time results.
sort_demo = SortDemo( order="Random", seed=37, size=2**10 )


# TODO - Lab 18: Read, discuss, and understand the following code.
def main():
    """Main program to run the tests."""

    for sort_name in [ "bubble", "selection", "insertion", "shell", "merge", "quick", "radix", "heap" ]:
        print( "{:<10s}".format( sort_name.capitalize() ), end="\t", flush=True )
        t = timeit( "exec_sort( '{}' )".format( sort_name ),
                    setup="from __main__ import exec_sort", number=2 ** 5 )
        print( " t = {:.2f}".format( t ) )


def exec_sort( sort_name ):
    """Execute the indicated sort.

    :param str sort_name: The name of the sort method to execute.
    """
    # Reset the sort without changing the ordering or
    # seed to produce consistent/comparable results.
    sort_demo.reset()
    # Show progress each time the timeit function executes the function.
    print( end=".", flush=True )
    # Use Python's exec() function to execute the indicated sort method.
    exec( "sort_demo.{}()".format( sort_name ) )


# Call main() if this file is being executed rather than imported.
if __name__ == "__main__":
    main()  # https://docs.python.org/3/library/__main__.html
