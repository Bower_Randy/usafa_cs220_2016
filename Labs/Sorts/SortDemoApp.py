"""CS 220, Data Abstraction, Spring 2016, Dr. Bower.

This file contains a GUI application to visualize the Sort Demo class.

Documentation: None.
"""

from threading import Thread, Event
from time import sleep
from Labs.Sorts.SortDemo import SortDemo
from Labs.Sorts.SortDemoGui import Ui_SortDemoGui
from PyQt4 import QtCore, QtGui
import sys


def main():
    """Launch a GUI created with Qt Designer."""
    # Create a QApplication to handle event processing.
    qt_app = QtGui.QApplication( sys.argv )

    # Create an instance of the app and show the main window.
    my_app = SortDemoApp()
    my_app.main_window.show()

    # Execute the QApplication, exiting when it returns (i.e., the window is closed).
    sys.exit( qt_app.exec_() )  # Note the underscore at the end of exec_().


class SortDemoApp:
    """Application class to instantiate and control a Sort Demo GUI."""

    def __init__( self ):
        """Initialize and show the gui."""
        # Create the animation thread and the pause/stop events for it (name the thread for debugging).
        self.animation_thread = Thread( target=self.run, name="Animation" )
        self.stop_animation = Event()
        self.run_animation = Event()
        self.run_animation.set()  # Initially set so the animation runs; cleared to pause the animation.

        # The sort thread will be re-created when the Play/Pause button is clicked,
        # but create an empty thread here so PyCharm will know what it is.
        self.sort_thread = Thread()

        # Create the main window in which the gui will display, create
        # an instance of the gui, and set it up in the main window.
        self.main_window = QtGui.QWidget()
        self.gui = Ui_SortDemoGui()
        self.gui.setupUi( self.main_window )

        # Connect to the play and stop buttons.
        self.gui.play_button.clicked.connect( self.play )
        self.gui.stop_button.clicked.connect( self.stop )
        # Catch the paint event so the data can be drawn.
        self.gui.drawing_widget.paintEvent = self.paint_event
        # Catch the window close event so threads can be stopped.
        self.main_window.closeEvent = self.close_event

        # Create a new Sort Demo class with size based on the size of the drawing widget.
        # Note this must be done after the window is shown so the drawing widget's size is set.
        self.sort_demo = SortDemo( size=min( self.gui.drawing_widget.width(), self.gui.drawing_widget.height() ) )
        self.sort_demo.reset()

    def play( self ):
        """Called when the user clicks the Play/Pause button."""
        if self.animation_thread.is_alive():
            # If the thread is already running, then play/pause the animation.
            if self.run_animation.is_set():
                self.run_animation.clear()
            else:
                self.run_animation.set()
        else:
            # Re-create the sort demo in case the size of the drawing widget has changed.
            self.sort_demo = SortDemo( size=min( self.gui.drawing_widget.width(), self.gui.drawing_widget.height() ) )

            # Get the seed from the GUI, using a try/except in case the user types something silly.
            try:
                seed = int( self.gui.seed.text() )
            except ( ValueError, TypeError ):
                seed = 37
                self.gui.seed.setText( str( seed ) )

            if self.gui.random_button.isChecked():
                self.sort_demo.reset( "Random", seed )
            elif self.gui.ascending_button.isChecked():
                self.sort_demo.reset( "Ascending", seed )
            elif self.gui.descending_button.isChecked():
                self.sort_demo.reset( "Descending", seed )
            else:
                self.sort_demo.reset( "Almost", seed )

            # Create the sort thread, but don't start it yet (name the thread for debugging).
            if self.gui.bubble_button.isChecked():
                self.sort_thread = Thread( target=self.sort_demo.bubble, name="Bubble" )
            elif self.gui.selection_button.isChecked():
                self.sort_thread = Thread( target=self.sort_demo.selection, name="Selection" )
            elif self.gui.insertion_button.isChecked():
                self.sort_thread = Thread( target=self.sort_demo.insertion, name="Insertion" )
            elif self.gui.shell_button.isChecked():
                self.sort_thread = Thread( target=self.sort_demo.shell, name="Shell" )
            elif self.gui.merge_button.isChecked():
                self.sort_thread = Thread( target=self.sort_demo.merge, name="Merge" )
            elif self.gui.quick_button.isChecked():
                self.sort_thread = Thread( target=self.sort_demo.quick, name="Quick" )
            elif self.gui.heap_button.isChecked():
                self.sort_thread = Thread( target=self.sort_demo.heap, name="Heap" )
            else:
                self.sort_thread = Thread( target=self.sort_demo.radix, name="Radix" )

            # Clear the sort demo's event to be sure the animation thread has a chance to draw the initial data.
            self.sort_demo.permission_to_continue.clear()
            # Clear the animation's stop event so it will be able to run.
            self.stop_animation.clear()
            # Set the animation's run event so it will be able to run.
            self.run_animation.set()
            # Start the sort thread first because the animation thread only runs when the sort thread is alive.
            self.sort_thread.start()
            # Finally, start the animation thread.
            self.animation_thread.start()

    def stop( self ):
        """Called when the user clicks the Stop button."""
        # Set the stop animation event so the animation loop will terminate.
        self.stop_animation.set()
        # Set the run animation event in case it was paused so the loop will terminate due to the stop event.
        self.run_animation.set()
        # Set the sort demo's event to make sure the sort thread finishes.
        self.sort_demo.permission_to_continue.set()

    def run( self ):
        """This function is to be launched in a separate thread to run the animation."""
        # The stop event will be set by the stop button or when the user closes the window.
        while not self.stop_animation.is_set() and self.sort_thread.is_alive():
            # Clear the sort demo's pause event so it will wait while the data is displayed.
            self.sort_demo.permission_to_continue.clear()
            # Update the drawing widget to show this frame of the animation.
            self.gui.drawing_widget.update()
            # Set the window title to show the current stats for the sort.
            self.main_window.setWindowTitle( str( self.sort_demo ) )
            # Sleep 1/30th of a second for a 30 frames per second animation.
            sleep( 0.0333 )
            # Wait if the run event has been cleared (i.e., the animation has been paused).
            self.run_animation.wait()
            # Set the sort demo's pause event so it will continue.
            self.sort_demo.permission_to_continue.set()

            # NOTE: The sleep in the above loop is *between* the clear() and set() of the
            # sort demo's continue event. Thus, once the sort demo pauses and the animation
            # starts drawing, the animation will draw an un-changing snapshot of the sort.
            # Further, since the set at the end of one iteration of the animation loop is
            # immediately followed by the clear the beginning of the next iteration of the
            # animation loop, the sort should be drawn most every time a pause is called.

        # Wait briefly for the sort thread to finish.
        while self.sort_thread.is_alive():
            sleep( 0.0333 )

        # Set the window title to show the final stats for the sort and do one last update.
        self.main_window.setWindowTitle( str( self.sort_demo ) + "    Done." )
        self.gui.drawing_widget.update()

        # Create a new animation thread so it's ready to start nex time the Play button is clicked.
        self.animation_thread = Thread( target=self.run, name="Animation" )

    def paint_event( self, q_paint_event ):
        """Called automatically whenever the drawing widget needs to repaint.

        :param PyQt.QtGui.QPaintEvent q_paint_event: The event object from PyQt (not used).
        """
        # Get a QPainter object that can paint on the drawing widget.
        painter = QtGui.QPainter( self.gui.drawing_widget )
        painter.setBrush( QtCore.Qt.blue )  # The brush determines the fill color.
        painter.setPen( QtCore.Qt.blue )    # The pen determines the outline color.

        # Use the painter to draw the sort demo's data with a 2x2 rectangle for each value.
        size = len( self.sort_demo.data )
        for x in range( size ):
            painter.drawRect( x, size - self.sort_demo.data[ x ], 2, 2 )

    def close_event( self, event ):
        """Called automatically when the user closes the application window.

        :param PyQt.QtGui.QCloseEvent event: The event object from PyQt.
        """
        # Stop the animation and accept the event so the window will shut down.
        self.stop()
        event.accept()


# Call main() if this file is being executed rather than imported.
if __name__ == "__main__":
    main()  # https://docs.python.org/3/library/__main__.html
