"""CS 220, Data Abstraction, Spring 2016, Dr. Bower.

This module contains implementations of various sorting algorithms.

Documentation: None required for non-graded lab exercises.
"""

import random
import threading
# The threading module is needed in case the sorts are being animated by a GUI.
# All references to threading and events can be ignored and the sorts work fine.


# TODO - Lab 18: Read, discuss, and understand the following code.
def main():
    """Main program to test each of the sort methods."""
    # Create a new sort demo, reset it, and show it before sorting.
    sort_demo = SortDemo( order="Random", seed=37, size=2 ** 4 )
    sort_demo.reset()
    if len( sort_demo.data ) > 20:
        print( sort_demo )  # Just show the stats, not the actual data.
    else:
        print( sort_demo, " ".join( [ str( i ) for i in sort_demo.data ] ), sort_demo.is_sorted() )

    for sort_name in reversed( [ "bubble", "selection", "insertion", "shell", "merge", "quick", "radix", "heap" ] ):
        sort_demo.reset()
        exec( "sort_demo.{}()".format( sort_name ) )  # Executes the sort method.
        if len( sort_demo.data ) > 20:
            print( sort_demo )  # Just show the stats, not the actual data.
        else:
            print( sort_demo, " ".join( [ str( i ) for i in sort_demo.data ] ), sort_demo.is_sorted() )


# TODO - Lab 18: Read, discuss, and understand the following code.
class SortDemo( object ):
    """Implements various sorting algorithms, keeping information to compare performance."""

    def __init__( self, order="Random", seed=37, size=512 ):
        """Initializes the sorting demo with the given parameters."""
        self.sort_name = "Sort Demo"
        self.order = order
        self.seed = seed
        self.data = list( range( size ) )

        # Variables to track the number of comparisons and moves required to complete a sort.
        self.comps = 0
        self.moves = 0

        # Create an event than another thread could use to pause the sort in order to show the data.
        self.permission_to_continue = threading.Event()
        # The event is initially set, so another thread must clear it to pause the sort.
        # NOTE: If another thread clears the event, it must also set it or the sort may freeze!
        self.permission_to_continue.set()

    def pause( self ):
        """Pause the sort so another thread could show the data."""
        # If the event is never cleared by another thread, the sort demo cruises along, never waiting.
        # NOTE: If another thread clears the event, it must also set it or the sort may freeze!
        self.permission_to_continue.wait()

    def __str__( self ):
        """Returns a string of all items in the pile."""
        return "{:<10s}{:10,d} elements, {:10,d} comparisons, {:10,d} moves.".format(
            self.sort_name + ":", len( self.data ), self.comps, self.moves )

    def reset( self, order=None, seed=None ):
        """Resets the sort demo with the given parameters.

        Orderings can be specified as "Random", "Ascending", "Descending", and "Almost".
        Ascending and descending data is useful for testing best and worst case performance, respectively.
        Almost sorted data presents some interesting comparisons of the different sorting algorithms.

        :param str order: String indicating order of initial data; if None, previous ordering is used.
        :param int seed: Integer indicating random number seed; if None, previous seed is used.
        """
        # If not specified, use the previous value of each parameter.
        self.order = self.order if order is None else order
        self.seed = self.seed if seed is None else seed

        # Clear the data from the previous sort.
        self.sort_name = "Sort Demo"
        self.comps = 0
        self.moves = 0

        # Reset the values in ascending order and reverse it if necessary.
        # Note: This does not re-allocate the memory for the list, so this could be done
        # in a loop or using Python's timeit module without killing the garbage collector.
        for i in range( len( self.data ) ):
            self.data[ i ] = i
        if self.order == "Descending":
            self.data.reverse()

        # Randomize the data by swapping elements; how many swaps depends on the requested ordering.
        # If the requested order is Ascending or Descending, num_swaps remains zero.
        num_swaps = 0
        if self.order == "Almost":
            num_swaps = len( self.data ) // 10  # Only swap about 10% of the values.
        elif self.order == "Random":
            num_swaps = len( self.data ) * 4  # Lots of swaps.

        # Seed the random number generator and perform the swaps. Using the same
        # seed ensures the exact same data and gives reliable comparison data.
        random.seed( self.seed )
        for _ in range( num_swaps ):
            i = random.randint( 0, len( self.data ) - 1 )
            if self.order == "Almost":
                # Swap with a value fairly close to i (use min/max to stay in the bounds of the list).
                j = max( 0, min( len( self.data ) - 1, i + random.randint( -num_swaps, num_swaps ) ) )
            else:
                # Swap with any value anywhere in the data.
                j = random.randint( 0, len( self.data ) - 1 )
            temp_data = self.data[ i ]
            self.data[ i ] = self.data[ j ]
            self.data[ j ] = temp_data

    def is_sorted( self ):
        """Determines if the data is sorted; useful for testing."""
        for i in range( len( self.data ) - 1 ):
            # Check for neighboring elements out of order.
            if self.data[ i ] > self.data[ i + 1 ]:
                return False
        # Didn't find any out of order, so it must be sorted.
        return True

    def bubble( self ):
        """Performs a bubble sort on the data."""
        # http://en.wikipedia.org/wiki/Bubble_sort
        self.sort_name = "Bubble"
        self._bubble( 0, len( self.data ) - 1 )

    # TODO - Lab 18: Add code to the method below to count comparisons and moves.
    def _bubble( self, left, right ):
        # This variable allows the sort to stop if it ever
        # makes a trip without swapping at least one value.
        swapped_one = True  # True so the loop can start.

        # Each trip "bubbles" the largest element to the right.
        trip = left
        while trip < right and swapped_one:
            swapped_one = False  # Haven't swapped one yet on this trip.

            # Why does this loop stop at (right - trip)?
            for index in range( left, right - trip ):
                # Compare adjacent values and swap them if necessary.
                if self.data[ index ] > self.data[ index + 1 ]:
                    temp_data = self.data[ index ]
                    self.data[ index ] = self.data[ index + 1 ]
                    self.data[ index + 1 ] = temp_data
                    swapped_one = True

                # Including this call to pause() shows each individual swap.
                # Make yourself a bowl of popcorn if you'd like to watch it.
                # self.pause()

            self.pause()
            trip += 1

    def selection( self ):
        """Performs a selection sort on the data."""
        # http://en.wikipedia.org/wiki/Selection_sort
        self.sort_name = "Selection"
        self._selection( 0, len( self.data ) - 1 )

    # TODO - Lab 18: Add code to the method below to count comparisons and moves.
    def _selection( self, left, right ):
        # Each trip puts the smallest item exactly where it belongs.
        for trip in range( left, right ):
            # Find the index of the smallest value and put it where it goes.
            small = self._index_of_smallest( trip, right )
            temp_data = self.data[ trip ]
            self.data[ trip ] = self.data[ small ]
            self.data[ small ] = temp_data
            self.pause()

    def _index_of_smallest( self, left, right ):
        # Finds and returns the index of the smallest item between the indexes left and right, inclusive.
        smallest = left
        for index in range( left + 1, right + 1 ):
            if self.data[ index ] < self.data[ smallest ]:
                smallest = index
        return smallest

    def insertion( self ):
        """Performs an insertion sort on the data."""
        # http://en.wikipedia.org/wiki/Insertion_sort
        self.sort_name = "Insertion"
        self._insertion( 0, len( self.data ) - 1 )

    # TODO - Lab 18: Add code to the method below to count comparisons and moves.
    def _insertion( self, left, right ):
        # Do one selection to put the smallest element in left so the
        # inner loop below doesn't have to repeatedly check i > 0.
        index = self._index_of_smallest( left, right )
        temp_data = self.data[ left ]
        self.data[ left ] = self.data[ index ]
        self.data[ index ] = temp_data
        self.pause()

        # Each trip puts one more element into the sorted portion of the data.
        for trip in range( left + 1, right + 1 ):
            # Copy the value being inserted out of the array.
            index = trip
            temp_data = self.data[ index ]

            # Move values into the empty space until the location
            # of the value being inserted is reached.
            while temp_data < self.data[ index - 1 ]:
                self.data[ index ] = self.data[ index - 1 ]
                index -= 1

            # Copy the value being inserted into its new location.
            self.data[ index ] = temp_data
            self.pause()

    def shell( self ):
        """Performs a shell sort on the data."""
        # http://en.wikipedia.org/wiki/Shell_sort
        self.sort_name = "Shell"
        self._shell( 0, len( self.data ) - 1 )

    # TODO - Lab 18: Add code to the method below to count comparisons and moves.
    def _shell( self, left, right ):
        # Calculate initial value for h.
        h = 1
        while h <= ( right - left ) / 9:
            h = 3 * h + 1

        # Because the first trip through a shell sort noticeably moves items,
        # pause once before starting to be sure the original data is displayed.
        self.pause()
        while h > 0:
            # The inner part of this sort is basically an insertion sort.
            # So when h=1, this will completely sort the data.
            for trip in range( left + h, right + 1 ):
                index = trip
                temp_data = self.data[ index ]

                while index >= left + h and temp_data <= self.data[ index - h ]:
                    self.data[ index ] = self.data[ index - h ]
                    index -= h

                self.data[ index ] = temp_data

                # Including this call to pause() shows each individual h-sort, making
                # the animation seems slow, but the sort is still running quite fast.
                # self.pause()

                # Instead of pausing every time (as in the above comment), pause about one-
                # quarter of the animation frames (in a 30-frames-per-seconds animation)
                # which seems to give a pretty accurate visualization of the shell sort.
                if trip % 7 == 0:
                    self.pause()

            h //= 3
            self.pause()

    def merge( self ):
        """Performs a merge sort on the data."""
        # http://en.wikipedia.org/wiki/Merge_sort
        self.sort_name = "Merge"
        self._merge( 0, len( self.data ) - 1, [ 0 ] * len( self.data ) )  # Merge needs an auxiliary array.

    def _merge( self, left, right, aux ):
        # When left >= right, there's nothing to sort.
        # Notice the recursive calls are before the merge, so the
        # first actual merge only merges one element segments.
        if left < right:
            middle = ( right + left ) // 2
            self._merge( left, middle, aux )          # Recursive call before actually merging.
            self._merge( middle + 1, right, aux )     # Recursive call before actually merging.
            self._merge2( left, middle, right, aux )  # Actually do the merge!
            self.pause()

    # TODO - Lab 19: Add code to the method below to count comparisons and moves.
    def _merge2( self, left, middle, right, aux ):
        # Copies the left portion of the data being merged into the auxiliary array.
        for i in range( middle + 1, left, -1 ):
            aux[ i - 1 ] = self.data[ i - 1 ]

        # Copies the right portion of the data being merged into the auxiliary array.
        # Note: The items are copied in REVERSED ORDER so the largest are in the middle.
        for j in range( middle, right ):
            aux[ right + middle - j ] = self.data[ j + 1 ]

        # Merges the left and right portions together in sorted order.
        # Note: since the right portion is in reverse order, there is no
        # need to worry about going out of bounds!
        i = left
        j = right
        for k in range( left, right + 1 ):
            if aux[ j ] < aux[ i ]:
                self.data[ k ] = aux[ j ]
                j -= 1
            else:
                self.data[ k ] = aux[ i ]
                i += 1

    def quick( self ):
        """Performs a quick sort on the data."""
        # http://en.wikipedia.org/wiki/Quicksort
        self.sort_name = "Quick"
        self._quick( 0, len( self.data ) - 1 )

    def _quick( self, left, right ):
        # If the segment of data to be sorted is small, the overhead of
        # recursion isn't worth the benefit, so just do an insertion sort.
        if right - left > 7:
            pivot = self._partition( left, right )  # Or use partition2 or partition3.
            self._quick( left, pivot - 1 )   # Recursive call after partitioning.
            self._quick( pivot + 1, right )  # Recursive call after partitioning.
        elif right - left > 0:  # At least two to sort.
            self._insertion( left, right )

    # TODO - Lab 19: Add code to the method below to count comparisons and moves.
    def _partition( self, left, right ):
        # Use the rightmost element as the pivot.
        pivot = self.data[ right ]
        i = left
        j = right - 1  # Subtract one because the rightmost is the pivot.

        # Continue until the i and j indices cross.
        while i < j:
            # The purpose of the loop below is to increment i to the index of the
            # next value that needs to be moved to the other side of the pivot.
            while self.data[ i ] < pivot:  # Q: Why doesn't this need to check i < right?
                i += 1  # The only purpose of this loop is to increment i to the next value

            # The purpose of the loop below is to decrement j to the index of the
            # next value that needs to be moved to the other side of the pivot.
            while pivot < self.data[ j ] and j > left:
                j -= 1

            # Only swap if i and j are still on opposite sides of the pivot.
            if i < j:
                temp_data = self.data[ i ]
                self.data[ i ] = self.data[ j ]
                self.data[ j ] = temp_data

            self.pause()

        # Put the pivot element between the two partitions.
        temp_data = self.data[ i ]
        self.data[ i ] = self.data[ right ]
        self.data[ right ] = temp_data
        self.pause()

        # Return the index of the pivot element so the
        # recursive calls can be made on either side.
        return i

    # TODO - Lab 19: Add code to the method below to count comparisons and moves.
    def _partition2( self, left, right ):
        # Select a random pivot element, move it to the rightmost
        # location, and then call the regular partition method.
        i = random.randint( 0, right - left ) + left
        temp_data = self.data[ i ]
        self.data[ i ] = self.data[ right ]
        self.data[ right ] = temp_data
        return self._partition( left, right )

    # TODO - Lab 19: Add code to the method below to count comparisons and moves.
    def _partition3( self, left, right ):
        # Select the pivot element as the median of the leftmost,
        # rightmost, and middle elements, move it to the rightmost
        # location, and then call the regular partition method.
        mid = ( left + right ) // 2

        # The leftmost element is the median.
        if self.data[ mid ] < self.data[ left ] < self.data[ right ] or \
                self.data[ right ] < self.data[ left ] < self.data[ mid ]:
            temp_data = self.data[ left ]
            self.data[ left ] = self.data[ right ]
            self.data[ right ] = temp_data

        # The middle element is the median.
        elif self.data[ left ] < self.data[ mid ] < self.data[ right ] or \
                self.data[ right ] < self.data[ mid ] < self.data[ left ]:
            temp_data = self.data[ mid ]
            self.data[ mid ] = self.data[ right ]
            self.data[ right ] = temp_data

        # No need for the last else here because it would mean
        # the rightmost element was the median, so just leave it.
        return self._partition( left, right )

    def radix( self ):
        """Performs a radix sort on the data."""
        # http://en.wikipedia.org/wiki/Radix_sort
        self.sort_name = "Radix"
        self._radix( 0, len( self.data ) - 1 )

    # TODO - Lab 20: Add code to the method below to count comparisons and moves.
    def _radix( self, left, right ):
        # Create a list of ten empty lists to use as buckets.
        buckets = [ [] for _ in range( 10 ) ]

        # Because the first trip through a radix sort noticeably moves items,
        # pause once before starting to be sure the original data is displayed.
        self.pause()

        # Need to make one trip for each digit of the largest value in the data.
        for digit in range( len( str( max( self.data ) ) ) ):
            # Put all values into the appropriate bucket, based on the current digit of each value.
            for index in range( left, right + 1 ):
                buckets[ self._get_digit( digit, self.data[ index ] ) ].append( self.data[ index ] )

            # Copy from the buckets back into the array and empty the buckets.
            index = 0
            for bucket in range( 10 ):
                for value in buckets[ bucket ]:
                    self.data[ index ] = value
                    index += 1

                buckets[ bucket ].clear()
                self.pause()

    def _get_digit( self, digit, value ):
        # For use in radix sort; digit 0 is the rightmost digit.
        return value // ( 10 ** digit ) % 10

    def heap( self ):
        """Performs a heap sort on the data."""
        # http://en.wikipedia.org/wiki/Heapsort
        self.sort_name = "Heap"
        self._heap( 0, len( self.data ) - 1 )

    # TODO - Lab 28: Add code to the method below to count comparisons and moves.
    def _heap( self, left, right ):
        # This loop "adds" things to the heap.
        for i in range( left + 1, right + 1 ):
            self._push_up( left, i )
            self.pause()

        # This loop "removes" things from the heap.
        for i in range( right - left + 1 ):
            temp_data = self.data[ left ]
            self.data[ left ] = self.data[ right - i ]
            self.data[ right - i ] = temp_data
            self._push_down( left, right - i )
            self.pause()

    def _push_up( self, left, current ):
        # Maintains the heap-ness after an insertion.
        if current > left:
            # Greater-than comparison because this is a MAX heap.
            if self.data[ current ] > self.data[ self._parent( current ) ]:
                temp_data = self.data[ current ]
                self.data[ current ] = self.data[ self._parent( current ) ]
                self.data[ self._parent( current ) ] = temp_data
                self._push_up( left, self._parent( current ) )

    def _push_down( self, current, right ):
        # Maintains the heap-ness after a deletion.
        if self._left_child( current ) < right:
            large = self._left_child( current )
            # Stay within range of the heap.
            if self._right_child( current ) < right and \
                    self.data[ self._right_child( current ) ] > self.data[ self._left_child( current ) ]:  # MAX heap.
                large = self._right_child( current )

            if self.data[ large ] > self.data[ current ]:
                temp_data = self.data[ current ]
                self.data[ current ] = self.data[ large ]
                self.data[ large ] = temp_data
                self._push_down( large, right )

    # Q: Why are the following three methods marked as "static"?
    # A: They only need the value of the parameter to do their work and are NOT dependent on
    #    the current state of the object, so they can be "static" which means all instances
    #    of this class would use/share these methods (i.e., there is no need for each instance
    #    to have a separate copy of such methods ... there can be only one).

    @staticmethod
    def _parent( curr ):
        # Returns the index of the parent of the item at the given index.
        return ( curr - 1 ) // 2  # Integer division!

    @staticmethod
    def _left_child( curr ):
        # Returns the index of the left child of the item at the given index.
        return curr * 2 + 1

    @staticmethod
    def _right_child( curr ):
        # Returns the index of the right child of the item at the given index.
        return curr * 2 + 2


# Call main() if this file is being executed rather than imported.
if __name__ == "__main__":
    main()  # https://docs.python.org/3/library/__main__.html
