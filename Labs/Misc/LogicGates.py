"""CS 220, Data Abstraction, Spring 2016, Dr. Bower.

This module contains a hierarchy of classes representing logic gates.

Documentation: http://interactivepython.org/runestone/static/pythonds/index.html
"""

import itertools


# TODO - Lab 2: Read, discuss, and understand the following code.
def main():
    """Main program to test the logic gates."""

    # The following code constructs the circuit shown in the reading:
    # http://interactivepython.org/runestone/static/pythonds/_images/circuit1.png
    print( "Sample circuit from reading:" )
    for data in itertools.product( range( 2 ), repeat=4 ):
        a = InputGate( "A", data[ 0 ] )
        b = InputGate( "B", data[ 1 ] )
        c = InputGate( "C", data[ 2 ] )
        d = InputGate( "D", data[ 3 ] )
        g1 = AndGate( "G1" )
        g2 = AndGate( "G2" )
        g3 = OrGate( "G3" )
        g4 = NotGate( "G4" )
        Connector( a, g1 )
        Connector( b, g1 )
        Connector( c, g2 )
        Connector( d, g2 )
        Connector( g1, g3 )
        Connector( g2, g3 )
        Connector( g3, g4 )
        print( data, "-->", g4.get_output() )

    # Three-input majority circuit, http://i.stack.imgur.com/p7Z9X.gif
    print( "\nThree-input majority circuit:" )
    for data in itertools.product( range( 2 ), repeat=3 ):
        a = InputGate( "A", data[ 0 ] )
        b = InputGate( "B", data[ 1 ] )
        c = InputGate( "C", data[ 2 ] )
        and1 = AndGate( "And1" )
        and2 = AndGate( "And2" )
        and3 = AndGate( "And3" )
        or1 = OrGate( "Or1" )
        or2 = OrGate( "Or2" )
        Connector( a, and1 )
        Connector( a, and2 )
        Connector( b, and1 )
        Connector( b, and3 )
        Connector( c, and2 )
        Connector( c, and3 )
        Connector( and1, or1 )
        Connector( and2, or1 )
        Connector( or1, or2 )
        Connector( and3, or2 )
        print( data, "-->", or2.get_output() )
        # print( a, b, c, and1, and2, and3, or1, or2, sep="\n" )

    # The following code constructs a two-bit adder, http://i.stack.imgur.com/WRZvX.png
    print( "\nTwo-bit adder:" )
    for data in itertools.product( range( 2 ), repeat=4 ):
        # Note how the input values map to the most and least
        # significant bits used as input to the logic gates.
        print( "{}{} + {}{} = ".format( data[ 0 ], data[ 1 ], data[ 2 ], data[ 3 ] ), end="" )
        a0 = InputGate( "A0", data[ 1 ] )
        b0 = InputGate( "B0", data[ 3 ] )
        a1 = InputGate( "A1", data[ 0 ] )
        b1 = InputGate( "B1", data[ 2 ] )
        xor1 = XorGate( "XOR1" )
        and1 = AndGate( "AND1" )
        xor2 = XorGate( "XOR2" )
        and2 = AndGate( "AND2" )
        xor3 = XorGate( "XOR3" )
        and3 = AndGate( "AND3" )
        or1 = OrGate( "OR1" )
        Connector( a0, xor1 )
        Connector( b0, xor1 )
        Connector( a0, and1 )
        Connector( b0, and1 )
        Connector( a1, xor3 )
        Connector( b1, xor3 )
        Connector( a1, and3 )
        Connector( b1, and3 )
        Connector( and1, xor2 )
        Connector( and1, and2 )
        Connector( xor3, xor2 )
        Connector( xor3, and2 )
        Connector( and2, or1 )
        Connector( and3, or1 )
        # Note the output of the or gate is the most significant bit.
        print( "{}{}{}".format( or1.get_output(), xor2.get_output(), xor1.get_output(), sep="" ) )

    # Note: The set-reset circuit (aka flip-flop circuit)
    # doesn't seem to model well in this software system.


class LogicGate( object ):
    """Parent class for all logic gates."""

    def __init__( self, name ):
        """Creates a named logic gate.

        :param str name: The name of the logic gate.
        """
        self.name = name

    def __str__( self ):
        """Returns a string representation of the object."""
        return self.name

    def get_output( self ):
        """Performs the gate logic and returns the output.

        :return: The output of the logic gate.
        :rtype: int
        """
        return self.perform_gate_logic()

    def perform_gate_logic( self ):
        """Performs the gate logic and returns the result.

        :return: The result of the gate logic.
        :rtype: int
        """
        return 0


class InputGate( LogicGate ):
    """Input gate that reads and stores persistent input.
    Allows connecting one input to multiple gates.
    """

    def __init__( self, name, pin=None ):
        """Creates an input gate with the given default input value.

        :param int pin: The default value for the pin.
        """
        super().__init__( name )
        self.pin = pin

    def __str__( self ):
        """Returns a string representation of the object."""
        return "{} {} ".format( super().__str__(), self.pin )

    def perform_gate_logic( self ):
        """Performs the gate logic by reading the input from the user
        or using the previously entered value as persistent input.

        :return: The input entered by the user.
        :rtype: int
        """
        if self.pin is None:
            self.pin = int( input( "Enter input for gate {} --> ".format( self.name ) ) )
        return self.pin

    def update( self, pin=None ):
        """Allows updating the input to a persistent input gate.

        :param int pin: The default value for the pin.
        """
        if pin is None:
            self.pin = int( input( "Enter input for gate {} --> ".format( self.name ) ) )
        else:
            self.pin = pin


class BinaryGate( LogicGate ):
    """A two-input logic gate."""

    def __init__( self, name ):
        """Creates a binary logic gate with two pins."""
        super().__init__( name )
        self.pin_a = None
        self.pin_b = None

    def __str__( self ):
        """Returns a string representation of the object."""
        return "{} {} {} ".format( super().__str__(), self.pin_a, self.pin_b )

    def get_pin_a( self ):
        """Gets the input to pin a, reading from the user if necessary."""
        if self.pin_a is None:
            return int( input( "Enter Pin A input for gate {} --> ".format( self.name ) ) )
        else:
            return self.pin_a.from_gate.get_output()

    def get_pin_b( self ):
        """Gets the input to pin b, reading from the user if necessary."""
        if self.pin_b is None:
            return int( input( "Enter Pin B input for gate {} --> ".format( self.name ) ) )
        else:
            return self.pin_b.from_gate.get_output()

    def set_next_pin( self, source ):
        """Sets the next pin for this gate; called when creating connections.

        :param Connector source: The input source for this pin.
        """
        if self.pin_a is None:
            self.pin_a = source
        else:
            if self.pin_b is None:
                self.pin_b = source
            else:
                raise RuntimeError( "Cannot Connect: NO EMPTY PINS on gate {}.".format( self.name ) )


class AndGate( BinaryGate ):
    """A binary gate that performs a logical AND operation."""

    def perform_gate_logic( self ):
        """Performs the logical AND operation with the two pins and returns the result.

        :return: The result of the logical AND of the two input pins.
        :rtype: int
        """
        # This version uses lazy-evaluation, so pin b may never be initialized.
        # If there is more than one connection, this may cause problems.
        # if self.get_pin_a() == 1 and self.get_pin_b() == 1:
        #     return 1
        # else:
        #     return 0
        a = self.get_pin_a()
        b = self.get_pin_b()
        return 1 if a == 1 and b == 1 else 0  # https://www.python.org/dev/peps/pep-0308/


class OrGate( BinaryGate ):
    """A binary gate that performs a logical OR operation."""

    def perform_gate_logic( self ):
        """Performs the logical OR operation with the two pins and returns the result.

        :return: The result of the logical OR of the two input pins.
        :rtype: int
        """
        # This version uses lazy-evaluation, so pin b may never be initialized.
        # If there is more than one connection, this may cause problems.
        # if self.get_pin_a() == 1 or self.get_pin_b() == 1:
        #     return 1
        # else:
        #     return 0
        a = self.get_pin_a()
        b = self.get_pin_b()
        return 1 if a == 1 or b == 1 else 0  # https://www.python.org/dev/peps/pep-0308/


class NandGate( AndGate ):
    """A binary gate that performs a logical NAND operation."""

    def perform_gate_logic( self ):
        """Performs the logical NAND operation with the two pins and returns the result.

        :return: The result of the logical NAND of the two input pins.
        :rtype: int
        """
        return ( super().perform_gate_logic() - 1 ) * -1


class NorGate( OrGate ):
    """A binary gate that performs a logical NOR operation."""

    def perform_gate_logic( self ):
        """Performs the logical NOR operation with the two pins and returns the result.

        :return: The result of the logical NOR of the two input pins.
        :rtype: int
        """
        return ( super().perform_gate_logic() - 1 ) * -1


class XorGate( OrGate ):
    """A binary gate that performs a logical XOR operation."""

    def perform_gate_logic( self ):
        """Performs the logical XOR operation with the two pins and returns the result.

        :return: The result of the logical XOR of the two input pins.
        :rtype: int
        """
        a = self.get_pin_a()
        b = self.get_pin_b()
        return 1 if a == 1 and b == 0 or a == 0 and b == 1 else 0


class UnaryGate( LogicGate ):
    """A one-input logic gate."""

    def __init__( self, name ):
        """Creates a unary logic gate with one pin."""
        super().__init__( name )
        self.pin = None

    def __str__( self ):
        """Returns a string representation of the object."""
        return "{} {} ".format( super().__str__(), self.pin )

    def get_pin( self ):
        """Gets the input to the pin, reading from the user if necessary."""
        if self.pin is None:
            return int( input( "Enter Pin input for gate {} --> ".format( self.name ) ) )
        else:
            return self.pin.from_gate.get_output()

    def set_next_pin( self, source ):
        """Sets the next pin for this gate; called when creating connections.

        :param Connector source: The input source for this pin.
        """
        if self.pin is None:
            self.pin = source
        else:
            raise RuntimeError( "Cannot Connect: NO EMPTY PINS on this gate {}.".format( self.name ) )


class NotGate( UnaryGate ):
    """A unary gate that performs a logical NOT operation."""

    def perform_gate_logic( self ):
        """Performs the logical NOT operation with the input pin and returns the result.

        :return: The result of the logical NOT of the input pin.
        :rtype: int
        """
        if self.get_pin():
            return 0
        else:
            return 1


class Connector:
    """A connector between two logic gates."""

    def __init__( self, f_gate, t_gate ):
        """Creates a new connection between the given gates."""
        self.from_gate = f_gate
        self.to_gate = t_gate
        self.to_gate.set_next_pin( self )

    def __str__( self ):
        """Returns a string representation of the object."""
        return "{}<-->{} ".format( self.from_gate.name, self.to_gate.name )


# Call main() if this file is being executed rather than imported.
if __name__ == "__main__":
    main()  # https://docs.python.org/3/library/__main__.html
