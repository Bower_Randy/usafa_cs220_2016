"""CS 220, Data Abstraction, Spring 2016, Dr. Bower.

This module contains a Python iterator that produces Fibonacci numbers.

Documentation: None.
"""


def main():
    """Main program to use the iterator."""
    fib_iterator = Fibonacci()
    for n in fib_iterator:
        print( n, end=" " )
    print()


# TODO - Lab 8: Read, discuss, and understand the following code.
class Fibonacci( object ):
    """This class implements a Python iterator to produce Fibonacci numbers."""

    def __init__( self, n=20 ):
        """Creates a new iterator to produce the given number of values.

        :param int n: How many values to produce.
        """
        self._n = n

    def __iter__( self ):
        """Returns an iterator object over the sequence of Fibonacci numbers."""
        # Count the number of values produced to know when to stop.
        self._count = 0
        # Initialize the values of a and b so the sequence starts at zero.
        self._a = 1
        self._b = 0
        # The Fibonacci object itself is also the iterator object (i.e., it has a __next__() method).
        return self

    def __next__( self ):
        """Calculates and returns the next Fibonacci number in the sequence.

        :returns: The next value in the sequence.
        :rtype: int
        """
        # Make sure there are more values to generate.
        if self._count < self._n:
            # Keep track of how many values have been returned.
            self._count += 1
            # Think carefully about how this "Pythonic" assignment statement works.
            # You will see it frequently when reading Python code, for better or worse.
            self._a, self._b = self._b, self._a + self._b
            return self._a
        else:
            # Raising the StopIteration exception *quietly*
            # stops a for loop using the iterator.
            raise StopIteration


# Call main() if this file is being executed rather than imported.
if __name__ == "__main__":
    main()  # https://docs.python.org/3/library/__main__.html
