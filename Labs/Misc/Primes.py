"""CS 220, Data Abstraction, Spring 2016, Dr. Bower.

This module contains a Python iterator that produces prime numbers.

Documentation: None.
"""

from math import floor, sqrt


def main():
    """Main program to use the iterator."""
    prime_iterator = Primes()
    for n in prime_iterator:
        print( n, end=" " )
    print()


# TODO - Lab 8: Read, discuss, and understand the following code.
class Primes( object ):
    """This class implements a Python iterator to produce prime numbers."""

    def __init__( self, n=25 ):
        """Creates a new iterator to produce the given number of values.

        :param int n: How many values to produce.
        """
        self._n = n

    def __iter__( self ):
        """Returns an iterator object over the sequence of prime numbers."""
        # Count the number of values produced to know when to stop.
        self._count = 0
        # Start with 1 because the __next__() method increments the value before testing primeness.
        self._previous = 1
        # The Primes object itself is also the iterator object (i.e., it has a __next__() method).
        return self

    def __next__( self ):
        """Calculates and returns the next prime number in the sequence.

        :returns: The next value in the sequence.
        :rtype: int
        """
        # Make sure there are more values to generate.
        if self._count < self._n:
            # Keep track of how many values have been returned.
            self._count += 1
            # Increment previous until another prime number is found, then return it.
            self._previous += 1
            while not is_prime( self._previous ):
                self._previous += 1
            return self._previous
        else:
            # Raising the StopIteration exception *quietly*
            # stops a for loop using the iterator.
            raise StopIteration


def is_prime( n ):
    """Determines if the given number is prime.

    :param int n: The number whose primeness is to be determined.
    :return: True if n is prime; False otherwise.
    :rtype: bool
    """
    for i in range( 2, 1 + floor( sqrt( n ) ) ):
        if n % i == 0:
            # Found a factor, n is not prime.
            return False
    # Finished loop without finding a factor, n is prime.
    return True


# Call main() if this file is being executed rather than imported.
if __name__ == "__main__":
    main()  # https://docs.python.org/3/library/__main__.html
