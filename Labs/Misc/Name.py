"""CS 220, Data Abstraction, Spring 2016, Dr. Bower.

This module contains a very simple class that has a single string attribute.
The class also contains two methods to count the vowels in the string using
two different recursive programming techniques.

Documentation: None.
"""

VOWELS = "AEIOUaeiou"


def main():
    """Main program to test the count_vowels methods."""
    ww = Name( "Heisenberg" )
    print( ww.count_vowels_v1() )
    print( ww.count_vowels_v2() )


# TODO - Lab 11: Read, discuss, and understand the following code.
class Name( object ):
    """This class stores a name as a string."""

    def __init__( self, name="" ):
        self.name = name

    def count_vowels_v1( self ):
        """Counts the number of vowels in the name.

        :return: The number of vowels in the name.
        :rtype: int
        """
        # Call the helper method, passing the name as a parameter.
        return self._count_vowels_v1_helper( self.name )

    def _count_vowels_v1_helper( self, s ):
        """Private, recursive helper function for count_vowels_v1.

        :param str s: The string in which to count vowels.
        :return: The number of vowels in the given string.
        :rtype: int
        """
        # Each recursive call passes a shorter string, so stop when the string is empty.
        if len( s ) == 0:
            return 0
        elif s[ 0 ] in VOWELS:
            # Found a vowel so add one to the result of a recursive call with a shorter string.
            return 1 + self._count_vowels_v1_helper( s[ 1: ] )
        else:
            # Recursive call with a shorter string.
            return self._count_vowels_v1_helper( s[ 1: ] )

    def count_vowels_v2( self ):
        """Counts the number of vowels in the name.

        :return: The number of vowels in the name.
        :rtype: int
        """
        # Call the helper method passing the name and current index of zero.
        return self._count_vowels_v2_helper( self.name, 0 )

    def _count_vowels_v2_helper( self, s, i ):
        """Private, recursive helper function for count_vowels_v2.

        :param str s: The string in which to count vowels.
        :param int i: The current index in the name.
        :return: The number of vowels in the name.
        :rtype: int
        """
        # Each recursive call passes a larger value for i, so stop when it exceeds the length of the string.
        if i >= len( s ):
            return 0
        elif s[ i ] in VOWELS:
            # Found a vowel so add one to the result of a recursive call with a larger value for i.
            return 1 + self._count_vowels_v2_helper( s, i + 1 )
        else:
            # Recursive call with a larger value for i.
            return self._count_vowels_v2_helper( s, i + 1 )


# Call main() if this file is being executed rather than imported.
if __name__ == "__main__":
    main()  # https://docs.python.org/3/library/__main__.html
