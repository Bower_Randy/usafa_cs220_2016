"""CS 220, Data Abstraction, Spring 2016, Dr. Bower.

This module contains a Python iterator that produces powers of two.

Documentation: http://www.programiz.com/python-programming/iterator
"""


def main():
    """Main program to use the iterator."""
    power_iterator = PowTwo()
    for n in power_iterator:
        print( n, end=" " )
    print()


# TODO - Lab 8: Read, discuss, and understand the following code.
class PowTwo( object ):
    """This class implements a Python iterator to produce powers of two."""

    def __init__( self, n=20 ):
        """Creates a new iterator to produce the given number of values.

        :param int n: How many values to produce.
        """
        self._n = n

    def __iter__( self ):
        """Returns an iterator object over the sequence of powers of two."""
        # Count the number of values produced to know when to stop.
        self._count = 0
        # The PowTwo object itself is also the iterator object (i.e., it has a __next__() method).
        return self

    def __next__( self ):
        """Calculates and returns the next powers of two in the sequence.

        :returns: The next value in the sequence.
        :rtype: int
        """
        # Make sure there are more values to generate.
        if self._count <= self._n:
            # Keep track of how many values have been returned.
            self._count += 1
            # Return the next power of two (use count-1 since it has already been incremented).
            return 2 ** ( self._count - 1 )
        else:
            # Raising the StopIteration exception *quietly*
            # stops a for loop using the iterator.
            raise StopIteration


# Call main() if this file is being executed rather than imported.
if __name__ == "__main__":
    main()  # https://docs.python.org/3/library/__main__.html
