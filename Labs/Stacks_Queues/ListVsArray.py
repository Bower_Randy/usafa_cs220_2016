"""CS 220, Data Abstraction, Spring 2016, Dr. Bower.

This module contains a somewhat contrived comparison of Python's
resizeable list structure to a more traditional fixed-size array.

To satisfy curious minds, here are some details of Python's list:
  http://www.laurentluce.com/posts/python-list-implementation/
  https://www.quora.com/How-are-Python-lists-implemented-internally/

Documentation: None.
"""

from random import choice, randint, seed
from timeit import timeit

MAX_SIZE = 2 ** 20
HOW_MANY = 2 ** 10
NUM_TESTS = 2 ** 5


# TODO - Lab 3: Read, discuss, and understand the following code.
def main():
    """Main program to run and time the tests."""

    t0 = 0
    t1 = 0
    # Run each test one time and add the time to the total.
    for test in range( NUM_TESTS ):
        # Q: Why not use the number parameter to timeit rather than this loop?
        # A: Python (as well as most any language that does garbage collection) does some
        #    memory optimization as a program runs. This usually gets better the longer a
        #    program runs, so the tests are run one at a time so they can alternate.

        print( "{}: Running test_list() ...".format( test ), flush=True )
        t0 += timeit( "test_list()", setup="from __main__ import test_list", number=1 )
        print( "{}: Running test_array() ... ".format( test ), flush=True )
        t1 += timeit( "test_array()", setup="from __main__ import test_array", number=1 )
        print()

    print( "Average time for test_list()  ... {}".format( t0 / NUM_TESTS ) )
    print( "Average time for test_array() ... {}".format( t1 / NUM_TESTS ) )


def test_list():
    """Tests a resizeable list by randomly growing/shrinking it."""
    # Seed the random number generator to ensure consistent tests.
    seed( 42 )

    # Start with a list one-quarter of the maximum size.
    data = []
    for i in range( MAX_SIZE // 4 ):
        data.append( randint( 10, 99 ) )

    # Randomly grow or shrink the list, either doubling or cutting the size in half.
    for _ in range( HOW_MANY ):
        current_size = len( data )
        # Randomly pick between growing or shrinking the list.
        if choice( [ True, False ] ) and len( data ) <= MAX_SIZE // 2:
            # Double the size of the list, adding random values.
            for _ in range( current_size, current_size * 2 ):
                data.append( randint( 10, 99 ) )
        else:
            # Remove half of the values from the end of the list.
            for _ in range( current_size // 2, current_size ):
                data.pop()

        # Do something interesting with the list.
        total = sum_list( data, len( data ) )


def test_array():
    """Tests a fixed-size list by randomly doubling/halving the number of items."""
    # Seed the random number generator to ensure consistent tests.
    seed( 42 )

    # Allocate the largest possible list immediately.
    data = [ 0 ] * MAX_SIZE
    # Keep track of how many values are actually in the list.
    current_size = MAX_SIZE // 4
    for i in range( current_size ):
        data[ i ] = randint( 10, 99 )

    # Randomly grow or shrink the list, either doubling or cutting the size in half.
    for _ in range( HOW_MANY ):
        # Randomly pick between growing or shrinking the list.
        if choice( [ True, False ] ) and current_size <= MAX_SIZE // 2:
            # Double the size of the list, adding random values.
            for i in range( current_size, current_size * 2 ):
                data[ current_size ] = randint( 10, 99 )
            current_size *= 2
        else:
            # Remove half of the values from the end of the list.
            current_size //= 2

        # Do something interesting with the list.
        total = sum_list( data, current_size )


def sum_list( data, n ):
    """Calculates and returns the sum of the first n values in data.

    :param list[int] data: The list of values to be summed.
    :param int n: How many values in the list to sum.
    :return: An integer with the sum of the for n values in data.
    :rtype: int
    """
    result = 0
    for i in range( min( n, len( data ) ) ):
        result += data[ i ]
    return result


# Call main() if this file is being executed rather than imported.
if __name__ == "__main__":
    main()  # https://docs.python.org/3/library/__main__.html
