"""CS 220, Data Abstraction, Spring 2016, Dr. Bower.

This module contains several implementations of a Stack ADT.

Documentation: None required for non-graded lab exercises.
"""


# TODO - Lab 3: Read, discuss, and understand the following code.
class StackList( object ):
    """This class implements a Stack ADT using a Python list."""

    def __init__( self ):
        """Creates an empty stack."""
        # A Python list to hold the items.
        self._items = []

    def push( self, item ):
        """Adds an item onto the top of the stack.

        :param item: Item to be added to the stack.
        :return: None
        """
        self._items.append( item )

    def pop( self ):
        """Removes and returns the top item on the stack.

        :return: The top item on the stack.
        :raises IndexError: If the stack is empty.
        """
        if self.is_empty():
            raise IndexError( "ERROR: Pop from empty stack." )
        else:
            return self._items.pop()

    def peek( self ):
        """Returns, but does not remove, the top item on the stack.

        :return: The top item on the stack.
        :raises IndexError: If the stack is empty.
        """
        if self.is_empty():
            raise IndexError( "ERROR: Peek at empty stack." )
        else:
            return self._items[ -1 ]

    def is_empty( self ):
        """Determines if the stack is empty.

        :return: True if the stack is empty; false otherwise.
        :rtype: bool
        """
        # A non-empty list tests as True.
        return not self._items


class StackArray( object ):
    """This class implements a Stack ADT using a fixed-size Python list.

    The purpose of this implementation is to improve efficiency by not
    requiring the list of items to grow and shrink as items are added
    and removed. This models the basic array structure in many languages.
    """

    def __init__( self, capacity=1024 ):
        """Creates an empty stack."""
        # To improve performance, allocate all memory in advance.
        # This does have the disadvantage of limiting the capacity.
        self._items = [ None ] * capacity

        # TODO - Lab 3: Complete the method as described in the lab document.

    def push( self, item ):
        """Adds an item onto the top of the stack.

        :param item: Item to be added to the stack.
        :raises IndexError: If the stack is full.
        :return: None
        """
        # TODO - Lab 3: Implement the method as described in the lab document.
        pass  # Remove the pass statement (and this comment) when writing your own code.

    def pop( self ):
        """Removes and returns the top item on the stack.

        :return: The top item on the stack.
        :raises IndexError: If the stack is empty.
        """
        # TODO - Lab 3: Implement the method as described in the lab document.
        pass  # Remove the pass statement (and this comment) when writing your own code.

    def peek( self ):
        """Returns, but does not remove, the top item on the stack.

        :return: The top item on the stack.
        :raises IndexError: If the stack is empty.
        """
        # TODO - Lab 3: Implement the method as described in the lab document.
        pass  # Remove the pass statement (and this comment) when writing your own code.

    def is_empty( self ):
        """Determines if the stack is empty.

        :return: True if the stack is empty; false otherwise.
        :rtype: bool
        """
        # TODO - Lab 3: Implement the method as described in the lab document.
        pass  # Remove the pass statement (and this comment) when writing your own code.


class StackLinked( object ):
    """This class implements a Stack ADT using a linked data structure."""

    class Node( object ):
        """A single node in a linked list."""

        def __init__( self, item=None, next_node=None ):
            """Creates a node with the given item and next node reference.

            :param item: The item to be stored in the node.
            :param next_node: The next node in the list.
            """
            self.item = item
            self.next = next_node

    def __init__( self ):
        """Creates an empty stack."""
        # TODO - Lab 10: Implement the method as described in the lab document.
        pass  # Remove the pass statement (and this comment) when writing your own code.

    def push( self, item ):
        """Adds an item onto the top of the stack.

        :param item: Item to be added to the stack.
        :return: None
        """
        # TODO - Lab 10: Implement the method as described in the lab document.
        pass  # Remove the pass statement (and this comment) when writing your own code.

    def pop( self ):
        """Removes and returns the top item on the stack.

        :return: The top item on the stack.
        :raises IndexError: If the stack is empty.
        """
        # TODO - Lab 10: Implement the method as described in the lab document.
        pass  # Remove the pass statement (and this comment) when writing your own code.

    def peek( self ):
        """Returns, but does not remove, the top item on the stack.

        :return: The top item on the stack.
        :raises IndexError: If the stack is empty.
        """
        # TODO - Lab 10: Implement the method as described in the lab document.
        pass  # Remove the pass statement (and this comment) when writing your own code.

    def is_empty( self ):
        """Determines if the stack is empty.

        :return: True if the stack is empty; false otherwise.
        :rtype: bool
        """
        # TODO - Lab 10: Implement the method as described in the lab document.
        pass  # Remove the pass statement (and this comment) when writing your own code.
