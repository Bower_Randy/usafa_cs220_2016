"""CS 220, Data Abstraction, Spring 2016, Dr. Bower.

This module contains several implementations of a Queue ADT.

Documentation: None required for non-graded lab exercises.
"""


# TODO - Lab 4: Read, discuss, and understand the following code.
class QueueList( object ):
    """This class implements a Queue ADT using a Python list."""

    def __init__( self ):
        """Creates an empty queue."""
        # A Python list to hold the items.
        self._items = []

    def enqueue( self, item ):
        """Adds an item to the end of the queue.

        :param item: Item to be added to the queue.
        :return: None
        """
        self._items.append( item )

    def dequeue( self ):
        """Removes and returns the first item in the queue.

        :return: The first item in the queue.
        :raises IndexError: If the queue is empty.
        """
        if self.is_empty():
            raise IndexError( "ERROR: Dequeue from empty queue." )
        else:
            return self._items.pop( 0 )

    def peek( self ):
        """Returns, but does not remove, the first item in the queue.

        :return: The first item in the queue.
        :raises IndexError: If the queue is empty.
        """
        if self.is_empty():
            raise IndexError( "ERROR: Peek at empty queue." )
        else:
            return self._items[ 0 ]

    def is_empty( self ):
        """Determines if the queue is empty.

        :return: True if the queue is empty; False otherwise.
        :rtype: bool
        """
        # A non-empty list tests as True.
        return not self._items


class QueueArray( object ):
    """This class implements a Queue ADT using a fixed-size Python list.

    The purpose of this implementation is to improve efficiency by not
    requiring the list of items to grow and shrink as items are added
    and removed. This models the basic array structure in many languages.
    """

    def __init__( self, capacity=1024 ):
        """Creates an empty queue."""
        # To improve performance, allocate all memory in advance.
        # This does have the disadvantage of limiting the capacity.
        self._items = [ None ] * capacity

        # TODO - Lab 4: Complete the method as described in the lab document.

    def enqueue( self, item ):
        """Adds an item to the end of the queue.

        :param item: Item to be added to the queue.
        :return: None
        """
        # TODO - Lab 4: Implement the method as described in the lab document.
        pass  # Remove the pass statement (and this comment) when writing your own code.

    def dequeue( self ):
        """Removes and returns the first item in the queue.

        :return: The first item in the queue.
        :raises IndexError: If the queue is empty.
        """
        # TODO - Lab 4: Implement the method as described in the lab document.
        pass  # Remove the pass statement (and this comment) when writing your own code.

    def peek( self ):
        """Returns, but does not remove, the first item in the queue.

        :return: The first item in the queue.
        :raises IndexError: If the queue is empty.
        """
        # TODO - Lab 4: Implement the method as described in the lab document.
        pass  # Remove the pass statement (and this comment) when writing your own code.

    def is_empty( self ):
        """Determines if the queue is empty.

        :return: True if the queue is empty; False otherwise.
        :rtype: bool
        """
        # TODO - Lab 4: Implement the method as described in the lab document.
        pass  # Remove the pass statement (and this comment) when writing your own code.


class QueueLinked( object ):
    """This class implements a Queue ADT using a linked data structure."""

    class Node( object ):
        """A single node in a linked list."""

        def __init__( self, item=None, next_node=None ):
            """Creates a node with the given item and next node reference.

            :param item: The item to be stored in the node.
            :param next_node: The next node in the list.
            """
            self.item = item
            self.next = next_node

    def __init__( self ):
        """Creates an empty queue."""
        # TODO - Lab 10: Implement the method as described in the lab document.
        pass  # Remove the pass statement (and this comment) when writing your own code.

    def enqueue( self, item ):
        """Adds an item to the end of the queue.

        :param item: Item to be added to the queue.
        :return: None
        """
        # TODO - Lab 10: Implement the method as described in the lab document.
        pass  # Remove the pass statement (and this comment) when writing your own code.

    def dequeue( self ):
        """Removes and returns the first item in the queue.

        :return: The first item in the queue.
        :raises IndexError: If the queue is empty.
        """
        # TODO - Lab 10: Implement the method as described in the lab document.
        pass  # Remove the pass statement (and this comment) when writing your own code.

    def peek( self ):
        """Returns, but does not remove, the first item in the queue.

        :return: The first item in the queue.
        :raises IndexError: If the queue is empty.
        """
        # TODO - Lab 10: Implement the method as described in the lab document.
        pass  # Remove the pass statement (and this comment) when writing your own code.

    def is_empty( self ):
        """Determines if the queue is empty.

        :return: True if the queue is empty; False otherwise.
        :rtype: bool
        """
        # TODO - Lab 10: Implement the method as described in the lab document.
        pass  # Remove the pass statement (and this comment) when writing your own code.
