"""CS 220, Data Abstraction, Spring 2016, Dr. Bower.

This module contains Stack and Queue unit tests.

Documentation: None.
"""

import unittest
import sys

# Use the appropriate imports below to test the different stack and queue implementations.
from Labs.Stacks_Queues.Stacks import StackList as Stack
from Labs.Stacks_Queues.Queues import QueueList as Queue

# from Labs.Stacks_Queues.Stacks import StackArray as Stack
# from Labs.Stacks_Queues.Queues import QueueArray as Queue

# from Labs.Stacks_Queues.Stacks import StackLinked as Stack
# from Labs.Stacks_Queues.Queues import QueueLinked as Queue


# TODO - Lab 3: Read, discuss, and understand the following code.
class UnitTests( unittest.TestCase ):
    """For more information about Python's unittest module, see
    https://docs.python.org/3/library/unittest.html
    """

    @classmethod
    def setUpClass( cls ):
        """A class method called before any tests in an individual class run."""
        pass

    @classmethod
    def tearDownClass( cls ):
        """A class method called after all tests in an individual class run."""
        pass

    def setUp( self ):
        """This method is called immediately before calling each test method."""
        # Create a new, empty, stack to be used for the test.
        self.stack = Stack()
        # Create a new, empty, queue to be used for the test.
        self.queue = Queue()

    def tearDown( self ):
        """This method is called immediately after each test method completes."""
        pass

    def test_stack_one_item( self ):
        """Tests basic operations with one integer item."""
        self.assertTrue( self.stack.is_empty() )   # It's new, so it should be empty.
        self.stack.push( 42 )                      # Adds an item.
        self.assertFalse( self.stack.is_empty() )  # No longer empty.
        self.assertEqual( self.stack.peek(), 42 )  # Added item is there.
        self.assertFalse( self.stack.is_empty() )  # Still there (i.e., peek doesn't remove).
        self.assertEqual( self.stack.pop(), 42 )   # Added item is removed.
        self.assertTrue( self.stack.is_empty() )   # Pop removes the item.

    def test_stack_many_items( self ):
        """Test adding and removing multiple items."""
        # It's new, so it should be empty.
        self.assertTrue( self.stack.is_empty() )

        # Add a whole bunch of items.
        for i in range( 1024 ):
            self.stack.push( i )
            self.assertFalse( self.stack.is_empty() )

        # Make sure the items are removed in the correct order.
        for i in range( 1023, -1, -1 ):
            self.assertFalse( self.stack.is_empty() )
            self.assertEqual( self.stack.peek(), i )
            self.assertEqual( self.stack.pop(), i )

        # Should now be empty.
        self.assertTrue( self.stack.is_empty() )

        # Now test re-add a few items to be sure it can recover from the empty state.
        self.stack.push( 42 )
        self.assertFalse( self.stack.is_empty() )
        self.assertEqual( self.stack.peek(), 42 )
        self.stack.push( 37 )
        self.assertFalse( self.stack.is_empty() )
        self.assertEqual( self.stack.peek(), 37 )

        # Finally, remove those items.
        self.assertEqual( self.stack.pop(), 37 )
        self.assertFalse( self.stack.is_empty() )
        self.assertEqual( self.stack.peek(), 42 )
        self.assertEqual( self.stack.pop(), 42 )
        self.assertTrue( self.stack.is_empty() )

    def test_stack_exceptions( self ):
        """Test exceptions."""
        # It's new, so it should be empty.
        self.assertTrue( self.stack.is_empty() )

        # Should not be able to peek when it's empty.
        with self.assertRaises( IndexError ):
            print( self.stack.peek() )

        # Should not be able to remove when it's empty.
        with self.assertRaises( IndexError ):
            print( self.stack.pop() )

        # Add a couple things, remove them, and then be sure the exceptions still work.
        self.stack.push( 42 )
        self.stack.push( 37 )
        self.assertEqual( self.stack.peek(), 37 )
        self.assertEqual( self.stack.pop(), 37 )
        self.assertEqual( self.stack.peek(), 42 )
        self.assertEqual( self.stack.pop(), 42 )

        # Should not be able to peek when it's empty.
        with self.assertRaises( IndexError ):
            print( self.stack.peek() )

        # Should not be able to remove when it's empty.
        with self.assertRaises( IndexError ):
            print( self.stack.pop() )

    def test_queue_one_item( self ):
        """Tests basic operations with one integer item."""
        self.assertTrue( self.queue.is_empty() )      # It's new, so it should be empty.
        self.queue.enqueue( 42 )                      # Adds an item.
        self.assertFalse( self.queue.is_empty() )     # No longer empty.
        self.assertEqual( self.queue.peek(), 42 )     # Added item is there.
        self.assertFalse( self.queue.is_empty() )     # Still there (i.e., peek doesn't remove).
        self.assertEqual( self.queue.dequeue(), 42 )  # Added item is removed.
        self.assertTrue( self.queue.is_empty() )      # Pop removes the item.

    def test_queue_many_items( self ):
        """Test adding and removing multiple items."""
        # It's new, so it should be empty.
        self.assertTrue( self.queue.is_empty() )

        # Add a whole bunch of items.
        for i in range( 1024 ):
            self.queue.enqueue( i )
            self.assertFalse( self.queue.is_empty() )

        # Make sure the items are removed in the correct order.
        for i in range( 1024 ):
            self.assertFalse( self.queue.is_empty() )
            self.assertEqual( self.queue.peek(), i )
            self.assertEqual( self.queue.dequeue(), i )

        # Should now be empty.
        self.assertTrue( self.queue.is_empty() )

        # Now test re-add a few items to be sure
        # it can recover from the empty state.
        self.queue.enqueue( 42 )
        self.assertFalse( self.queue.is_empty() )
        self.assertEqual( self.queue.peek(), 42 )
        self.queue.enqueue( 37 )
        self.assertFalse( self.queue.is_empty() )
        self.assertEqual( self.queue.peek(), 42 )

        # Finally, remove those items.
        self.assertEqual( self.queue.dequeue(), 42 )
        self.assertFalse( self.queue.is_empty() )
        self.assertEqual( self.queue.peek(), 37 )
        self.assertEqual( self.queue.dequeue(), 37 )
        self.assertTrue( self.queue.is_empty() )

    def test_queue_exceptions( self ):
        """Test exceptions."""
        # It's new, so it should be empty.
        self.assertTrue( self.queue.is_empty() )

        # Should not be able to peek when it's empty.
        with self.assertRaises( IndexError ):
            print( self.queue.peek() )

        # Should not be able to remove when it's empty.
        with self.assertRaises( IndexError ):
            print( self.queue.dequeue() )

        # Add a couple things, remove them, and
        # then be sure the exceptions still work.
        self.queue.enqueue( 42 )
        self.queue.enqueue( 37 )
        self.assertEqual( self.queue.peek(), 42 )
        self.assertEqual( self.queue.dequeue(), 42 )
        self.assertEqual( self.queue.peek(), 37 )
        self.assertEqual( self.queue.dequeue(), 37 )

        # Should not be able to peek when it's empty.
        with self.assertRaises( IndexError ):
            print( self.queue.peek() )

        # Should not be able to remove when it's empty.
        with self.assertRaises( IndexError ):
            print( self.queue.dequeue() )

    def test_palindromes( self ):
        """Tests the Stack and Queue ADTs by doing several palindrome checks."""
        # A list of palindromes.
        palindromes = [ "Go hang a salami; I'm a lasagna hog.", "Able was I ere I saw Elba.",
                        "Madam, I'm Adam.", "Madam in Eden, I'm Adam", "Never odd or even",
                        "Lee has a race car as a heel.", "No sir! Away! A papaya war is on!",
                        "Stressed? No tips? Spit on desserts.", "Mr. Owl ate my metal worm.",
                        "Flee to me, remote Elf!", "So many Dynamos!", "1001001", "Huh?" ]

        # Testing the is_palindrome() function, which uses a Stack and a Queue.
        for p in palindromes:
            with self.subTest( p ):
                self.assertTrue( is_palindrome( p ) )


def is_palindrome( s ):
    """Determines if the given string is a palindrome.

    :param str s: The string to be tested for palindrome-ness.
    :return: True if the given string is a palindrome; False otherwise.
    :rtype: bool
    """
    # Create an empty stack and an empty queue to use in the test.
    stack = Stack()
    queue = Queue()
    # Add all the alphanumeric characters to the stack and the queue.
    for c in s.lower():
        if c.isalnum():
            stack.push( c )
            queue.enqueue( c )

    # Remove characters from the stack and queue as long as they are equal.
    while not stack.is_empty() and not queue.is_empty() and stack.pop() == queue.dequeue():
        pass

    # Since the characters will come out of the stack and queue in
    # opposite order, if the loop empties them, it's a palindrome.
    return stack.is_empty() and queue.is_empty()


if __name__ == '__main__':
    sys.argv.append( "-v" )  # Add the verbose command line flag.
    unittest.main()
